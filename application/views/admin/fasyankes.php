<div class="panel-header panel-header-sm">
    
      </div>
<div class="row">
          <div class="col-md-12">
            <div class="card">

            <div class="card-header">
                <h5 class="title">Administrator Panel</h5>
                <p class="category">Master data Unit TB</p>
              </div>

              <div class="card-body">
<div class="input-group col-lg-4">
    <input type="text" id="search" name="search" class="form-control" placeholder="<?php echo $this->lang->line('search');?>">
    <div class="input-group-append"><span class="btn  btn-success" id="src"><i class="fas fa-search"></i></span></div>
    &nbsp;
    <a href="#cardform" class="btn btn-success btn-xs" id="add" data-toggle="modal" data-target="#modalForm" data-backdrop="static" data-keyboard='false'><i class="fas fa-plus-circle"></i>&nbsp;<?php echo $this->lang->line('add');?></a>
</div>

</div>

              <div class="card-body all-icons">
            <div class="row">




<table class="table table-stripe">
<thead>
<tr>
    <th scope="col"><h6><?php echo $this->lang->line('number');?></h6></th>
    <th scope="col"><h6>Kode Unit</h6></th>
    <th scope="col"><h6>Kategori Unit</h6></th>
    <th scope="col"><h6><?php echo $this->lang->line('province_name');?></h6></th>
    <th scope="col"><h6><?php echo $this->lang->line('district_name');?></h6></th>
    <th scope="col"><h6>Nama Unit TB</h6></th>
    <th scope="col"><h6><?php echo $this->lang->line('action');?></h6></th>
</tr>
</thead>
<tbody id="dataBody">
    <?php 
    $i=1;
    foreach($datalist as $list){ ?>
    <tr>
    <th scope="row"><?php echo $i;?></th>
    <td scope="col"><?php echo $list->unit_code;?></td>
    <td scope="col"><?php echo $list->unitgroup_name?></td>
    <td scope="col"><?php echo $list->province_name?></td>
    <td scope="col"><?php echo $list->district_name?></td>
    <td scope="col"><?php echo $list->unit_name?></td>
    <td scope="col">
    <a href="#" class="btn btn-danger btn-sm" onclick="conf('<?php echo $list->unit_code?>')"><i class="fas fa-trash-alt"></i>&nbsp;<?php echo $this->lang->line('delete');?></a>
    &nbsp;
    <a href="#cardedit" id="btnedit" class="btn btn-success btn-sm"   onclick="getDetail('<?php echo $list->unit_code;?>','<?php echo $list->province_name;?>')"><i class="fas fa-edit"></i>&nbsp;<?php echo $this->lang->line('edit');?></a>
    </td>
    
</tr>

    <?php $i++;
} ?>
</tbody>
</table>

            </div>
              </div>
            </div>
          </div>
</div>
