<?php
Class Users extends CI_Controller{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('admin/M_user');
        $this->load->model('admin/M_propinsi');
    }
    public function index($page){
        $data['propinsi'] = $this->M_propinsi->getList();
        $data['unitLevel'] = $this->M_user->unitLevel();
        $data['datalist'] = $this->M_user->getList($page);
        $data['total'] = $this->M_user->countAll();
        $this->template->renderpage('admin/users',$data);
    }

    public function uservalidate(){
        $username = $this->input->post("user_name");
        $muser = $this->M_user->checkUsername($username);
        if($muser>0){
            echo "false";
        }else{
            echo "true";
        }
    }


    public function detail(){
        $rdata = $this->input->post();
        $json['response'] = $this->M_user->detail($rdata);
        if($json){
            $json['status'] = "success";
        }else{
            $json['status'] = "error";
        }
        echo json_encode($json);
    }

   

    public function useradd(){
        
        $passwd = $this->M_user->getPassword();
        $rdata = $this->input->post();
        //unset($rdata['user_password']);
        $rdata["user_password"]= md5($passwd);
        $rdata["user_oldpassword"] = $passwd;
        
        $json=$this->M_user->insert($rdata);
        if($json){
            $jinfo['status']='success';
        }else{
            $jinfo['status']='error';
        }
        echo json_encode($jinfo);
    }


    public function usergroup(){
        $uid = $this->input->post("user_unit_level");
        if($json['response'] = $this->M_user->userGroup($uid)){
            $json['status'] = "success";
        }else{
            $json['status']="error";
        };

        echo json_encode($json);
        
    
    }

    public function delete($id){
        if($this->M_user->delete($id)){
            redirect(base_url()."account/user/list/1");
        }
    }

    public function search(){
        $search = $this->input->post('search');
        $data['datalist'] = $this->M_user->search($search);
        $this->load->view('admin/user_search',$data);
    }


    public function emailvalidation(){
        $email =$this->input->post();
        $mcheck = $this->M_user->checkmail($email);
        if($mcheck>0){
            echo "false";
        }else{
            echo "true";
        }

    }

    public function editemail(){
        $mdata['user_email'] = $this->input->post('edit_user_email');
        $mdata['user_name'] = $this->input->post('edit_user_name');
        $lcheck = $this->M_user->checkmail($mdata);
        if($lcheck>0){
            echo "true";
        }else{
        $email['user_email'] = $mdata['user_email'];
        $rcheck = $this->M_user->checkmail($email);
        if($rcheck>0){
            echo "false";
        }else{
            echo "true";
        }
          

        }
    }


    public function update(){
        $rdata = $this->input->post();
        $username = $this->input->post("user_name");
        if($this->M_user->update($rdata,$username)){
            $json['status']='success';
        }else{
            $json['status']="failed";
        }
        echo json_encode($json);
    }
}