<?php
header("Content-type: application/vnd-ms-excel");
header("Content-Disposition: attachment; filename=data_tabulasi_perangkat_TCM_".$id.".xls");
?>

<table class="table table-stripe">
<thead>
<tr>
    <td scope="col"  class="text-center">No.</td>
    <td scope="col"  class="text-center">Propinsi</td>
    <td scope="col"  class="text-center">Kabupaten</td>
    <td scope="col"  class="text-center">Nama Lab</td>
    <td scope="col"  class="text-center">Perangkat TCM</td>
    <td scope="col"  class="text-center">
                        Upload Terakhir
                      </td>
                      <td scope="col"  class="text-center">
                        Total
                      </td>
                      <td  scope="col" class="text-center">
                        MTB-
                      </td>
                      <td scope="col"   class="text-center">MTB+ RS</td>
                      <td scope="col"  class="text-center">MTB+ RR</td>
                      <td scope="col"  class="text-center">Invalid</td>
                      <td scope="col"  class="text-center">Error</td>
                      <td  scope="col" class="text-center">No Result</td>
                      </tr>
</thead>
<tbody id="dataBody">
    <?php 
    $i=1;
    foreach($datalist as $list){ ?>
    <tr>
    <th scope="row" class="text-right"><?php echo $i;?></th>
    <td scope="col" class="text-left"><?php echo $this->M_global->namaPropinsi("$list->unit_province");?></td>
    <td scope="col" class="text-left"><?php echo $this->M_global->namaKabupaten("$list->unit_district")?></td>

    <td scope="col" class="text-left"><?php echo $list->unit_name?></td>
    <td scope="col" class="text-left"><?php echo $list->host_name?></td>
    <td scope="col" class="text-center"><?php echo $list->last_date?></td>
    <td scope="col" class="text-right"><?php echo $list->total?></td>
    <td scope="col" class="text-right"><?php echo $list->mtb_neg?></td>
    <td scope="col" class="text-right"><?php echo $list->mtb_pos_rs?></td>
    <td scope="col" class="text-right"><?php echo $list->mtb_pos_rr?></td>
    <td scope="col" class="text-right"><?php echo $list->mtb_invalid?></td> 
    <td scope="col" class="text-right"><?php echo $list->mtb_error?></td>
    <td scope="col" class="text-right"><?php echo $list->mtb_noresult?></td>
    
    
</tr>

    <?php $i++;
} ?>
</tbody>
</table>