<script src="<?php echo base_url()."assets/js/jquery.tablesorter.js";?>"></script>
 <script src="<?php echo base_url()."assets/js/jquery.tablesorter.widgets.js";?>"></script>
 <link href="<?php echo base_url()."assets/css/theme.jui.css";?>" rel="stylesheet" />

<div class="panel-header panel-header-sm">
    
      </div>
<div class="row">
          <div class="col-md-12">
            <div class="card">
<?php
$faskes = $this->M_global->namaFaskes($this->uri->segment(4));
?>
            <div class="card-header">
                <h5 class="title">Rekap Penggunaan Cartridge</h5>
                <p class="category"><?php echo $faskes;?></p>
              </div>


              <div class="card-body all-icons">
            <div class="row" id="wrapper">



            <table id="tblData" class="table" cellspacing="0" cellpadding="0">

<thead>
<tr>
   
    <th scope="col" class="text-center">Tahun</th>
    <th scope="col"  class="text-center">Jan</th>
    <th scope="col"  class="text-center">Feb</th>
    <th scope="col"  class="text-center">Mar</th>
    <th scope="col"  class="text-center">Apr</th>
    <th scope="col"  class="text-center">Mei</th>
    <th scope="col"  class="text-center">Jun</th>
    <th scope="col"  class="text-center">Jul</th>
    <th scope="col"  class="text-center">Agt</th>
    <th scope="col"  class="text-center">Sep</th>
    <th scope="col"  class="text-center">Okt</th>
    <th scope="col"  class="text-center">Nop</th>
    <th scope="col"  class="text-center">Des</th>
    <th scope="col"  class="text-center">Total</th>
   
                      </tr>
</thead>
<tbody id="dataBody">
    <?php 
    $i=1;
    foreach($datalist as $list){ ?>
    <tr>
    <th scope="row" class="text-center"><?php echo $list->tahun;?></th>
    <td scope="col" class="text-center"><?php echo $list->jan;?></td>
    <td scope="col" class="text-center"><?php echo $list->feb;?></td>
    <td scope="col" class="text-center"><?php echo $list->mar;?></td>
    <td scope="col" class="text-center"><?php echo $list->apr;?></td>
    <td scope="col" class="text-center"><?php echo $list->mei;?></td>
    <td scope="col" class="text-center"><?php echo $list->jun;?></td>
    <td scope="col" class="text-center"><?php echo $list->jul;?></td>
    <td scope="col" class="text-center"><?php echo $list->agt;?></td>
    <td scope="col" class="text-center"><?php echo $list->sep;?></td>
    <td scope="col" class="text-center"><?php echo $list->okt;?></td>
    <td scope="col" class="text-center"><?php echo $list->nop;?></td>
    <td scope="col" class="text-center"><?php echo $list->des;?></td>
    <td scope="col" class="text-center"><?php echo $list->total;?></td>
    
</tr>

    <?php $i++;
} ?>
</tbody>
</table>

            </div>
            <div class="card-footer">

            <a href="<?php echo base_url()."report/tabulasi/logistikbulanandownload/".$this->uri->segment('4');?>" class="btn btn-block"><i class="fas fa-download fa-2x"></i>&nbsp;Download</a>


            </div>
              </div>
            </div>
          </div>
</div>



<script>
     $(function(){
  $('#tblData').tablesorter(); 
});
     </script>