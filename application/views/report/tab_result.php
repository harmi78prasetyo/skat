<?php

$max = ceil($total/100);
  if($this->uri->segment(4)>5 && ($this->uri->segment(4)+5)>6){

  
   if($this->uri->segment(4)==$max || ($this->uri->segment(4)+2)>$max){ 
     $maxnum = 1+$this->uri->segment(4);
  }else{ 
    $maxnum = 3+$this->uri->segment(4);
  }
  
  $start = $maxnum-5;
  $prev = $start-1; 

}else{
      $maxnum = 6;
      $start = 1;
      $prev = 1;

  }
  ?>




<div class="panel-header panel-header-sm">


    
      </div>
<div class="row">


          <div class="col-md-12">
            <div class="card">

            <div class="card-header">
                <h5 class="title">Tabulasi Data</h5>
                <p class="category">Tabel data hasil pemeriksaan</p>




                





<nav aria-label="...">
  <ul class="pagination">
  <?php if($this->uri->segment(4)>1){?>
    <li class="page-item">
      <a class="page-link" href="<?php echo $prev;?>">Previous</a>
    </li>
  <?php }else{ ?>
    <li class="page-item disabled">
      <a class="page-link" href="#" tabindex="-1">Previous</a>
    </li>
  <?php } ?>

<?php
  for($i=$start;$i<=($maxnum-1);$i++){
  

if($i==$this->uri->segment(4)){
        ?>
        <li class="page-item active" id="page_<?php echo $i;?>"><a class="page-link" href="<?php echo $i;?>"><?php echo $i;?></a></li>
        <?php

    }else{

      if($i>$max){
       ?>
        <li class="page-item disabled" id="page_<?php echo $i;?>"><a class="page-link" href="<?php echo $i;?>"><?php echo $i;?></a></li>
<?php
      }else{
      ?>
<li class="page-item" id="page_<?php echo $i;?>"><a class="page-link" href="<?php echo $i;?>"><?php echo $i;?></a></li>

      <?php
      }
    }
   }
   
   if($maxnum<$max){
    ?>
    
    <li class="page-item">
      <a class="page-link" href="<?php echo $maxnum; ?>">Next</a>
    </li>
   <?php }else{?>
    <li class="page-item">
      <a class="page-link disabled" href="<?php echo $maxnum; ?>">Next</a>
    </li>
  <?php } ?>
  </ul>
</nav>

</div>


              <div class="card-body all-icons">



              <div class="row">
                <form>
  <select name="province_code" id="province_code">
    <option value="">-- Propinsi --</option>
    <?php foreach($propinsi as $lp){?>
      <?php if($this->session->userdata("user_group")=='2' || $this->session->userdata("user_group")=='1' ){?>
        <option value="<?php echo $lp->province_code;?>"><?php echo $lp->province_name;?></option>
     
      <?php }else{ ?> 
        <option value="<?php echo $lp->province_code;?>" <?php if($this->session->userdata("user_province")==$lp->province_code){?> selected="selected" <?php }else{?> disabled="disabled"<?php }?>><?php echo $lp->province_name;?></option>
       
        <?php }?>
      <?php }?>
  </select>
  <select name="district_code" id="district_code">
    <option value="">-- Kab/Kota --</option>
  </select>
  <select name="unit_code" id="unit_code">
    <option value="">-- Fasyankes --</option>
  </select>
  <input type="text" name="start_date" id="start_date" placeholder="Periode" class="tanggal"> s/d <input type="text" name="end_date" id="end_date" placeholder="Periode" class="tanggal" disabled="disabled">
  <a href="<?php echo base_url()."report/tabulasi/resultdownload";?>" id="linkDownload" class="btn btn-light">Download</a>
</form>  
              </div>

              
            <div class="row">




<table class="table table-stripe">
<thead>
<tr>
    <td scope="col"><h6 class="title">No.</h6></td>
    <td scope="col"><h6 class="title">Propinsi</h6></td>
    <td scope="col"><h6 class="title">Kabupaten</h6></td>
    <td scope="col"><h6 Class="title">Nama Lab</h6></td>
    <td scope="col"><h6 Class="title">ID Pasien</h6></td>
    <td scope="col">
                        <h6  Class="title">Tanggal Pemeriksaan</h6>
                      </td>
                    
                      <td scope="col"><h6  Class="title">MTB</h6></td>
                      <td scope="col"><h6 Class="title">RR</h6></td>
                      </tr>
</thead>
<tbody id="dataBody">
    <?php 
    $i=1;
    foreach($datalist as $list){ ?>
    <tr>
    <th scope="row" class="text-center"><?php echo $i+(($this->uri->segment('4')*100)-100);?></th>
    <td scope="col" class="text-center"><?php echo $list->province_name;?></td>
    <td scope="col" class="text-center"><?php echo $list->district_name;?></td>
    <td scope="col" class="text-center"><?php echo $list->unit_name;?></td>
    <td scope="col" class="text-center"><?php echo $list->test_patient_id;?></td>
    <td scope="col" class="text-center"><?php echo $list->tgl_periksa;?></td>
    <td scope="col" class="text-center"><?php echo $list->mtb_result?></td>
    <td scope="col" class="text-center"><?php echo $list->rr_result;?></td>
    
    
    
</tr>

    <?php $i++;
} ?>
</tbody>
</table>

            </div>
              </div>
            </div>
          </div>
</div>

<script>
  $('document').ready(function(){

    $('#province_code').change(function(){
                  $.ajax({
                    url :'<?php echo base_url()."administratif/kabupaten/listkabupaten";?>',
                    type:'POST',
                    dataType:'json',
                    data:{
                      'province_code':$(this).val()
                      },
                      success:function(jdata){
                        var str ='<option value=""><?php echo $this->lang->line('district');?></option>';
                        $.each(jdata.response,function(i,item){
                          str +='<option value="'+item.district_code+'">'+item.district_name+'</option>';
                          })
                          $('#district_code').html(str);
                          }
                          })
                         // FusionCharts('lc2').dispose();
                          if($(this).val()!=''){
                            $('#dataBody').load("<?php echo base_url()."report/tabulasi/resultfilter/1";?>",{"unit_province":$(this).val()});
                            $('#linkDownload').attr("href","<?php echo base_url()."report/tabulasi/resultdownload/daerah/";?>"+$(this).val());
                          }else{
                            $('#dataBody').load("<?php echo base_url()."report/tabulasi/resultfilter/1";?>");
                            $('#linkDownload').attr("href","<?php echo base_url()."report/tabulasi/resultdownload/nasional";?>");
                          }
                  
                  
                  });



                  $('#district_code').change(function(){
                    $.ajax({
                      url:'<?php echo base_url()."administratif/fasyankes/listfaskes";?>',
                      type:'POST',
                      dataType:'json',
                      data:{
                        'unit_district':$(this).val(),
                        'unit_unitgroup_id':'4'
                      },
                      success:function(jsdata){
                        var str = '<option value="">Fasyankes</option>';
                        $.each(jsdata.response,function(i,item){
                        str +='<option value="'+item.unit_code+'">'+item.unit_name+'</option>';  
                        })
                        $('#unit_code').html(str);;
                      }
                    })
                   // FusionCharts('lc2').dispose();

                    if($(this).val()!=''){
                      $('#dataBody').load("<?php echo base_url()."report/tabulasi/resultfilter/1";?>",{"unit_district":$(this).val()});
                      $('#linkDownload').attr("href","<?php echo base_url()."report/tabulasi/resultdownload/kabupaten/";?>"+$(this).val());
                     

                     
                    }else{
                      $('#dataBody').load("<?php echo base_url()."report/tabulasi/resultfilter/1";?>",{"unit_province":$("#province_code").val()});                            
                      $('#linkDownload').attr("href","<?php echo base_url()."report/tabulasi/resultdownload/daerah/";?>"+$("#province_code").val());
                    }
                  });



                  
                  $('#unit_code').change(function(){
                  //  FusionCharts('lc2').dispose();


                    if($(this).val()!=''){
                      $('#dataBody').load("<?php echo base_url()."report/tabulasi/resultfilter/1";?>",{"host_unit_code":$(this).val()});
                      $('#linkDownload').attr("href","<?php echo base_url()."report/tabulasi/resultdownload/unit/";?>"+$(this).val());
                    }else{
                      $('#dataBody').load("<?php echo base_url()."report/tabulasi/resultfilter/1";?>",{"unit_district":$("#district_code").val()});
                      $('#linkDownload').attr("href","<?php echo base_url()."report/tabulasi/resultdownload/kabupaten/";?>"+$("#district_code").val());

                    }



                  })






  $('#start_date').datepicker({
  format:"yyyy-mm-dd",
  startView:"year",
  minView:"year",
  maxDate:"0d"
}).on('changeDate',function(ev){
  $(this).blur();
  $(this).datepicker('hide');
$('#end_date').removeAttr('disabled');
if($('#end_date').val()!=''){
  var periode = $('#start_date').val()+"_"+$('#end_date').val();
if($('#province_code').val()==''){
  $('#dataBody').load("<?php echo base_url()."report/tabulasi/resultfilter/1";?>",{"start_date":$('#start_date').val(),"end_date":$('#end_date').val()});
  $('#linkDownload').attr("href","<?php echo base_url()."report/tabulasi/resultdownload/daerah/";?>"+periode);
 
}else if($('#district_code').val()=='' && $('#province_code').val()!=''){
  $('#dataBody').load("<?php echo base_url()."report/tabulasi/resultfilter/1";?>",{"unit_province":$("#province_code").val(),"start_date":$('#start_date').val(),"end_date":$('#end_date').val()});
  $('#linkDownload').attr("href","<?php echo base_url()."report/tabulasi/resultdownload/daerah/";?>"+$("#province_code").val()+"/"+periode);
 
}else if($('#unit_code').val()=='' && $('#district_code').val()!=''){
  $('#dataBody').load("<?php echo base_url()."report/tabulasi/resultfilter/1";?>",{"unit_district":$("#district_code").val(),"start_date":$('#start_date').val(),"end_date":$('#end_date').val()});
  $('#linkDownload').attr("href","<?php echo base_url()."report/tabulasi/resultdownload/kabupaten/";?>"+$("#district_code").val()+"/"+periode);
 
}else if($('#unit_code').val()!=''){
  $('#dataBody').load("<?php echo base_url()."report/tabulasi/resultfilter/1";?>",{"host_unit_code":$("#unit_code").val(),"start_date":$('#start_date').val(),"end_date":$('#end_date').val()});

  $('#linkDownload').attr("href","<?php echo base_url()."report/tabulasi/resultdownload/unit/";?>"+$("#unit_code").val()+"/"+periode);
}else{
  $('#dataBody').load("<?php echo base_url()."report/tabulasi/resultfilter/1";?>",{"start_date":$('#start_date').val(),"end_date":$('#end_date').val()});
  $('#linkDownload').attr("href","<?php echo base_url()."report/tabulasi/resultdownload/daerah/";?>"+periode);
}

}
});


$('#end_date').datepicker({
  format:"yyyy-mm-dd",
  startView:"year",
  minView:"year",
  maxDate:"0d"
}).on('changeDate',function(ev){
  $(this).blur();
  $(this).datepicker('hide');
 // alert($(this).val());
//$('#end_date').removeAttr('disabled');
var periode = $('#start_date').val()+"_"+$('#end_date').val();
if($('#province_code').val()==''){
  $('#dataBody').load("<?php echo base_url()."report/tabulasi/resultfilter/1";?>",{"start_date":$('#start_date').val(),"end_date":$('#end_date').val()});
  $('#linkDownload').attr("href","<?php echo base_url()."report/tabulasi/resultdownload/daerah/";?>"+periode);
 
}else if($('#district_code').val()=='' && $('#province_code').val()!=''){
  $('#dataBody').load("<?php echo base_url()."report/tabulasi/resultfilter/1";?>",{"unit_province":$("#province_code").val(),"start_date":$('#start_date').val(),"end_date":$('#end_date').val()});
  $('#linkDownload').attr("href","<?php echo base_url()."report/tabulasi/resultdownload/daerah/";?>"+$("#province_code").val()+"/"+periode);
 
}else if($('#unit_code').val()=='' && $('#district_code').val()!=''){
  $('#dataBody').load("<?php echo base_url()."report/tabulasi/resultfilter/1";?>",{"unit_district":$("#district_code").val(),"start_date":$('#start_date').val(),"end_date":$('#end_date').val()});
  $('#linkDownload').attr("href","<?php echo base_url()."report/tabulasi/resultdownload/kabupaten/";?>"+$("#district_code").val()+"/"+periode);
 
}else if($('#unit_code').val()!=''){
  $('#dataBody').load("<?php echo base_url()."report/tabulasi/resultfilter/1";?>",{"host_unit_code":$("#unit_code").val(),"start_date":$('#start_date').val(),"end_date":$('#end_date').val()});

  $('#linkDownload').attr("href","<?php echo base_url()."report/tabulasi/resultdownload/unit/";?>"+$("#unit_code").val()+"/"+periode);
}else{
  $('#dataBody').load("<?php echo base_url()."report/tabulasi/resultfilter/1";?>",{"start_date":$('#start_date').val(),"end_date":$('#end_date').val()});
  $('#linkDownload').attr("href","<?php echo base_url()."report/tabulasi/resultdownload/daerah/";?>"+periode);
}
 
});




  })
</script>