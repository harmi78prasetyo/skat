<div class="panel-header panel-header-sm">
    
      </div>
<div class="row">
          <div class="col-md-12">
            <div class="card">

            <div class="card-header">
                <h5 class="title">Administrator Panel</h5>
                <p class="category">Aktivasi Perangkat TCM</p>
              </div>

              <div class="card-body">
<div class="input-group col-lg-4">
    <input type="text" id="search" name="search" class="form-control" placeholder="<?php echo $this->lang->line('search');?>">
    <div class="input-group-append"><span class="btn  btn-success" id="src"><i class="fas fa-search"></i></span></div>
    &nbsp;
</div>
</div>

              <div class="card-body all-icons">
            <div class="row">




<table class="table table-stripe">
<thead>
<tr>
    <th scope="col"><h6><?php echo $this->lang->line('number');?></h6></th>
    <th scope="col"><h6>Host ID</h6></th>
    <th scope="col"><h6>Serial</h6></th>
    <th scope="col"><h6>Insert Date</h6></th>
    <th scope="col"><h6><?php echo $this->lang->line('action');?></h6></th>
</tr>
</thead>
<tbody id="dataBody">
    <?php 
    $i=1;
    foreach($datalist as $list){ ?>
    <tr>
    <th scope="row"><?php echo $i;?></th>
    <td scope="col"><?php echo $list->HostId;?></td>
    <td scope="col"><?php echo $list->Serial;?></td>
    <td scope="col"><?php echo $list->InsertedOn;?></td>
    <td scope="col">
        <p class="btn btn-success" id="actButton" onclick="activated('<?php echo $list->DeviceId;?>')">Aktivasi</p>
    </td>
    
</tr>

    <?php $i++;
} ?>
</tbody>
</table>

            </div>
              </div>
            </div>
          </div>
</div>







<!---- Modal Form -->

<!---- Form Add New User -->
<div class="modal fade" id="modalForm">
    <div class="modal-dialog modal-dialog-centered">
      <div class="modal-content">
      
        <!-- Modal Header -->
        <div class="modal-header bg-merah">
          <h4 class="modal-title"><i class="fas fa-laptop-medical"></i>&nbsp;Aktivasi Perangkat</h4>
          <button type="button" class="close" data-dismiss="modal">&times;</button>
        </div>
        
        <!-- Modal body -->
        <div class="modal-body">
          
        <form method="POST" action="<?php echo base_url()."master/device/add";?>" id="device">

<input type="hidden" id="host_device_id">
<div class="form-row">
<div class="col-md-12" style="margin-top: 10px;">
                <div class="form-label-group">
              
                <select name="host_province" id="host_province" class="form-control custom-select"  placeholder="<?php echo $this->lang->line("province");?>"
                >
                      <option value=""><?php echo $this->lang->line("province");?></option>
                  <?php

foreach($propinsi as $provlist){
?>
<option value="<?php echo $provlist->province_code; ?>"><?php echo $provlist->province_name;?></option>
<?php
}
?>

</select>        
                </div>
              </div>
</div>



<div class="form-row">
<div class="col-md-12" style="margin-top: 10px;">
                <div class="form-label-group">
              
                <select name="host_district" id="host_district" class="form-control custom-select"   placeholder="<?php echo $this->lang->line("district");?>"  disabled="disabled">
                      <option value=""><?php echo $this->lang->line("district");?></option>
                  

</select>        
                </div>
              </div>
</div>



<div class="form-row">
<div class="col-md-12" style="margin-top: 10px;">
                <div class="form-label-group">
              
                <select name="host_unit_code" id="host_unit_code" class="form-control custom-select"   placeholder="Unit TB"  disabled="disabled">
                      <option value=""><?php echo $this->lang->line("host_unit");?></option>
                  

</select>        
                </div>
              </div>
</div>








<div class="form-row">
<div class="col-md-12" style="padding-top:10px">
                <div class="form-label-group">
                
                <a class="btn btn-success btn-block"   id="btnSubmit" onClick="submitForm('<?php echo base_url()."administratif/device/activated";?>','<?php echo base_url()."master/device/newlist";?>')"><?php echo $this->lang->line('submit');?></a>
               
               
                </div>
              </div>
</div>

        </form>
        </div>
      </div>
    </div>
</div>

<!-- End Form -->

<script>

    
    $(document).ready(function(){
        $('#host_province').change(function(){
            getListKab($(this).val(),'#host_district','<?php echo base_url()."administratif/kabupaten/listkabupaten";?>');
        })
        $('#host_district').change(function(){
            getListFaskes($(this).val(),'#host_unit_code','<?php echo base_url()."administratif/fasyankes/listfaskes";?>');
        })
      
    })
</script>