<div class="panel-header panel-header-sm">
    
      </div>
<div class="row">
          <div class="col-md-12">
            <div class="card">

            <div class="card-header">
                <h5 class="title">Administrator Panel</h5>
                <p class="category">Master data Perangkat TCM</p>
              </div>

              <div class="card-body">
<div class="input-group col-lg-4">
    <input type="text" id="search" name="search" class="form-control" placeholder="<?php echo $this->lang->line('search');?>">
    <div class="input-group-append"><span class="btn  btn-success" id="src"><i class="fas fa-search"></i></span></div>
    &nbsp;
</div>

</div>

              <div class="card-body all-icons">
            <div class="row">




<table class="table table-stripe">
<thead>
<tr>
    <th scope="col"><h6><?php echo $this->lang->line('number');?></h6></th>
    <th scope="col"><h6>Unit TB (LAB)</h6></th>
    <th scope="col"><h6>HOST ID </h6></th>
    <th scope="col"><h6>SERIAL</h6></th>
    <th scope="col"><h6>Tanggal Aktifasi</h6></th>
    
</tr>
</thead>
<tbody id="dataBody">
    <?php 
    $i=1;
    foreach($datalist as $list){ ?>
    <tr>
    <th scope="row"><?php echo $i;?></th>
    <td scope="col"><?php echo $list->unit_name;?></td>
    <td scope="col"><?php echo $list->host_name?></td>
    <td scope="col"><?php echo $list->host_serial?></td>
    <td scope="col"><?php echo $list->host_date_registered?></td>
    
    
</tr>

    <?php $i++;
} ?>
</tbody>
</table>

            </div>
              </div>
            </div>
          </div>
</div>
