<?php
Class M_global extends CI_Model{
    public function __construct()
    {
        parent::__construct();
    }

    public function namaFaskes($id){
        $this->db->select("unit_name");
        $this->db->where("unit_code",$id);
        $b= $this->db->get(DB_MASTER_UNIT_TB)->row();
        return $b->unit_name;
    }

    public function namaPropinsi($id){
        $this->db->select("province_name");
        $this->db->where("province_code",$id);
        $b=$this->db->get(DB_MASTER_PROPINSI)->row();
        return $b->province_name;
    }

    public function namaKabupaten($id){
        $this->db->select("district_name");
        $this->db->where("district_code",$id);
        $b=$this->db->get(DB_MASTER_KABUPATEN)->row();
        return $b->district_name;
    }

    public function formatTanggal($tgl){
        $this->db->select("DATE_FORMAT('$tgl','%d/%m/%Y') as tgl");
        $tg=$this->db->get()->row();
        return $tg->tgl;
    }

    public function listPropinsi(){
        return $this->db->get(DB_MASTER_PROPINSI)->result();
    }
    public function listKabupaten($prop){
        $this->db->where("district_province",$prop);
        return $this->db->get(DB_MASTER_KABUPATEN)->result();
    }

    public function listFasyankes($district){
        $this->db->where("unit_district",$district);
        $this->db->where("unit_unitgroup_id","4");
        return $this->db->get(DB_MASTER_UNIT_TB)->result();
    }

    public function lastday($date){
        $this->db->select("DAY(LAST_DAY('".$date."')) as lastdate");
        $q = $this->db->get()->row();
        return $q->lastdate;
    }

    public function getDate($str){
        $this->db->select("DATE_FORMAT(STR_TO_DATE('".$str."','%Y%m%d'),'%d/%m/%Y') as label");
        $q = $this->db->get()->row();
        return $q->label;
    }

    public function getMonth($str){
        $this->db->select("DATE_FORMAT(STR_TO_DATE('".$str."','%Y%m%d'),'%m-%Y') as label");
        $q = $this->db->get()->row();
        return $q->label;
    }



    public function getPassword(){
        $this->db->select("left(UUID(),8) as user_oldpassword");
        $q = $this->db->get()->row();
        return $q->user_oldpassword;
    }
}