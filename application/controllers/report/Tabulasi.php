<?php
Class Tabulasi extends CI_Controller{
    public function __construct()
    {
        parent::__construct();
        $this->load->model("M_report");
    }

    public function index(){
        $this->template->renderpage("report/tabulasi");
    }

    public function tabdevices(){


        $data['propinsi'] = $this->M_global->listPropinsi();

        if($this->session->userdata("user_group")=='3'){
            $data['kabupaten'] = $this->M_global->listKabupaten($this->session->userdata("user_province"));
            $data['datalist'] = $this->M_report->getDeviceDataFilter("daerah",array("unit_province"=>$this->session->userdata("user_province")),null);
        }
        
        if($this->session->userdata("user_group")=='4'){
            $data['unit'] = $this->M_global->listFasyankes($this->session->userdata("user_district"));
            $data['datalist'] = $this->M_report->getDeviceDataFilter("kabupaten",array("unit_district"=>$this->session->userdata("user_district")),null);
        }

        if($this->session->userdata("user_group")=='5'){
            $data['unit'] = $this->M_global->listFasyankes($this->session->userdata("user_district"));
            $data['datalist'] = $this->M_report->getDeviceDataFilter("unit",array("unit_code"=>$this->session->userdata("user_unit")),null);
        
        }

        if($this->session->userdata("user_group")=='1' || $this->session->userdata("user_group")=='2' ){
            $data['datalist'] = $this->M_report->getDeviceData();
        }

       

        
        $this->template->renderpage("report/tab_devices",$data);
    }

    public function tabdevicesfilter($lev){
        $rdata = $this->input->post();
        unset($rdata['start_date']);
        unset($rdata['end_date']);
        if($this->input->post('start_date')){
        $periode = $this->input->post('start_date')."_".$this->input->post('end_date');
        }else{
            $periode=null;
        }

        $data['datalist'] = $this->M_report->getDeviceDataFilter($lev,$rdata,$periode);
        $this->load->view('report/tab_devices_data',$data);

    }


    public function tabdevicesDownload(){
       
      if($this->uri->segment('4')=='nasional'){
          $lev = "nasional";
          $rdata =null;
          $periode=$this->uri->segment('5');
      }elseif($this->uri->segment('4')=='daerah'){
        $lev = "daerah";
        $rdata =array("unit_province"=>$this->uri->segment('5'));
        $periode=$this->uri->segment('6');
    }elseif($this->uri->segment('4')=='kabupaten'){
        $lev = "kabupaten";
        $rdata =array("unit_district"=>$this->uri->segment('5'));
        $periode=$this->uri->segment('6');
    }elseif($this->uri->segment('4')=='unit'){
        $lev = "unit";
        $rdata =array("unit_code"=>$this->uri->segment('5'));
        $periode=$this->uri->segment('6');
    }

        $data['datalist'] = $this->M_report->getDeviceDataFilter($lev,$rdata,$periode);
        $data['id'] = $lev;
        $this->load->view('report/tab_devices_excel',$data);

    }

    public function result($page){
        $data['propinsi'] = $this->M_global->listPropinsi();
        $data['datalist'] = $this->M_report->getResult($page);
        $data['total'] = $this->M_report->totalResult();
        $this->template->renderpage("report/tab_result",$data);
    }

    public function resultfilter($page){
        $rdata = $this->input->post();
        unset($rdata['start_date']);
        unset($rdata['end_date']);
        if($this->input->post('start_date')){
        $periode = $this->input->post('start_date')."_".$this->input->post('end_date');
        }else{
            $periode=null;
        }
        $data['datalist'] = $this->M_report->getResultFilter($page,$rdata,$periode);
        $data['total'] = $this->M_report->totalResultFilter($rdata,$periode);
        $this->load->view('report/tab_result_data',$data);



    }

    public function resultdownload(){
        if($this->uri->segment('4')=='nasional'){
            //$lev = "nasional";
            $rdata =null;
            $periode=$this->uri->segment('5');
        }elseif($this->uri->segment('4')=='daerah'){
         // $lev = "daerah";
          $rdata =array("unit_province"=>$this->uri->segment('5'));
          $periode=$this->uri->segment('6');
      }elseif($this->uri->segment('4')=='kabupaten'){
         // $lev = "kabupaten";
          $rdata =array("unit_district"=>$this->uri->segment('5'));
          $periode=$this->uri->segment('6');
      }elseif($this->uri->segment('4')=='unit'){
          //$lev = "unit";
          $rdata =array("host_unit_code"=>$this->uri->segment('5'));
          $periode=$this->uri->segment('6');
      }else{
        $rdata =null;
        $periode=null;
      }
        $data['datalist'] = $this->M_report->getResultDownload($rdata,$periode);
        $this->load->view("report/tab_result_excel",$data);
    }


    public function result_chart(){
        $rdata = $this->input->post();
        $data['datalist'] = $this->M_report->getResultChartList($rdata);
        $data['id'] = $this->input->post('id_date');
        $this->load->view('report/tab_result_list',$data);
    }

    public function result_chart_download(){
        if($this->uri->segment('4')=='nasional'){
        $rdata = array("id_date"=>$this->uri->segment('5'));
        }elseif($this->uri->segment('4')=='daerah'){
            $rdata = array("id_date"=>$this->uri->segment('6'),"unit_province"=>$this->uri->segment('5'));
        }elseif($this->uri->segment('4')=='kabupaten'){
            $rdata = array("id_date"=>$this->uri->segment('6'),"unit_district"=>$this->uri->segment('5'));

        }elseif($this->uri->segment('4')=='unit'){
            $rdata = array("id_date"=>$this->uri->segment('6'),"host_unit_code"=>$this->uri->segment('5'));
        }
        $data['datalist'] = $this->M_report->getResultChartList($rdata);
        $data['id'] = $this->uri->segment('6');
        $this->load->view('report/tab_result_list_excel',$data);
    }




    public function logistik(){
        $data['propinsi'] = $this->M_global->listPropinsi();
        $data['datalist'] = $this->M_report->getCartridge();
        $this->template->renderpage("report/tab_cartridge",$data);
    }

    public function logistikfilter(){

        $rdata = $this->input->post();
        unset($rdata['start_date']);
        unset($rdata['end_date']);
        if($this->input->post('start_date')){
        $periode = $this->input->post('start_date')."_".$this->input->post('end_date');
        }else{
            $periode=null;
        }
        $data['datalist'] = $this->M_report->getCartridgeFilter($rdata,$periode);
        $this->load->view("report/tab_cartridge_data",$data);
    }

    public function logistikdownload(){
        if($this->uri->segment('4')=='nasional'){
            //$lev = "nasional";
            $rdata =null;
            $periode=$this->uri->segment('5');
        }elseif($this->uri->segment('4')=='daerah'){
         // $lev = "daerah";
          $rdata =array("unit_province"=>$this->uri->segment('5'));
          $periode=$this->uri->segment('6');
      }elseif($this->uri->segment('4')=='kabupaten'){
         // $lev = "kabupaten";
          $rdata =array("unit_district"=>$this->uri->segment('5'));
          $periode=$this->uri->segment('6');
      }elseif($this->uri->segment('4')=='unit'){
          //$lev = "unit";
          $rdata =array("unit_code"=>$this->uri->segment('5'));
          $periode=$this->uri->segment('6');
      }else{
        $rdata =null;
        $periode=null;
      }
        $data['datalist'] = $this->M_report->getCartridgeDownload($rdata,$periode);
        $this->load->view("report/tab_cartridge_excel",$data);
    }



    


    public function logistikbulanan($id){
        $data['datalist'] = $this->M_report->getCartridgeBulanan($id);
        $this->template->renderpage("report/tab_cartridge_bulanan",$data);
    }

    public function logistikbulananDownload($id){
        $data['datalist'] = $this->M_report->getCartridgeBulanan($id);
        $this->load->view("report/tab_cartridge_bulanan_excel",$data);
    }


}