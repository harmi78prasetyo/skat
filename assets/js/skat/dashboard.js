function getDetail(id,url){
    $('#areadata').load(url,{"monthyear":id});
    $('#modalData').modal('show');

  }

  function changeChart(id,url){
    FusionCharts(id).setJSONUrl(url);
  }

  function changeAttr(id,args){
    FusionCharts(id).setChartAttribute(args);
  }