<div class="panel-header panel-header-sm">

      </div>

      <div class="row">
          <div class="col-md-12">
            <div class="card">

            <div class="card-header">
                <h5 class="title">Tabulasi Data</h5>
                <p class="category">Tabel data per perangkat TCM</p>
              </div>


              <div class="card-body all-icons">
                <div class="row">
                <form>
  <select name="province_code" id="province_code">
    <option value="">-- Propinsi --</option>
    <?php foreach($propinsi as $lp){?>
      <?php if($this->session->userdata("user_group")=='2' || $this->session->userdata("user_group")=='1' ){?>
        <option value="<?php echo $lp->province_code;?>"><?php echo $lp->province_name;?></option>
     
      <?php }else{ ?> 
        <option value="<?php echo $lp->province_code;?>" <?php if($this->session->userdata("user_province")==$lp->province_code){?> selected="selected" <?php }else{?> disabled="disabled"<?php }?>><?php echo $lp->province_name;?></option>
       
        <?php }?>
      <?php }?>
  </select>
  <select name="district_code" id="district_code">
  <option value="">-- Kab/Kota --</option>
    <?php if($this->session->userdata("user_group")>='3') { 
      foreach($kabupaten as $lkab){
      ?> 
      <option value="<?php echo $lkab->district_code;?>"  <?php if($this->session->userdata("user_district")==$lkab->district_code){?> selected="selected" <?php };?>><?php echo $lkab->district_name;?></option>
      
      <?php }  }?>
 
  </select>
  <select name="unit_code" id="unit_code">
    <option value="">-- Fasyankes --</option>
    <?php if($this->session->userdata("user_group")>='4') { 
      foreach($unit as $lunit){
      ?> 
      <option value="<?php echo $lunit->unit_code;?>"  <?php if($this->session->userdata("user_unit")==$lunit->unit_code){?> selected="selected" <?php };?>><?php echo $lunit->unit_name;?></option>
      
      <?php }  }?>

  </select>
  <input type="text" name="start_date" id="start_date" placeholder="Periode" class="tanggal"> s/d <input type="text" name="end_date" id="end_date" placeholder="Periode" class="tanggal" disabled="disabled">
</form>
              </div>
            <div class="row">




<table class="table table-stripe">
<thead>
<tr>
    <td scope="col"  class="text-center">No.</td>
    <td scope="col"  class="text-center">Propinsi</td>
    <td scope="col"  class="text-center">Kabupaten</td>
    <td scope="col"  class="text-center">Nama Lab</td>
    <td scope="col"  class="text-center">Perangkat TCM</td>
    <td scope="col"  class="text-center">
                        Upload Terakhir
                      </td>
                      <td scope="col"  class="text-center">
                        Total
                      </td>
                      <td  scope="col" class="text-center">
                        MTB-
                      </td>
                      <td scope="col"   class="text-center">MTB+ RS</td>
                      <td scope="col"  class="text-center">MTB+ RR</td>
                      <td scope="col"  class="text-center">Invalid</td>
                      <td scope="col"  class="text-center">Error</td>
                      <td  scope="col" class="text-center">No Result</td>
                      </tr>
</thead>
<tbody id="dataBody">
    <?php 
    $i=1;
    foreach($datalist as $list){ ?>
    <tr>
    <th scope="row" class="text-right"><?php echo $i;?></th>
    <td scope="col" class="text-left"><?php echo $this->M_global->namaPropinsi("$list->unit_province");?></td>
    <td scope="col" class="text-left"><?php echo $this->M_global->namaKabupaten("$list->unit_district")?></td>

    <td scope="col" class="text-left"><?php echo $list->unit_name?></td>
    <td scope="col" class="text-left"><?php echo $list->host_name?></td>
    <td scope="col" class="text-center"><?php echo $list->last_date?></td>
    <td scope="col" class="text-right"><?php echo $list->total?></td>
    <td scope="col" class="text-right"><?php echo $list->mtb_neg?></td>
    <td scope="col" class="text-right"><?php echo $list->mtb_pos_rs?></td>
    <td scope="col" class="text-right"><?php echo $list->mtb_pos_rr?></td>
    <td scope="col" class="text-right"><?php echo $list->mtb_invalid?></td> 
    <td scope="col" class="text-right"><?php echo $list->mtb_error?></td>
    <td scope="col" class="text-right"><?php echo $list->mtb_noresult?></td>
    
    
</tr>

    <?php $i++;
} ?>
</tbody>
</table>

            </div>
              </div>
              <div class="card-footer">

              <a href="<?php echo base_url()."report/tabulasi/tabdevicesdownload/nasional";?>" id="linkDownload"><div class="btn btn-block"><i class="fas fa-download fa-2x"></i>&nbsp;Download</div></a>
              </div>
            </div>
          </div>
</div>


<script>
  $('document').ready(function(){
    $('#start_date').datepicker({
  format:"yyyy-mm-dd",
  startView:"year",
  minView:"year",
  maxDate:"0d"
}).on('changeDate',function(ev){
  $(this).blur();
  $(this).datepicker('hide');
$('#end_date').removeAttr('disabled');
if($('#end_date').val()!=''){
  var periode = $('#start_date').val()+"_"+$('#end_date').val();
if($('#province_code').val()==''){
  $('#dataBody').load("<?php echo base_url()."report/tabulasi/tabdevicesfilter/nasional";?>",{"start_date":$('#start_date').val(),"end_date":$('#end_date').val()});
  $('#linkDownload').attr("href","<?php echo base_url()."report/tabulasi/tabdevicesdownload/daerah/";?>"+periode);
 
}else if($('#district_code').val()=='' && $('#province_code').val()!=''){
  $('#dataBody').load("<?php echo base_url()."report/tabulasi/tabdevicesfilter/daerah/";?>",{"unit_province":$("#province_code").val(),"start_date":$('#start_date').val(),"end_date":$('#end_date').val()});
  $('#linkDownload').attr("href","<?php echo base_url()."report/tabulasi/tabdevicesdownload/daerah/";?>"+$("#province_code").val()+"/"+periode);
 
}else if($('#unit_code').val()=='' && $('#district_code').val()!=''){
  $('#dataBody').load("<?php echo base_url()."report/tabulasi/tabdevicesfilter/kabupaten/";?>",{"unit_district":$("#district_code").val(),"start_date":$('#start_date').val(),"end_date":$('#end_date').val()});
  $('#linkDownload').attr("href","<?php echo base_url()."report/tabulasi/tabdevicesdownload/kabupaten/";?>"+$("#district_code").val()+"/"+periode);
 
}else if($('#unit_code').val()!=''){
  $('#dataBody').load("<?php echo base_url()."report/tabulasi/tabdevicesfilter/unit/";?>",{"unit_code":$("#unit_code").val(),"start_date":$('#start_date').val(),"end_date":$('#end_date').val()});

  $('#linkDownload').attr("href","<?php echo base_url()."report/tabulasi/tabdevicesdownload/unit/";?>"+$("#unit_code").val()+"/"+periode);
}else{
  $('#dataBody').load("<?php echo base_url()."report/tabulasi/tabdevicesfilter/nasional";?>",{"start_date":$('#start_date').val(),"end_date":$('#end_date').val()});
  $('#linkDownload').attr("href","<?php echo base_url()."report/tabulasi/tabdevicesdownload/daerah/";?>"+periode);
}

}
});


$('#end_date').datepicker({
  format:"yyyy-mm-dd",
  startView:"year",
  minView:"year",
  maxDate:"0d"
}).on('changeDate',function(ev){
  $(this).blur();
  $(this).datepicker('hide');
 // alert($(this).val());
//$('#end_date').removeAttr('disabled');
var periode = $('#start_date').val()+"_"+$('#end_date').val();
if($('#province_code').val()==''){
  $('#dataBody').load("<?php echo base_url()."report/tabulasi/tabdevicesfilter/nasional";?>",{"start_date":$('#start_date').val(),"end_date":$('#end_date').val()});
  $('#linkDownload').attr("href","<?php echo base_url()."report/tabulasi/tabdevicesdownload/daerah/";?>"+periode);
 
}else if($('#district_code').val()=='' && $('#province_code').val()!=''){
  $('#dataBody').load("<?php echo base_url()."report/tabulasi/tabdevicesfilter/daerah/";?>",{"unit_province":$("#province_code").val(),"start_date":$('#start_date').val(),"end_date":$('#end_date').val()});
  $('#linkDownload').attr("href","<?php echo base_url()."report/tabulasi/tabdevicesdownload/daerah/";?>"+$("#province_code").val()+"/"+periode);
 
}else if($('#unit_code').val()=='' && $('#district_code').val()!=''){
  $('#dataBody').load("<?php echo base_url()."report/tabulasi/tabdevicesfilter/kabupaten/";?>",{"unit_district":$("#district_code").val(),"start_date":$('#start_date').val(),"end_date":$('#end_date').val()});
  $('#linkDownload').attr("href","<?php echo base_url()."report/tabulasi/tabdevicesdownload/kabupaten/";?>"+$("#district_code").val()+"/"+periode);
 
}else if($('#unit_code').val()!=''){
  $('#dataBody').load("<?php echo base_url()."report/tabulasi/tabdevicesfilter/unit/";?>",{"unit_code":$("#unit_code").val(),"start_date":$('#start_date').val(),"end_date":$('#end_date').val()});

  $('#linkDownload').attr("href","<?php echo base_url()."report/tabulasi/tabdevicesdownload/unit/";?>"+$("#unit_code").val()+"/"+periode);
}else{
  $('#dataBody').load("<?php echo base_url()."report/tabulasi/tabdevicesfilter/nasional";?>",{"start_date":$('#start_date').val(),"end_date":$('#end_date').val()});
  $('#linkDownload').attr("href","<?php echo base_url()."report/tabulasi/tabdevicesdownload/daerah/";?>"+periode);
}
 
});






$('#province_code').change(function(){
                  $.ajax({
                    url :'<?php echo base_url()."administratif/kabupaten/listkabupaten";?>',
                    type:'POST',
                    dataType:'json',
                    data:{
                      'province_code':$(this).val()
                      },
                      success:function(jdata){
                        var str ='<option value=""><?php echo $this->lang->line('district');?></option>';
                        $.each(jdata.response,function(i,item){
                          str +='<option value="'+item.district_code+'">'+item.district_name+'</option>';
                          })
                          $('#district_code').html(str);
                          }
                          })
                         // FusionCharts('lc2').dispose();
                          if($(this).val()!=''){
                            $('#dataBody').load("<?php echo base_url()."report/tabulasi/tabdevicesfilter/daerah";?>",{"unit_province":$(this).val()});
                            $('#linkDownload').attr("href","<?php echo base_url()."report/tabulasi/tabdevicesdownload/daerah/";?>"+$(this).val());
                          }else{
                            $('#dataBody').load("<?php echo base_url()."report/tabulasi/tabdevicesfilter/nasional";?>");
                            $('#linkDownload').attr("href","<?php echo base_url()."report/tabulasi/tabdevicesdownload/nasional";?>");
                          }
                  
                  
                  });


                  $('#district_code').change(function(){
                    $.ajax({
                      url:'<?php echo base_url()."administratif/fasyankes/listfaskes";?>',
                      type:'POST',
                      dataType:'json',
                      data:{
                        'unit_district':$(this).val(),
                        'unit_unitgroup_id':'4'
                      },
                      success:function(jsdata){
                        var str = '<option value="">Fasyankes</option>';
                        $.each(jsdata.response,function(i,item){
                        str +='<option value="'+item.unit_code+'">'+item.unit_name+'</option>';  
                        })
                        $('#unit_code').html(str);;
                      }
                    })
                   // FusionCharts('lc2').dispose();

                    if($(this).val()!=''){
                      $('#dataBody').load("<?php echo base_url()."report/tabulasi/tabdevicesfilter/kabupaten";?>",{"unit_district":$(this).val()});
                      $('#linkDownload').attr("href","<?php echo base_url()."report/tabulasi/tabdevicesdownload/kabupaten/";?>"+$(this).val());
                     

                     
                    }else{
                      $('#dataBody').load("<?php echo base_url()."report/tabulasi/tabdevicesfilter/daerah";?>",{"unit_province":$("#province_code").val()});                            
                      $('#linkDownload').attr("href","<?php echo base_url()."report/tabulasi/tabdevicesdownload/daerah/";?>"+$("#province_code").val());
                    }
                  });



                  $('#unit_code').change(function(){
                  //  FusionCharts('lc2').dispose();


                    if($(this).val()!=''){
                      $('#dataBody').load("<?php echo base_url()."report/tabulasi/tabdevicesfilter/daerah";?>",{"unit_code":$(this).val()});
                      $('#linkDownload').attr("href","<?php echo base_url()."report/tabulasi/tabdevicesdownload/unit/";?>"+$(this).val());
                    }else{
                      $('#dataBody').load("<?php echo base_url()."report/tabulasi/tabdevicesfilter/kabupaten";?>",{"unit_district":$("#district_code").val()});
                      $('#linkDownload').attr("href","<?php echo base_url()."report/tabulasi/tabdevicesdownload/kabupaten/";?>"+$("#district_code").val());

                    }



                  })


  })
  </script>