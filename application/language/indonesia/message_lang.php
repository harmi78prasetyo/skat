<?php
/* Language : ID */
/* text message block */

//A
//B
//C
$lang['delete_confirmation'] = "Anda yakin akan menghapus data ini?";
//D
$lang['data_save'] = "Data berhasil disimpan";
//E
$lang['error'] = "Error";
//F
$lang['failed'] = "Data gagal disimpan";
//G
//H
//I
//J
//K
//L
$lang['loginsuccess'] = "Log in berhasil";
$lang['loginfailed'] = "Log in gagal";
//M
//N
$lang['not_found'] = "Data tidak bisa dikembalikan";
//O
//P
//Q
//R
//S
$lang['success'] = "Sukses";
//T
//U
//V
//W
//X
//Y
//Z



$lang['status_pickup_waiting'] = "Paket menunggu diambil kurir";
$lang['status_pickup_confirmed'] = "telah dikonfirmasi oleh kurir";
$lang['status_pickup_allready'] = "Paket sudah diambil oleh kurir";
$lang['status_pickup_arrived'] = "Paket sudah sampai";
$lang['status_received'] = "Paket sudah diterima";
$lang['status_finish'] = "Hasil sudah tersedia"; 

$lang['notif_order_1'] = "Ada Order baru dari";
$lang['notif_order_2'] = "Permintaan telah diterima oleh kurir dan sedang melakukan pengambilan";
$lang['notif_order_3'] = "Paket sudah diambil dan dibawa oleh kurir";
$lang['notif_order_4'] = "Paket sampai tujun";
$lang['notif_order_5'] = "Paket diterima oleh";
$lang['notif_order_6'] = "Paket dengan no. order "; 
$lang['notif_order_6b']= " sudah selesai dan hasil sudah ada";
$lang['finished_confirmation'] ="Aapakah anda yakin sudah selesai?";
$lang['specimen_form'] = "Form Spesimen";
$lang['network_destination'] = "Faskes Penerima";
$lang['delete_success'] = "Hapus data berhasil";
$lang['delete_failed'] = "Hapus data gagal";

