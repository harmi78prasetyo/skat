<link href="<?php echo base_url()."assets/css/table2.css";?>" rel="stylesheet" />
<input type="hidden" id="level" value="absolut">
<input type="hidden" id="idname" value="lc1">
<form>
  <select name="province_code" id="province_code">
    <option value="">-- Propinsi --</option>
    <?php foreach($propinsi as $lp){?>
      <?php if($this->session->userdata("user_group")=='2' || $this->session->userdata("user_group")=='1' ){?>
        <option value="<?php echo $lp->province_code;?>"><?php echo $lp->province_name;?></option>
     
      <?php }else{ ?> 
        <option value="<?php echo $lp->province_code;?>" <?php if($this->session->userdata("user_province")==$lp->province_code){?> selected="selected" <?php }else{?> disabled="disabled"<?php }?>><?php echo $lp->province_name;?></option>
       
        <?php }?>
      <?php }?>
  </select>
  <select name="district_code" id="district_code">
  <option value="">-- Kab/Kota --</option>
    <?php if($this->session->userdata("user_group")>='3') { 
      foreach($kabupaten as $lkab){
      ?> 
      <option value="<?php echo $lkab->district_code;?>"  <?php if($this->session->userdata("user_district")==$lkab->district_code){?> selected="selected" <?php };?>><?php echo $lkab->district_name;?></option>
      
      <?php }  }?>
 
  </select>
  <select name="unit_code" id="unit_code">
    <option value="">-- Fasyankes --</option>
    <?php if($this->session->userdata("user_group")>='4') { 
      foreach($unit as $lunit){
      ?> 
      <option value="<?php echo $lunit->unit_code;?>"  <?php if($this->session->userdata("user_unit")==$lunit->unit_code){?> selected="selected" <?php };?>><?php echo $lunit->unit_name;?></option>
      
      <?php }  }?>

  </select>
  <input type="text" name="start_date" id="start_date" placeholder="Periode" class="tanggal"> s/d <input type="text" name="end_date" id="end_date" placeholder="Periode" class="tanggal" disabled="disabled">
</form>
<div class="panel-header panel-header-lg" style="margin-top:50px">
        <div id="headchart">

        </div>
      </div>


      <div class="row">
          <div class="col-lg-6 col-md-6">
            <div class="card card-chart">
              <div class="card-header">
              <div class="dropdown">
                  <button type="button" class="btn btn-round btn-outline-default dropdown-toggle btn-simple btn-icon no-caret" data-toggle="dropdown">
                    <i class="now-ui-icons loader_gear"></i>
                  </button>
                  <div class="dropdown-menu dropdown-menu-right">
                    <a class="dropdown-item" href="#" id="persen_res">Dalam Persen</a>
                    <a class="dropdown-item" href="#" id="absolut_res">Dalam Absolut</a>
                    <a class="dropdown-item" href="#" id="perprop_res">Tampilkan Per Propinsi</a>
                    <a class="dropdown-item" href="#" id="perkab_res">Tampilkan Per Kabupaten</a>
                    <a class="dropdown-item" href="#" id="perlab_res">Tampilkan Per Lab</a>
                  </div>
                </div>

              </div>
              <div class="card-body">
                <div  id="resultchart">
                
                </div>
              </div>
              <div class="card-footer">
              &nbsp;
              </div>
            </div>
          </div>


          <div class="col-lg-6 col-md-6">
            <div class="card card-chart">
              <div class="card-header">
                <h5 class="card-category"><?php echo date("d/m/Y");?></h5>
                <div class="dropdown">
               
                </div>
              </div>
              <div class="card-body">
                <div  id="pieresult">
                   
                  
                </div>
              </div>
              <div class="card-footer">
               
              </div>
            </div>
          </div>
         

        </div>

        <div class="row">


          <div class="col-lg-6 col-md-6">
            <div class="card card-chart">
              <div class="card-header">
                <h5 class="card-category"><?php echo date("d/m/Y");?></h5>
                <div class="dropdown">
                
                </div>
              </div>
              <div class="card-body">
                <div  id="error_rate">
                   
                  
                </div>
              </div>
              <div class="card-footer">
               
              </div>
            </div>
          </div>


          
          <div class="col-lg-6 col-md-6">
            <div class="card card-chart">
              <div class="card-header">
                <h5 class="card-category"><?php echo date("d/m/Y");?></h5>
                <div class="dropdown">
                
                </div>
              </div>
              <div class="card-body">
                <div  id="konektifitas">
                   
                  
                </div>
              </div>
              <div class="card-footer">
               
              </div>
            </div>
          </div>



        </div>






        <div class="modal fade" id="modalData">
    <div class="modal-dialog modal-dialog-centered modal-dialog-scrollable modal-xl" style="width: 100%;">
      <div class="modal-content">
      
        <!-- Modal Header -->
        <div class="modal-header bg-merah">
          <h6 class="modal-title"><i class="fas fa-database"></i>&nbsp;Detail</h6>
          <button type="button" class="close" data-dismiss="modal">&times;</button>
        </div>
        
        <!-- Modal body -->
        <div class="modal-body" id="areadata">
          

        </div>
        <div class="modal-footer">
          <a id="download" href="#"><div class="btn btn-success btn-block"><h6><i class="fas fa-download fa-2x"></i>&nbsp;Download</h6></div></a>
        </div>
      </div>
    </div>
</div>
<script src="<?php echo base_url()."assets/js/skat/dashboard.js";?>"></script>

        <script>
          function getDetail(id){

            var sp = id.split('_');
            if(sp[0]=='nasional'){
              var iddate = sp[1];
              var kode = null;
              $('#areadata').load('<?php echo base_url()."report/tabulasi/result_chart";?>',{"id_date":iddate});
              $('#download').attr("href","<?php echo base_url()."report/tabulasi/result_chart_download/nasional/";?>"+iddate);
            }else if(sp[0]=='daerah'){
              var iddate = sp[2];
              var kode = sp[1];
              $('#areadata').load('<?php echo base_url()."report/tabulasi/result_chart";?>',{"id_date":iddate,"unit_province":kode});
              $('#download').attr("href","<?php echo base_url()."report/tabulasi/result_chart_download/daerah/";?>"+kode+"/"+iddate);
            }else if(sp[0]=='kabupaten'){
              var iddate = sp[2];
              var kode = sp[1];
              $('#areadata').load('<?php echo base_url()."report/tabulasi/result_chart";?>',{"id_date":iddate,"unit_district":kode});
              $('#download').attr("href","<?php echo base_url()."report/tabulasi/result_chart_download/kabupaten/";?>"+kode+"/"+iddate);
            }else if(sp[0]=='unit'){
              var iddate = sp[2];
              var kode = sp[1];
              $('#areadata').load('<?php echo base_url()."report/tabulasi/result_chart";?>',{"id_date":iddate,"host_unit_code":kode});
              $('#download').attr("href","<?php echo base_url()."report/tabulasi/result_chart_download/unit/";?>"+kode+"/"+iddate);
            }


           
            
           
            $('#modalData').modal('show');


          }

         
            $('document').ready(function(){

              var group = "<?php echo $this->session->userdata("user_group");?>";

             // $('#absolut_res').hide()
               // $('#perprop_res').hide();
               if(group=='1' || group=='2'){
              scrollLine('<?php echo base_url()."chart/chart_examtrend/nasional";?>','headchart','lc1','100%','300')
              stackChart('<?php echo base_url()."chart/chartresult/nasional";?>','resultchart','lc2','90%','400');
              pieChart('<?php echo base_url()."chart/chartpieresult/nasional";?>','pieresult','lc3','100%','400');
              barChart('<?php echo base_url()."chart/chartoffline/nasional";?>','konektifitas','lc4','100%','500');
              barChart('<?php echo base_url()."chart/charterror/nasional";?>','error_rate','lc5','100%','500');
               }else if(group=='3'){

                scrollLine('<?php echo base_url()."chart/chart_examtrend/daerah/".$this->session->userdata("user_province");?>','headchart','lc1','100%','300')
              stackChart('<?php echo base_url()."chart/chartresult/daerah/".$this->session->userdata("user_province");?>','resultchart','lc2','90%','400');
              pieChart('<?php echo base_url()."chart/chartpieresult/daerah/".$this->session->userdata("user_province");?>','pieresult','lc3','100%','400');
              barChart('<?php echo base_url()."chart/chartoffline/daerah/".$this->session->userdata("user_province");?>','konektifitas','lc4','100%','500');
              barChart('<?php echo base_url()."chart/charterror/daerah/".$this->session->userdata("user_province");?>','error_rate','lc5','100%','500');

               }else if(group=='4'){

                scrollLine('<?php echo base_url()."chart/chart_examtrend/kabupaten/".$this->session->userdata("user_district");?>','headchart','lc1','100%','300')
              stackChart('<?php echo base_url()."chart/chartresult/kabupaten/".$this->session->userdata("user_district");?>','resultchart','lc2','90%','400');
              pieChart('<?php echo base_url()."chart/chartpieresult/kabupaten/".$this->session->userdata("user_district");?>','pieresult','lc3','100%','400');
              barChart('<?php echo base_url()."chart/chartoffline/kabupaten/".$this->session->userdata("user_district");?>','konektifitas','lc4','100%','500');
              barChart('<?php echo base_url()."chart/charterror/kabupaten/".$this->session->userdata("user_district");?>','error_rate','lc5','100%','500');


               }else if(group=='5'){

scrollLine('<?php echo base_url()."chart/chart_examtrend/unit/".$this->session->userdata("user_unit");?>','headchart','lc1','100%','300')
stackChart('<?php echo base_url()."chart/chartresult/unit/".$this->session->userdata("user_unit");?>','resultchart','lc2','90%','400');
pieChart('<?php echo base_url()."chart/chartpieresult/unit/".$this->session->userdata("user_unit");?>','pieresult','lc3','100%','400');
barChart('<?php echo base_url()."chart/chartoffline/unit/".$this->session->userdata("user_unit");?>','konektifitas','lc4','100%','500');
barChart('<?php echo base_url()."chart/charterror/unit/".$this->session->userdata("user_unit");?>','error_rate','lc5','100%','500');


}

  $('#start_date').datepicker({
  format:"yyyy-mm-dd",
  startView:"year",
  minView:"year",
  maxDate:"0d"
}).on('changeDate',function(ev){
  $(this).blur();
  $(this).datepicker('hide');
$('#end_date').removeAttr('disabled');
if($('#end_date').val()!=''){
  if($('#province_code').val()==''){
  changeChart('lc1','<?php echo base_url()."chart/chart_examtrend/nasional/";?>'+$('#start_date').val()+'_'+$('#end_date').val())
  changeChart('lc2','<?php echo base_url()."chart/chartresult/nasional/";?>'+$('#start_date').val()+'_'+$('#end_date').val())
  changeChart('lc3','<?php echo base_url()."chart/chartpieresult/nasional/";?>'+$('#start_date').val()+'_'+$('#end_date').val())
  
}else if($('#district_code').val()=='' && $('#province_code').val()!=''){
  changeChart('lc1','<?php echo base_url()."chart/chart_examtrend/daerah/";?>'+$('#province_code').val()+'/'+$('#start_date').val()+'_'+$('#end_date').val())
  changeChart('lc2','<?php echo base_url()."chart/chartresult/daerah/";?>'+$('#province_code').val()+'/'+$('#start_date').val()+'_'+$('#end_date').val())
  changeChart('lc3','<?php echo base_url()."chart/chartpieresult/daerah/";?>'+$('#province_code').val()+'/'+$('#start_date').val()+'_'+$('#end_date').val())

  
}else if($('#unit_code').val()=='' && $('#district_code').val()!=''){
  changeChart('lc1','<?php echo base_url()."chart/chart_examtrend/kabupaten/";?>'+$('#district_code').val()+'/'+$('#start_date').val()+'_'+$('#end_date').val())
  changeChart('lc2','<?php echo base_url()."chart/chartresult/kabupaten/";?>'+$('#district_code').val()+'/'+$('#start_date').val()+'_'+$('#end_date').val())
  changeChart('lc3','<?php echo base_url()."chart/chartpieresult/kabupaten/";?>'+$('#district_code').val()+'/'+$('#start_date').val()+'_'+$('#end_date').val())

}else if($('#unit_code').val()!=''){

  changeChart('lc1','<?php echo base_url()."chart/chart_examtrend/unit/";?>'+$('#unit_code').val()+'/'+$('#start_date').val()+'_'+$('#end_date').val())
  changeChart('lc2','<?php echo base_url()."chart/chartresult/unit/";?>'+$('#unit_code').val()+'/'+$('#start_date').val()+'_'+$('#end_date').val())
  changeChart('lc3','<?php echo base_url()."chart/chartpieresult/unit/";?>'+$('#unit_code').val()+'/'+$('#start_date').val()+'_'+$('#end_date').val())


  
}else{
  changeChart('lc1','<?php echo base_url()."chart/chart_examtrend/nasional/";?>'+$('#start_date').val()+'_'+$('#end_date').val())
  changeChart('lc2','<?php echo base_url()."chart/chartresult/nasional/";?>'+$('#start_date').val()+'_'+$('#end_date').val())
  changeChart('lc3','<?php echo base_url()."chart/chartpieresult/nasional/";?>'+$('#start_date').val()+'_'+$('#end_date').val())
}

}
});


$('#end_date').datepicker({
  format:"yyyy-mm-dd",
  startView:"year",
  minView:"year",
  maxDate:"0d"
}).on('changeDate',function(ev){
  $(this).blur();
  $(this).datepicker('hide');
 // alert($(this).val());
//$('#end_date').removeAttr('disabled');
if($('#province_code').val()==''){
  changeChart('lc1','<?php echo base_url()."chart/chart_examtrend/nasional/";?>'+$('#start_date').val()+'_'+$('#end_date').val())
  changeChart('lc2','<?php echo base_url()."chart/chartresult/nasional/";?>'+$('#start_date').val()+'_'+$('#end_date').val())
  changeChart('lc3','<?php echo base_url()."chart/chartpieresult/nasional/";?>'+$('#start_date').val()+'_'+$('#end_date').val())
  
}else if($('#district_code').val()=='' && $('#province_code').val()!=''){
  changeChart('lc1','<?php echo base_url()."chart/chart_examtrend/daerah/";?>'+$('#province_code').val()+'/'+$('#start_date').val()+'_'+$('#end_date').val())
  changeChart('lc2','<?php echo base_url()."chart/chartresult/daerah/";?>'+$('#province_code').val()+'/'+$('#start_date').val()+'_'+$('#end_date').val())
  changeChart('lc3','<?php echo base_url()."chart/chartpieresult/daerah/";?>'+$('#province_code').val()+'/'+$('#start_date').val()+'_'+$('#end_date').val())

  
}else if($('#unit_code').val()=='' && $('#district_code').val()!=''){
  changeChart('lc1','<?php echo base_url()."chart/chart_examtrend/kabupaten/";?>'+$('#district_code').val()+'/'+$('#start_date').val()+'_'+$('#end_date').val())
  changeChart('lc2','<?php echo base_url()."chart/chartresult/kabupaten/";?>'+$('#district_code').val()+'/'+$('#start_date').val()+'_'+$('#end_date').val())
  changeChart('lc3','<?php echo base_url()."chart/chartpieresult/kabupaten/";?>'+$('#district_code').val()+'/'+$('#start_date').val()+'_'+$('#end_date').val())

}else if($('#unit_code').val()!=''){

  changeChart('lc1','<?php echo base_url()."chart/chart_examtrend/unit/";?>'+$('#unit_code').val()+'/'+$('#start_date').val()+'_'+$('#end_date').val())
  changeChart('lc2','<?php echo base_url()."chart/chartresult/unit/";?>'+$('#unit_code').val()+'/'+$('#start_date').val()+'_'+$('#end_date').val())
  changeChart('lc3','<?php echo base_url()."chart/chartpieresult/unit/";?>'+$('#unit_code').val()+'/'+$('#start_date').val()+'_'+$('#end_date').val())


  
}else{
  changeChart('lc1','<?php echo base_url()."chart/chart_examtrend/nasional/";?>'+$('#start_date').val()+'_'+$('#end_date').val())
  changeChart('lc2','<?php echo base_url()."chart/chartresult/nasional/";?>'+$('#start_date').val()+'_'+$('#end_date').val())
  changeChart('lc3','<?php echo base_url()."chart/chartpieresult/nasional/";?>'+$('#start_date').val()+'_'+$('#end_date').val())
}


});




               

                $('#persen_res').click(function(){
                    $('#level').val('persen');
                   changeAttr('lc2',{ "stack100percent": "1","showpercentvalues": "1","showvalues":"1","showsum":"0"});
                    $(this).hide();
                   $('#absolut_res').show();
                });

                $('#absolut_res').click(function(){
                    $('#level').val('absolut');
                    changeAttr('lc2',{ "stack100percent": "0","showpercentvalues": "0","showvalues":"1","showsum":"0"});
                    $(this).hide();
                   $('#persen_res').show();
                })

                
                $('#province_code').change(function(){
                  $.ajax({
                    url :'<?php echo base_url()."administratif/kabupaten/listkabupaten";?>',
                    type:'POST',
                    dataType:'json',
                    data:{
                      'province_code':$(this).val()
                      },
                      success:function(jdata){
                        var str ='<option value=""><?php echo $this->lang->line('district');?></option>';
                        $.each(jdata.response,function(i,item){
                          str +='<option value="'+item.district_code+'">'+item.district_name+'</option>';
                          })
                          $('#district_code').html(str);
                          }
                          })
                         // FusionCharts('lc2').dispose();
                          if($(this).val()!=''){

                            changeChart('lc1','<?php echo base_url()."chart/chart_examtrend/daerah/";?>'+$(this).val())
                            changeChart('lc2','<?php echo base_url()."chart/chartresult/daerah/";?>'+$(this).val())
                            changeChart('lc3','<?php echo base_url()."chart/chartpieresult/daerah/";?>'+$(this).val())
                            changeChart('lc4','<?php echo base_url()."chart/chartoffline/daerah/";?>'+$(this).val())
                            changeChart('lc5','<?php echo base_url()."chart/charterror/daerah/";?>'+$(this).val())
                            

                        

                          }else{
                            
                            changeChart('lc1','<?php echo base_url()."chart/chart_examtrend/nasional";?>')
                            changeChart('lc2','<?php echo base_url()."chart/chartresult/nasional";?>')
                            changeChart('lc3','<?php echo base_url()."chart/chartpieresult/nasional";?>')
                            changeChart('lc4','<?php echo base_url()."chart/chartoffline/nasional";?>')
                            changeChart('lc5','<?php echo base_url()."chart/charterror/nasional";?>')
                          }
                  
                  
                  });


                  $('#district_code').change(function(){
                    $.ajax({
                      url:'<?php echo base_url()."administratif/fasyankes/listfaskes";?>',
                      type:'POST',
                      dataType:'json',
                      data:{
                        'unit_district':$(this).val(),
                        'unit_unitgroup_id':'4'
                      },
                      success:function(jsdata){
                        var str = '<option value="">Fasyankes</option>';
                        $.each(jsdata.response,function(i,item){
                        str +='<option value="'+item.unit_code+'">'+item.unit_name+'</option>';  
                        })
                        $('#unit_code').html(str);;
                      }
                    })
                   // FusionCharts('lc2').dispose();

                    if($(this).val()!=''){
                     

                      changeChart('lc1','<?php echo base_url()."chart/chart_examtrend/kabupaten/";?>'+$(this).val())
                            changeChart('lc2','<?php echo base_url()."chart/chartresult/kabupaten/";?>'+$(this).val())
                            changeChart('lc3','<?php echo base_url()."chart/chartpieresult/kabupaten/";?>'+$(this).val())
                            changeChart('lc4','<?php echo base_url()."chart/chartoffline/kabupaten/";?>'+$(this).val())
                            changeChart('lc5','<?php echo base_url()."chart/charterror/kabupaten/";?>'+$(this).val())
                    }else{
                      
                      changeChart('lc1','<?php echo base_url()."chart/chart_examtrend/daerah/";?>')
                            changeChart('lc2','<?php echo base_url()."chart/chartresult/daerah/";?>')
                            changeChart('lc3','<?php echo base_url()."chart/chartpieresult/daerah/";?>')
                            changeChart('lc4','<?php echo base_url()."chart/chartoffline/daerah/";?>')
                            changeChart('lc5','<?php echo base_url()."chart/charterror/daerah/";?>');
                    }
                  });



                  $('#unit_code').change(function(){
                  //  FusionCharts('lc2').dispose();


                    if($(this).val()!=''){
                      
                      changeChart('lc1','<?php echo base_url()."chart/chart_examtrend/unit/";?>'+$(this).val())
                            changeChart('lc2','<?php echo base_url()."chart/chartresult/unit/";?>'+$(this).val())
                            changeChart('lc3','<?php echo base_url()."chart/chartpieresult/unit/";?>'+$(this).val())
                            changeChart('lc4','<?php echo base_url()."chart/chartoffline/unit/";?>'+$(this).val())
                            changeChart('lc5','<?php echo base_url()."chart/charterror/unit/";?>'+$(this).val())
}else{
  changeChart('lc1','<?php echo base_url()."chart/chart_examtrend/kabupaten/";?>'+$(this).val())
                            changeChart('lc2','<?php echo base_url()."chart/chartresult/kabupaten/";?>')
                            changeChart('lc3','<?php echo base_url()."chart/chartpieresult/kabupaten/";?>')
                            changeChart('lc4','<?php echo base_url()."chart/chartoffline/kabupaten/";?>')
                            changeChart('lc5','<?php echo base_url()."chart/charterror/kabupaten/";?>')
}



                  })









            })
            </script>