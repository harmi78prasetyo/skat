<?php
Class M_report extends CI_Model{
    public function __construct()
    {
        parent::__construct();
    }

    public function getData(){
        $this->db->select("unit_name,unit_code,DATE_FORMAT(MAX(last_date),'%d/%m/%Y') as last_date");

        $this->db->select_sum("total","total");
        $this->db->select_sum("mtb_neg","mtb_neg");
        $this->db->select_sum("mtb_pos_rs","mtb_pos_rs");
        $this->db->select_sum("mtb_pos_rr","mtb_pos_rr");
        $this->db->select_sum("mtb_error","mtb_error");
        $this->db->select_sum("mtb_invalid","mtb_invalid");
        $this->db->select_sum("mtb_noresult","mtb_noresult");
        $this->db->group_by("unit_code");
        return $this->db->get("hp_view_rekap_datadetail")->result();
    }


    public function getDeviceData(){
        $this->db->select("unit_province,unit_district,unit_name,host_name,host_id,DATE_FORMAT(MAX(last_date),'%d/%m/%Y %H:%i:%s') as last_date");
        $this->db->select_sum("total","total");
        $this->db->select_sum("mtb_neg","mtb_neg");
        $this->db->select_sum("mtb_pos_rs","mtb_pos_rs");
        $this->db->select_sum("mtb_pos_rr","mtb_pos_rr");
        $this->db->select_sum("mtb_error","mtb_error");
        $this->db->select_sum("mtb_invalid","mtb_invalid");
        $this->db->select_sum("mtb_noresult","mtb_noresult");
        $this->db->group_by("host_id");
        return $this->db->get("hp_view_rekap_datadetail")->result();
    }

    public function getDeviceDataFilter($lev,$rdata=null,$periode=null){

        if($lev=='daerah'){
            $this->db->group_by("host_id,unit_province");
        }elseif($lev=='kabupaten'){
            $this->db->group_by("host_id,unit_district");
        }elseif($lev=="unit"){
            $this->db->group_by("host_id,unit_code");
        }elseif($lev=='nasional'){
            $this->db->group_by("host_id");
        }

        if($periode!=null){
        $exp = explode("_",$periode);
        $this->db->where("thnbulan between DATE_FORMAT('".$exp[0]."','%Y%m') AND DATE_FORMAT('".$exp[1]."','%Y%m')");
        }

        if($rdata!=null){
        $this->db->where($rdata);
        }

        $this->db->select("unit_province,unit_district,unit_name,host_name,host_id,DATE_FORMAT(MAX(last_date),'%d/%m/%Y') as last_date");
        $this->db->select_sum("total","total");
        $this->db->select_sum("mtb_neg","mtb_neg");
        $this->db->select_sum("mtb_pos_rs","mtb_pos_rs");
        $this->db->select_sum("mtb_pos_rr","mtb_pos_rr");
        $this->db->select_sum("mtb_error","mtb_error");
        $this->db->select_sum("mtb_invalid","mtb_invalid");
        $this->db->select_sum("mtb_noresult","mtb_noresult");
        
        return $this->db->get("hp_view_rekap_datadetail")->result();
    }


    public function getResult($page){
        $start=($page-1)*100;
        $this->db->limit(100,$start);
        $this->db->select("*");
        return $this->db->get(DB_VIEW_TAB_RESULT)->result();
    }
    public function totalResult(){
        return $this->db->count_all_results(DB_VIEW_TAB_RESULT);
    }


    public function getResultFilter($page,$rdata=null,$periode=null){
        $start=($page-1)*100;
        $this->db->limit(100,$start);

        if($periode!=null){
        $exp = explode("_",$periode);
        $this->db->where("TestEndedOn between '".$exp[0]."' AND '".$exp[1]."' ");
        }
        if($rdata!=null){
        $this->db->where($rdata);
        }
        $this->db->select("*");
        return $this->db->get(DB_VIEW_TAB_RESULT)->result();
    }

    public function totalResultFilter($rdata=null,$periode=null){
        if($periode!=null){
            $exp = explode("_",$periode);
            $this->db->where("TestEndedOn between '".$exp[0]."' AND '".$exp[1]."' ");
            }
            if($rdata!=null){
            $this->db->where($rdata);
            }
            return $this->db->count_all_results(DB_VIEW_TAB_RESULT);

    }

    

    public function getResultDownload($rdata=null,$periode=null){
       
        if($periode!=null){
        $exp = explode("_",$periode);
        $this->db->where("TestEndedOn between '".$exp[0]."' AND '".$exp[1]."' ");
        }
        if($rdata!=null){
        $this->db->where($rdata);
        }
        $this->db->select("*");
        return $this->db->get(DB_VIEW_TAB_RESULT)->result();
    }



    public function getResultMonthly($id){
        //$start=($page-1)*100;
        //$this->db->limit(100,$start);
        $this->db->where("DATE_FORMAT(TestStartedOn,'%Y%m')",$id);
        $this->db->select("*");
        return $this->db->get(DB_VIEW_EXAM_RESULT)->result();
    }
    public function getResultChartList($id){
        $this->db->where($id);
        $this->db->select("*");
        return $this->db->get(DB_VIEW_EXAM_RESULT)->result();
    }

    public function getCartridge(){
        $this->db->select("unit_name,province_name,district_name,unit_code");
        $this->db->select_sum("total");
        $this->db->from("hp_view_rekap_datadetail");
        $this->db->join("hp_master_province","unit_province=province_code");
        $this->db->join("hp_master_district","unit_district=district_code");
        $this->db->group_by("unit_name");
        return $this->db->get()->result();
    }


    public function getCartridgeFilter($rdata=null,$periode=null){

        
        if($periode!=null){
        $exp = explode("_",$periode);
        $this->db->where("thnbulan between DATE_FORMAT('".$exp[0]."','%Y%m') AND DATE_FORMAT('".$exp[1]."','%Y%m')");
        }

        if($rdata!=null){
        $this->db->where($rdata);
        }

        $this->db->select("unit_name,province_name,district_name,unit_code");
        $this->db->select_sum("total");
        $this->db->from("hp_view_rekap_datadetail");
        $this->db->join("hp_master_province","unit_province=province_code");
        $this->db->join("hp_master_district","unit_district=district_code");
        $this->db->group_by("unit_name");
        return $this->db->get()->result();
    }

    public function getCartridgeDownload($rdata=null,$periode=null){

        
        if($periode!=null){
        $exp = explode("_",$periode);
        $this->db->where("thnbulan between DATE_FORMAT('".$exp[0]."','%Y%m') AND DATE_FORMAT('".$exp[1]."','%Y%m')");
        }

        if($rdata!=null){
        $this->db->where($rdata);
        }

        $this->db->select("unit_name,province_name,district_name,unit_code");
        $this->db->select_sum("total");
        $this->db->from("hp_view_rekap_datadetail");
        $this->db->join("hp_master_province","unit_province=province_code");
        $this->db->join("hp_master_district","unit_district=district_code");
        $this->db->group_by("unit_name");
        return $this->db->get()->result();
    }


    public function getCartridgeBulanan($id){
        $this->db->select("*");
        $this->db->where("unit_code",$id);
        return $this->db->get(DB_VIEW_TAB_LOGISTIK)->result();
    }




}