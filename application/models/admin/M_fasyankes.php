<?php
Class M_fasyankes extends CI_Model{
    public function __construct()
    {
        parent::__construct();
    }

    public function getList($page){
        $start = ($page-1)*100;
        $this->db->limit(100,$start);
        $this->db->from(DB_MASTER_UNIT_TB);
        $this->db->join(DB_MASTER_UNIT_GROUP,"unitgroup_id=unit_unitgroup_id");
        $this->db->join(DB_MASTER_KABUPATEN,"unit_district=district_code");
        $this->db->join(DB_MASTER_PROPINSI,"unit_province=province_code");
        return $this->db->get()->result();
    }

    public function countAll(){
        return $this->db->count_all_results(DB_MASTER_UNIT_TB);
    }

    public function getFaskesList($kab){
        $this->db->where($kab);
        //return $this->db->get_compiled_select(DB_MASTER_UNIT_TB);
        return $this->db->get(DB_MASTER_UNIT_TB)->result();
    }

    public function detail($id){
        $this->db->where("hf_code",$id);
        $this->db->from(DB_MASTER_UNIT_TB);
        $this->db->join(DB_MASTER_UNIT_GROUP,"unitgroup_id=unit_unitgroup_id");
        $this->db->join(DB_MASTER_KABUPATEN,"unit_district=district_code");
        $this->db->join(DB_MASTER_PROPINSI,"unit_province=province_code");
        return $this->db->get()->row();
    }
}