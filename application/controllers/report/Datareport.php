<?php
Class Datareport extends CI_Controller{
    public function __construct()
    {
        parent::__construct();
        $this->load->model("M_report");
    }

    public function reportPemeriksaan(){
        $data['report'] = $this->M_report->getData();
        $this->template->renderpage("report/table_report",$data);
    }
}