<?php
header("Content-type: application/vnd-ms-excel");
header("Content-Disposition: attachment; filename=data_tabulasi_logistik_bulanan.xls");
?>

<table id="tblData" class="table" cellspacing="0" cellpadding="0">

<thead>
<tr>
   
    <th scope="col" class="text-center">Tahun</th>
    <th scope="col"  class="text-center">Jan</th>
    <th scope="col"  class="text-center">Feb</th>
    <th scope="col"  class="text-center">Mar</th>
    <th scope="col"  class="text-center">Apr</th>
    <th scope="col"  class="text-center">Mei</th>
    <th scope="col"  class="text-center">Jun</th>
    <th scope="col"  class="text-center">Jul</th>
    <th scope="col"  class="text-center">Agt</th>
    <th scope="col"  class="text-center">Sep</th>
    <th scope="col"  class="text-center">Okt</th>
    <th scope="col"  class="text-center">Nop</th>
    <th scope="col"  class="text-center">Des</th>
    <th scope="col"  class="text-center">Total</th>
   
                      </tr>
</thead>
<tbody id="dataBody">
    <?php 
    $i=1;
    foreach($datalist as $list){ ?>
    <tr>
    <th scope="row" class="text-center"><?php echo $list->tahun;?></th>
    <td scope="col" class="text-center"><?php echo $list->jan;?></td>
    <td scope="col" class="text-center"><?php echo $list->feb;?></td>
    <td scope="col" class="text-center"><?php echo $list->mar;?></td>
    <td scope="col" class="text-center"><?php echo $list->apr;?></td>
    <td scope="col" class="text-center"><?php echo $list->mei;?></td>
    <td scope="col" class="text-center"><?php echo $list->jun;?></td>
    <td scope="col" class="text-center"><?php echo $list->jul;?></td>
    <td scope="col" class="text-center"><?php echo $list->agt;?></td>
    <td scope="col" class="text-center"><?php echo $list->sep;?></td>
    <td scope="col" class="text-center"><?php echo $list->okt;?></td>
    <td scope="col" class="text-center"><?php echo $list->nop;?></td>
    <td scope="col" class="text-center"><?php echo $list->des;?></td>
    <td scope="col" class="text-center"><?php echo $list->total;?></td>
    
</tr>

    <?php $i++;
} ?>
</tbody>
</table>
