<style>
    label{
        color:red;
        margin-left: 10px;
    }
    </style>
<div class="modal fade" id="modalForm">
    <div class="modal-dialog modal-dialog-centered">
      <div class="modal-content">
      
        <!-- Modal Header -->
        <div class="modal-header bg-merah">
          <h6 class="modal-title"><i class="fas fa-database"></i>&nbsp;Ganti Password</h6>
          <button type="button" class="close" data-dismiss="modal">&times;</button>
        </div>
        
        <!-- Modal body -->
        <div class="modal-body">

<form id="resetForm">
<div class="form-row">
<div class="form-group">
<div class="col-md-12" style="margin-top: 10px;">
<input type="hidden" id="user_name" name="user_name" value="<?php echo $user;?>">
              <input type='password' class="form-control" name="user_password" id="user_password" placeholder="Password Baru" required="required">
              
</div>
              
            </div>
</div>
            <div class="form-row">

            <div class="form-group">
            <div class="col-md-12" style="margin-top: 10px;">
             
              <input type='password' class="form-control" name="user_password_confirmation" id="user_password_confirmation" required="required" placeholder="Konfirmasi Password Baru">
            </div>
            </div>

            </div>


            <div class="form-row">

<div class="form-group">
<div class="col-md-12" style="margin-top: 10px;">
 
<button name="reset" id="reset" class="btn btn-danger" type="submit">Ganti Password</button>
</div>
</div>

</div>

            

</form>


        </div>
        <div class="modal-footer">
          &nbsp;
      
        </div>
      </div>
    </div>
  </div>

  <script>
      $('document').ready(function(){
          $('#modalForm').modal({
              show:true,
              backdrop:"static",
              keyboard:false
          });

          $('#resetForm').validate({
              rules:{
                  user_password:{
                      required:true,
                      minlength:6
                  },
                  user_password_confirmation:{
                      required:true,
                      minlength:6,
                      equalTo:$('#user_pasword').val()
                  }
                },
                messages:{
                    user_password:{
                        required:"Field ini Wajib diisi",
                        minlength:"Panjang password minimal 6 karakter"
                    },
                    user_password_confirmation:{
                        required:"Field ini Wajib diisi",
                        minlength:"Panjang password minimal 6 karakter",
                        equalTo:"Konfirmasi harus sama dengan password baru"

                    }
                },
                submitHandler:function(form){
                    $.ajax({
                        url:"<?php echo base_url()."login/chpasswd";?>",
                        type:"POST",
                        dataType:"json",
                        data:{
                            "user_password":$('#user_password').val(),
                            "user_name":$('#user_name').val()
                        },
                        success:function(jdata){
                            if(jdata.status=="success"){
                               
                                $.ajax({
                                    url:"<?php echo base_url()."login/auth";?>",
                                    type:"POST",
                                    dataType:"json",
                                    data:{
                                        "password":$('#user_password').val(),
                                        "username":$('#user_name').val()
                                    },
                                    success:function(jsdata){
                                        if(jsdata.status=='success'){
                                            alert("Penggantian password berhasil");
                                            document.location= "<?php echo base_url()."home"?>";
                                            }else{
                                                alert("Password Gagal diganti");
                                                }
                                                }
                                })
                            }else{
                                alert("Password Gagal diganti");
                            }

                        }
                    })
                }
          })

         
      })
      </script>