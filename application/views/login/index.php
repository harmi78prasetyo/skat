<style>
  label{
    color:red;
    margin-left: 10px;
  }
  </style>
<body>
  <main class="d-flex align-items-center min-vh-100 py-3 py-md-0">
    <div class="container">
      <div class="card login-card">
        <div class="row no-gutters">
          <div class="col-md-5">
            <img src="<?php echo base_url()."assets/login/images/login.jpeg";?>" alt="login" class="login-card-img">
          </div>
          <div class="col-md-7">
            <div class="card-body">
              <div class="brand-wrapper">
                <img src="<?php echo base_url()."assets/login/images/logo.svg";?>" alt="logo" class="logo">
              </div>
              <p class="login-card-description">Login</p>
              <form action="#!">
                  <div class="form-group">
                    <label for="user_name" class="sr-only">Username</label>
                    <input type="text" name="user_name" id="user_name" class="form-control" placeholder="User Name">
                  </div>
                  <div class="form-group mb-4">
                    <label for="password" class="sr-only">Password</label>
                    <input type="password" name="password" id="password" class="form-control">
                  </div>
                  <input name="login" id="login" class="btn btn-block login-btn mb-4" type="button" value="Login">
                  <a href="#!" class="text-reset" id="a_reset">Lupa password?</a>
                </form>
            
            </div>
          </div>
        </div>
      </div>
      <!-- <div class="card login-card">
        <img src="assets/images/login.jpg" alt="login" class="login-card-img">
        <div class="card-body">
          <h2 class="login-card-title">Login</h2>
          <p class="login-card-description">Sign in to your account to continue.</p>
          <form action="#!">
            <div class="form-group">
              <label for="email" class="sr-only">Email</label>
              <input type="email" name="email" id="email" class="form-control" placeholder="Email">
            </div>
            <div class="form-group">
              <label for="password" class="sr-only">Password</label>
              <input type="password" name="password" id="password" class="form-control" placeholder="Password">
            </div>
            <div class="form-prompt-wrapper">
              <div class="custom-control custom-checkbox login-card-check-box">
                <input type="checkbox" class="custom-control-input" id="customCheck1">
                <label class="custom-control-label" for="customCheck1">Remember me</label>
              </div>              
              <a href="#!" class="text-reset">Forgot password?</a>
            </div>
            <input name="login" id="login" class="btn btn-block login-btn mb-4" type="button" value="Login">
          </form>
          <p class="login-card-footer-text">Don't have an account? <a href="#!" class="text-reset">Register here</a></p>
        </div>
      </div> -->
    </div>
  </main>


  <div class="modal fade" id="modalForm">
    <div class="modal-dialog modal-dialog-centered">
      <div class="modal-content">
      
        <!-- Modal Header -->
        <div class="modal-header bg-merah">
          <h6 class="modal-title"><i class="fas fa-database"></i>&nbsp;Reset Password</h6>
          <button type="button" class="close" data-dismiss="modal">&times;</button>
        </div>
        
        <!-- Modal body -->
        <div class="modal-body">

<form id="resetForm">

<div class="form-group">
              <input type="email" name="user_email" id="user_email" class="form-control" required="required" placeholder="Email Yang Terdaftar">
            </div>
            <input name="reset" id="reset" class="btn btn-block btn-danger mb-4" type="submit" value="Reset Password">

</form>


        </div>
        <div class="modal-footer">
          &nbsp;
      
        </div>
      </div>
    </div>
  </div>
  

  <script src="https://code.jquery.com/jquery-3.4.1.min.js"></script>
  <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js"></script>
  <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"></script>
  <script src="<?php echo base_url()."assets/js/jquery/jquery.validate.js";?>"></script>
</body>
</html>

<script>
$('document').ready(function(){
 
$('#a_reset').click(function(){
  $('#modalForm').modal({
    show:true,
    backdrop:"static",
    keyboard:false
  });
})

$('#resetForm').validate({
  rules:{
    user_email:{
      email:true,
      required:true,
      remote:{
        url:"<?php echo base_url()."login/checkmail";?>",
        type:"POST"
      }
    }
  },
    messages:{
      user_email:{
        required:"Ketikkan Alamat Email yang terdaftar",
        email:"Alamat Email Tidak valid",
        remote:"Alamat Email Tidak Terdaftar"
      }
    },
    submitHandler:function(form){
      $.ajax({
        url:"<?php echo base_url()."login/reset";?>",
        type:"POST",
        dataType:"json",
        data:{
          "user_email":$('#user_email').val()
        },
        success:function(jxdata){
          if(jxdata.status=="success"){
            $.ajax({
              url:"<?php echo base_url()."email/mailer/resetpassword";?>",
              type:"POST",
              dataType:"json",
              data:{
                "user_email":$('#user_email').val()
              },
              success:function(mxdata){
                if(mxdata.status=='success'){
                  alert("Password Baru sudah berhasil dikirim ke email");
                  $('#modalForm').modal('hide');
                }else{
                  alert("Password baru gagal dikirim ke email");
                }
              }
            })

          }else{
            alert("Reset Password Gagal")
          }
        }
      })
    }
  });

  $('#login').click(function(){

    $.ajax({
      url:"<?php echo base_url()."login/auth";?>",
      type:"post",
      dataType:"json",
      data:{
        "username":$('#user_name').val(),
        "password":$('#password').val()
      },
      success:function(jdata){
        if(jdata.status=='success'){
          document.location= "<?php echo base_url()."home"?>";
        }else{
          alert("Login Gagal!!");
        }
      }
    })
  
  })
})
</script>