<?php
Class Propinsi extends CI_Controller{
    public function __construct()
    {
        parent::__construct();
        $this->load->model("admin/M_propinsi");
    }

    public function index(){
        $data['propinsi'] = $this->M_propinsi->getList();
        $this->template->renderpage('admin/propinsi',$data);
    }
    public function add(){
        $rdata = $this->input->post();
        if($this->M_propinsi->insert($rdata)){
            $json['status']="success";
        }else{
            $json['status']='error';
        }
        echo json_encode($json);
    }

    public function detail($id){
        $json=$this->M_propinsi->detail($id);
        echo json_encode($json);
    }

    public function listdata(){
        $data['propinsi'] = $this->M_propinsi->getList();
        $this->load->view('admin/list_propinsi',$data);
    }

    public function search(){
        $search = $this->input->post('search');
        $data['propinsi'] = $this->M_propinsi->search($search);
        $this->load->view('admin/list_propinsi',$data);
    }

    public function delete($id){
        //$search = $this->input->post('search');
        if($this->M_propinsi->delete($id)){
        $data['propinsi'] = $this->M_propinsi->getList();
        }
        $this->load->view('admin/list_propinsi',$data);
    }

    public function update($id){
        $jdata = $this->M_propinsi->update($rdata,$id);
        if($jdata){
            $json['status']='success';
        }else{
            $json['status']="error";
        }
        echo json_encode($json);
    }

    
}