<?php
Class Images {
   
    protected $_ci;
    public function __construct(){
        $this->_ci = &get_instance();
    }

    public function resize($source,$target,$w,$h){
        $img = getimagesize($source);
        $width = $img[0];
        $height = $img[1];

        $h2 = ceil($height*($w/$width));
        $w2 = $w;


        $ext = $img['mime'];
        $path = $target;
        
    
        $newimage = imagecreatetruecolor($w2,$h2);

        switch($ext){
            // Image is a JPG
            case 'image/jpg':
            case 'image/jpeg':
                // create a jpeg extension
                $image = imagecreatefromjpeg($source);
                $etc = "jpg";
                break;
            // Image is a GIF
            case 'image/gif':
                $image = @imagecreatefromgif($source);
                $etc = "gif";
                break;
            // Image is a PNG
            case 'image/png':
                $background = imagecolorallocate($newimage, 255, 255, 255);
            imagecolortransparent($newimage, $background);
            imagealphablending($newimage, false);
            imagesavealpha($newimage, true); 
                $image = @imagecreatefrompng($source);
                $etc = "png";
                break;
            // Mime type not found
            default:
                throw new Exception("File is not an image, please use another file type.", 1);
        }
        $white = imagecolorallocate($newimage,255,255,255);

        imagecopyresampled($newimage,$image,0,0,0,0,$w2,$h2,$width,$height);

        if($etc=='jpg'){
        imagejpeg($newimage,$path,75);
        }elseif($etc=="gif"){
            imagegif($newimage,$path,75);
        }elseif($etc=="png"){
            imagepng($newimage,$path,0);
        }
        imagedestroy($newimage);
    }

    public function base64tojpg($path,$string,$target,$w,$h){
        $fop = fopen($path,"wb");
        fwrite($fop,base64_decode($string));
        fclose($fop);
        //unlink($path);
        $this->resize($path,$target,$w,$h);
        
    }
    
}