<meta charset="utf-8" />

  
<link rel="icon" type="image/png" href="<?php echo base_url()?>assets/img/skat/logoonly.ico">
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
  <title>
    SKAT - Sistem Konektivitas Alat TCM
  </title>
  <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0, shrink-to-fit=no' name='viewport' />

<link href="https://fonts.googleapis.com/css?family=Montserrat:400,700,200" rel="stylesheet" />
  <!-- <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.1/css/all.css" integrity="sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr" crossorigin="anonymous">-->
  <!-- CSS Files -->
  <link href="<?php echo base_url()."assets/css/bootstrap.min.css";?>" rel="stylesheet" />
  <link href="<?php echo base_url()."assets/css/now-ui-dashboard.css?v=1.5.0";?>" rel="stylesheet" />
  <link href="<?php echo base_url()."assets/fwa/css/all.min.css";?>" rel="stylesheet" />

  <link href="<?php echo base_url()."assets/datepicker/css/datepicker.css";?>" rel="stylesheet" />

  <script src="<?php echo base_url()."assets/js/core/jquery.min.js";?>"></script>

  
  <script src="<?php echo base_url()."assets/js/core/popper.min.js";?>"></script>
  <script src="<?php echo base_url()."assets/js/core/bootstrap.min.js";?>"></script>

  <script src="<?php echo base_url()."assets/charts/FusionCharts.js";?>"></script>
  <script src="<?php echo base_url()."assets/fwa/js/all.min.js";?>"></script>
  <script src="<?php echo base_url()."assets/js/jquery/jquery.alphanum.js";?>"></script>
  <script src="<?php echo base_url()."assets/datepicker/js/bootstrap-datepicker-id.js";?>"></script>
  <script src="<?php echo base_url()."assets/js/jquery/jquery.validate.js";?>"></script>



  <script src="<?php echo base_url()."assets/js/device.js";?>"></script>

  <script>

function mchart(ctype,url,iddiv,id,width,height){
      var lChart = new FusionCharts("<?php echo base_url()."assets/charts/";?>"+ctype,id, width, height, "0", "1");
                                lChart.setJSONUrl(url);
                                lChart.render(iddiv);

//alert(url);
    }

function stackChart(url,iddiv,id,$w,$h){
  var mchart = new FusionCharts("<?php echo base_url()."assets/chart/StackedBar2D.swf";?>",id,$w,$h,"0","1");
  mchart.setJSONUrl(url);
  mchart.render(iddiv);
}

function pieChart(url,iddiv,id,$w,$h){
  var mchart = new FusionCharts("<?php echo base_url()."assets/chart/Pie2D.swf";?>",id,$w,$h,"0","1");
  mchart.setJSONUrl(url);
  mchart.render(iddiv);
}

function barChart(url,iddiv,id,$w,$h){
  var mchart = new FusionCharts("<?php echo base_url()."assets/chart/Bar2D.swf";?>",id,$w,$h,"0","1");
  mchart.setJSONUrl(url);
  mchart.render(iddiv);
}


function scrollLine(url,iddiv,id,$w,$h){
  var mchart = new FusionCharts("<?php echo base_url()."assets/chart/scrollline2d.swf";?>",id,$w,$h,"0","1");
  mchart.setJSONUrl(url);
  mchart.render(iddiv);
}



    function formatDate(tgl){
var tg = tgl.split("-");
var dbFormat = tg[2]+"-"+tg[1]+"-"+tg[0];
return dbFormat;
}



$('.tanggal').datepicker({
  format:"dd-mm-yyyy",
  startView:"year",
  minView:"year"
}).on('changeDate',function(ev){
  $(this).blur();
  $(this).datepicker('hide');
});

  </script>

<style>.datepicker{z-index:9151 !important;}</style>