<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	https://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|	$route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples:	my-controller/index	-> my_controller/index
|		my-controller/my-method	-> my_controller/my_method
*/
$route['default_controller'] = 'login';
$route['404_override'] = '';
$route['translate_uri_dashes'] = FALSE;

/*
| -------------------------------------------------------------------------
| Sample REST API Routes
| -------------------------------------------------------------------------
*/
//$route['api/example/users/(:num)'] = 'api/example/users/id/$1'; // Example 4
//$route['api/example/users/(:num)(\.)([a-zA-Z0-9_-]+)(.*)'] = 'api/example/users/id/$1/format/$3$4'; // Example 8
/*$route['api/pasien'] = 'getdatapasien/pasien';
$route['api/fase'] = 'getdatapasien/fasepengobatan';
$route['api/auth'] = 'auth/token';
$route['api/pasienAll'] = 'getdatapasien/pasienAll';
$route['api/absensi/(:any)'] = 'getdatapasien/absensi/$1';
$route['api/listpasien']['POST'] = 'getpasien/listpasien';
$route['api/labid'] = 'unittb/laboratory';
$route['api/xpert']['POST'] = 'unittb/exam';
$route['api/xpert']['PUT'] = 'unittb/exam';
$route['api/faskes'] = 'faskes/datafaskes';
$route['api/lab/(:any)'] = 'unittb/labid/$1';
*/
$route['lang/(:any)']['GET'] = "language/switchlang/$1";
$route['login']='login/index';
$route['login/auth']['POST'] = 'login/auth';
$route['login/logout'] = 'login/logout';

$route['master/province/list'] = 'administratif/propinsi/index';
$route['master/province/add'] = 'administratif/propinsi/add';
$route['master/province/delete/(:any)'] = 'geo/province/delete/$1';
$route['master/province/update/(:any)'] = 'geo/province/update/$1';
$route['master/province/search'] = 'geo/province/search';
$route['master/province/updatelist'] = 'geo/province/datalist';


$route['master/district/list/(:num)'] = 'administratif/kabupaten/index/$1';

$route['master/district/add'] = 'geo/district/add';
$route['master/district/delete/(:any)'] = 'geo/district/delete/$1';
$route['master/district/update/(:any)'] = 'geo/district/update/$1';
$route['master/district/search'] = 'geo/district/search';
$route['master/district/updatelist'] = 'geo/district/datalist';
$route['master/district/detail'] = 'geo/district/detail';
$route['master/district/districtbyprovince'] = 'geo/district/districtbyprovince';


$route['master/subdistrict/list/(:num)'] = 'geo/subdistrict/index/$1';
$route['master/subdistrict/add'] = 'geo/subdistrict/add';
$route['master/subdistrict/delete/(:any)'] = 'geo/subdistrict/delete/$1';
$route['master/subdistrict/update'] = 'geo/subdistrict/update';
$route['master/subdistrict/search'] = 'geo/subdistrict/search';
$route['master/subdistrict/updatelist'] = 'geo/subdistrict/datalist';
$route['master/subdistrict/detail/(:any)'] = 'geo/subdistrict/detail/$1';
$route['master/subdistrict/bydistrict'] = 'geo/subdistrict/bydistrict';

$route['master/hf/list/(:num)'] = 'administratif/fasyankes/index/$1';

$route['master/hf/add'] = 'unit/healthfacility/add';
$route['master/hf/delete/(:any)'] = 'unit/healthfacility/delete/$1';
$route['master/hf/update/(:any)'] = 'unit/healthfacility/update/$1';
$route['master/hf/search'] = 'unit/healthfacility/search';
$route['master/hf/updatelist'] = 'unit/healthfacility/datalist';
$route['master/hf/detail/(:any)'] = 'unit/healthfacility/detail/$1';

$route['master/hf/hfbydistrict'] = 'unit/healthfacility/hfbydistrict';
$route['master/hf/reflist'] = 'unit/healthfacility/hfreflist';

$route['master/device/list/(:num)']='administratif/device/index/$1';
$route['master/device/newlist']='administratif/device/newlist';

$route['account/user/list/(:num)'] = 'administrator/users/index/$1';
$route['account/user/add'] = 'account/user/add';
$route['account/user/delete/(:any)'] = 'account/user/delete/$1';
$route['account/user/update/(:any)'] = 'account/user/update/$1';

$route['account/group/list'] = 'account/group/index';
$route['account/group/add'] = 'account/group/add';
$route['account/group/update/(:any)'] = 'account/group/update/$1';
$route['account/group/detail/(:any)'] = 'account/group/detail/$1';
$route['account/group/delete/(:num)'] = 'account/group/delete/$1';


$route['transactional/order/list'] = 'transactional/order/index';
$route['transactional/order/add'] = 'transactional/order/add';
$route['transactional/specimen/list/(:any)'] = 'transactional/specimen/index/$1';
$route['transactional/specimen/add'] = 'transactional/specimen/add';
$route['transactional/specimen/delete/(:any)/(:any)'] = 'transactional/specimen/delete/$1/$2';
$route['transactional/pickup/list'] = 'transactional/pickup/index';
$route['transactional/pickup/add'] = 'transactional/pickup/add';
$route['transactional/order/received'] = 'transactional/order/received';
$route['transactional/received/list/(:any)'] = 'transactional/received/index/$1';
$route['transactional/result/list'] = 'transactional/result/index';
$route['transactional/result/detail/(:any)'] = 'transactional/result/specimen/$1';

$route['network/laboratorium/list/(:num)'] = 'network/laboratorium/index/$1';
$route['network/laboratorium/detail/(:any)'] = 'network/laboratorium/detail/$1';
$route['network/laboratorium/add'] = 'network/laboratorium/add';
$route['network/laboratorium/delete/(:any)/(:any)'] = 'network/laboratorium/delete/$1/$2';
$route['network/laboratorium/new/(:any)'] = 'network/laboratorium/new/$1';

$route['dashboard/faskes'] = 'dashboard/dashboard/fasyankes';

$route['apitb/login']['POST'] = "apitb/login/index";
$route['apitb/login/logout']['POST'] = "apitb/login/logout";
$route['apitb/order/list/(:any)']['GET'] = "apitb/order/index/$1";
$route['apitb/order/confirmation/(:any)']['GET'] = "apitb/order/confirmation/$1";
$route['apitb/order/received/(:any)']['GET'] = "apitb/order/received/$1";
$route['apitb/pickup/confirmlist/(:any)/(:any)']['GET'] = 'apitb/pickup/tobeconfirm/$1/$2';
$route['apitb/pickup/confirm/(:any)']['PUT'] = 'apitb/pickup/index/$1';

$route['apitb/pickup/list/(:any)/(:any)']['GET'] = 'apitb/pickup/index/$1/$2';
$route['apitb/pickup/delivered/(:any)']['GET'] = 'apitb/pickup/delivered/$1';
$route['apitb/result/vlresult']['POST'] = 'apitb/result/vlinsert';
$route['apitb/result/list/(:any)']['GET'] = 'apitb/result/index/$1';



$route['apitb/master/province']['GET'] = 'apitb/master/province';
$route['apitb/master/district/(:any)']['GET'] = 'apitb/master/district/$1';
$route['apitb/master/subdistrict/(:any)']['GET'] = 'apitb/master/subdistrict/$1';
$route['apitb/master/hf_sender/(:any)']['GET'] = 'apitb/master/hf_sender/$1';
$route['apitb/master/hf_destination/(:any)']['GET'] = 'apitb/master/hf_destination/$1';
$route['apitb/master/shipper/(:any)']['GET'] = 'apitb/master/shipper/$1';

$route['apitb/order/add']['POST'] = "apitb/order/index";
$route['apitb/order/update/(:any)']['PUT'] = "apitb/order/index/$1";
$route['apitb/order/delete/(:any)']['DELETE'] = "apitb/order/index/$1";
$route['apitb/order/detail/(:any)']['GET'] = "apitb/order/detail/$1";
$route['apitb/specimen/list/(:any)']['GET'] = 'apitb/specimen/index/$1';
$route['apitb/specimen/add']['POST'] = 'apitb/specimen/index';
$route['apitb/specimen/delete/(:any)']['DELETE'] = 'apitb/specimen/index/$1';
$route['apitb/specimen/update/(:any)']['PUT'] = 'apitb/specimen/index/$1';
$route['apitb/specimen/detail/(:any)']['GET'] = 'apitb/specimen/detail/$1';
$route['apitb/specimen/feedback/(:any)']['GET'] = 'apitb/specimen/feedback/$1';

$route['apitb/notification/list/(:any)']['GET'] = 'apitb/notification/index/$1';
$route['apitb/notification/detail/(:any)']['GET'] = 'apitb/notification/detail/$1';
$route['apitb/notification/readall/(:any)']['PUT'] = 'apitb/notification/readall/$1';

$route['apitb/user/chpassword/(:any)']['PUT'] = 'apitb/user/chpassword/$1';