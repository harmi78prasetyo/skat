<?php
Class M_propinsi extends CI_Model{
    public function __construct()
    {
        parent::__construct();
    }

    public function getList(){
        return $this->db->get(DB_MASTER_PROPINSI)->result();

    }
    public function insert($rdata){
        return $this->db->insert(DB_MASTER_PROPINSI,$rdata);
    }
    public function detail($id){
        $this->db->where("province_code",$id);
        return $this->db->get(DB_MASTER_PROPINSI)->row();
    }

    public function search($search){
        $this->db->like("province_code",$search);
        $this->db->or_like("province_name",$search);
        return $this->db->get(DB_MASTER_PROPINSI)->result();
    }
    public function delete($id){
        return $this->db->delete(DB_MASTER_PROPINSI,array("province_code"=>$id));
    }
    
}