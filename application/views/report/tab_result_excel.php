<?php
header("Content-type: application/vnd-ms-excel");
header("Content-Disposition: attachment; filename=data_hasil_pemeriksaan_".date("YmdHis").".xls");
?>
<table class="table table-stripe">
<thead>
<tr>
    <td scope="col"><h6 class="title">No.</h6></td>
    <td scope="col"><h6 class="title">Propinsi</h6></td>
    <td scope="col"><h6 class="title">Kabupaten</h6></td>
    <td scope="col"><h6 Class="title">Nama Lab</h6></td>
    <td scope="col"><h6 Class="title">ID Pasien</h6></td>
    <td scope="col">
                        <h6  Class="title">Tanggal Pemeriksaan</h6>
                      </td>
                    
                      <td scope="col"><h6  Class="title">MTB</h6></td>
                      <td scope="col"><h6 Class="title">RR</h6></td>
                      </tr>
</thead>
<tbody id="dataBody">
    <?php 
    $i=1;
    foreach($datalist as $list){ ?>
    <tr>
    <th scope="row" class="text-center"><?php echo $i;?></th>
    <td scope="col" class="text-center"><?php echo $list->province_name;?></td>
    <td scope="col" class="text-center"><?php echo $list->district_name;?></td>
    <td scope="col" class="text-center"><?php echo $list->unit_name;?></td>
    <td scope="col" class="text-center"><?php echo $list->test_patient_id;?></td>
    <td scope="col" class="text-center"><?php echo $list->tgl_periksa;?></td>
    <td scope="col" class="text-center"><?php echo $list->mtb_result?></td>
    <td scope="col" class="text-center"><?php echo $list->rr_result;?></td>
    
    
    
</tr>

    <?php $i++;
} ?>
</tbody>