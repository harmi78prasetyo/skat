<?php
header("Content-type: application/vnd-ms-excel");
header("Content-Disposition: attachment; filename=data_tabulasi_logistik_cartridge_".date("Ymd").".xls");
?>

<table class="table table-stripe col-md-6">
<thead>
<tr>
    <td scope="col" class="text-center"><h6 class="title">No.</h6></td>
    <td scope="col" class="text-center"><h6 class="title">Propinsi</h6></td>
    <td scope="col" class="text-center"><h6 class="title">Kabupaten</h6></td>
    <td scope="col" class="text-center"><h6 Class="title">Nama Lab</h6></td>
    <td scope="col"  class="text-center"><h6 Class="title">Total</h6></td>
   
                      </tr>
</thead>
<tbody id="dataBody">
    <?php 
    $i=1;
    foreach($datalist as $list){ ?>
    <tr>
    <th scope="row" class="text-center"><?php echo $i;?></th>
    <td scope="col" class="text-center"><?php echo $list->province_name;?></td>
    <td scope="col" class="text-center"><?php echo $list->district_name;?></td>
    <td scope="col" class="text-center"><?php echo $list->unit_name;?></td>
    <td scope="col" class="text-center"><?php echo $list->total?></td>
    
</tr>

    <?php $i++;
} ?>
</tbody>
</table>