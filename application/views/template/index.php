<head>
    <?php
    echo $header;
    ?>
</head>
<body class="">
  <div class="wrapper ">
  <div class="sidebar" data-color="red">
<?php
echo $menu;
?>  
</div>  
<div class="main-panel" id="main-panel">
<?php
echo $navbar;
?>
<div class="content" style="margin-top: 100px;">
<?php
echo $content;
?>
</div>
<?php
echo $footer;
?>
</div>
</div>
</body>