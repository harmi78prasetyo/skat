<?php
Class Kabupaten extends CI_Controller{
    public function __construct()
    {
        parent::__construct();
        $this->load->model("admin/M_kabupaten");
    
    }

    public function index($page){
       $data['kabupaten'] = $this->M_kabupaten->getList($page);
        $data['total'] = $this->M_kabupaten->countAll();
        $this->template->renderpage("admin/kabupaten",$data);

    }

    public function listkabupaten(){
        $propid = $this->input->post('province_code');
        if($json['response'] = $this->M_kabupaten->getKabupaten($propid)){
            $json['status']='success';
        }else{
            $json['status']="error";
        };

        echo json_encode($json);
        
    }
}