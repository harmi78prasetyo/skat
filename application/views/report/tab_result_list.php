
 <link href="<?php echo base_url()."assets/css/theme.jui.css";?>" rel="stylesheet" />
 <script src="<?php echo base_url()."assets/js/jquery.tablesorter.js";?>"></script>
 <script src="<?php echo base_url()."assets/js/jquery.tablesorter.widgets.js";?>"></script>
 <body>
 <div id="wrapper">
<table id="keywords" cellspacing="0" cellpadding="0">

<thead>
<tr>
 
    <th scope="col">Nama Faskes</th>
    <th scope="col">ID Pasien</th>
    <th scope="col">ID Sampel</th>
    <th scope="col">Tanggal Pemeriksaan
    </th>
                    
                      <th scope="col">Pemeriksaan MTB</th>
                      <th scope="col">Pemeriksaan RR</th>
                      </tr>
</thead>
<tbody id="dataBody">
    <?php 
    $i=1;
    foreach($datalist as $list){ ?>
    <tr>
    

    <td scope="col" class="text-center lalign"><?php echo $this->M_global->namaFaskes($list->host_unit_code);?></td>
    <td scope="col" class="text-center"><?php echo $list->PatientId;?></td>
    <td scope="col" class="text-center"><?php echo $list->SampleId;?></td>
    <td scope="col" class="text-center"><?php echo $this->M_global->formatTanggal($list->TestEndedOn);?></td>
    <td scope="col" class="text-center"><?php echo $list->mtb_result?></td>
    <td scope="col" class="text-center"><?php echo $list->rr_result;?></td>
    
    
    
</tr>

    <?php $i++;
} ?>
</tbody>
</table>
 </div>
 </body>

 <script>
     $(function(){
  $('#keywords').tablesorter(); 
});
     </script>