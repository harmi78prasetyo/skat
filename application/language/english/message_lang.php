<?php
/* Language : ID */
/* text message block */

//A
//B
//C
$lang['delete_confirmation'] = "Are you sure to erase this data?";
//D
$lang['data_save'] = "Data has been saved";
//E
$lang['error'] = "Error";
//F
$lang['failed'] = "Failed";
//G
//H
//I
//J
//K
//L
$lang['loginsuccess'] = "Log in successful";
$lang['loginfailed'] = "Log in failed";
//M
//N
$lang['not_found'] = "Data cannot be retrieved";
//O
//P
//Q
//R
//S
$lang['success'] = "Successful";
//T
//U
//V
//W
//X
//Y
//Z



$lang['status_pickup_waiting'] = "Package is waiting to be picked up by courier";
$lang['status_pickup_confirmed'] = "Pickup by courier has been confirmed";
$lang['status_pickup_allready'] = "Package has been picked up by courier";
$lang['status_pickup_arrived'] = "Package arrived";
$lang['status_received'] = "Package has been received";
$lang['status_finish'] = "Test result is available"; 

$lang['notif_order_1'] = "New order request from";
$lang['notif_order_2'] = "Order requests are accepted, courier will pick up the specimen";
$lang['notif_order_3'] = "Package has been picked up and carried by courier";
$lang['notif_order_4'] = "Package arrived";
$lang['notif_order_5'] = "Package is received by";
$lang['notif_order_6'] = "Package test with order number "; 
$lang['notif_order_6b']= " is complete and test results are available";
$lang['finished_confirmation'] ="Are you sure you are done?";
$lang['specimen_form'] = "Specimen Form";
$lang['network_destination'] = "Recipient Health Facility";
$lang['delete_success'] = "delete data successfull";
$lang['delete_failed'] = "delete data unsuccessfull";

