<?php
Class M_login extends CI_Model{
    public function __construct()
    {
        parent::__construct();

    }

    public function auth($rdata){
        $this->db->where($rdata);
       // return $this->db->get_compiled_select(DB_SYS_USER);
    
        return $this->db->get(DB_SYS_USER)->row();
    }

    public function checkmail($rdata){
        $this->db->where($rdata);
        $q = $this->db->get(DB_SYS_USER);
        return $q->num_rows();
    }

    

    public function resetPassword($rdata,$email){
        $this->db->where($email);
        return $this->db->update(DB_SYS_USER,$rdata);
    }
}