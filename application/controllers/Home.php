<?php
Class Home extends CI_Controller{
    
    public function __construct(){
        parent::__construct();
        
     if(!$this->session->userdata('user_name')){
            redirect(base_url()."login");
        }elseif($this->session->userdata('user_activated')=='2'){

            redirect(base_url()."login/pwdchange");
        
        }
        

     //   $this->load->model('sys/M_module');
       // $this->load->model("transactional/M_order");
    }

    public function index(){
if($this->session->userdata("user_group")>='3'){
    $data['kabupaten'] = $this->M_global->listKabupaten($this->session->userdata("user_province"));
}

if($this->session->userdata("user_group")>='4'){
    $data['unit'] = $this->M_global->listFasyankes($this->session->userdata("user_district"));
}
$data['propinsi'] = $this->M_global->listPropinsi();
$this->template->renderpage('frontpage/dashboard',$data);
    }

}