<?php
Class Device extends CI_Controller{
    public function __construct()
    {
        parent::__construct();
        $this->load->model("admin/M_devices");
    }
    
    public function index($page){
        $data['datalist'] = $this->M_devices->getList($page);
        $data['total'] = $this->countAll();
        $this->template->renderpage("admin/devices",$data);
    }

    public function newList()
    {
        $data['propinsi'] = $this->M_global->listPropinsi();
        $data['datalist'] = $this->M_devices->getNewList();
        $this->template->renderpage("admin/newdevices",$data);
    }

    public function countAll(){
        return $this->db->count_all_results(DB_MASTER_TCMHOST);
    }


    public function activated(){
        $id = $this->input->post("host_device_id");
        $m = $this->M_devices->getInfoDevices($id);
//print_r($m);
        $rdata = array();
        $rdata = $this->input->post();
        $rdata['host_deployment_id'] = $m->DeploymentId;
        $rdata['host_hostid'] = $m->HostId;
        $rdata['host_name'] = $m->HostId;
        $rdata['host_serial'] = $m->Serial;
        $rdata['host_date_registered'] = $m->InsertedOn;

        if($this->M_devices->insert($rdata)){
            $this->M_devices->update($m->DeploymentId);
            $json['status'] ='success';
        }else{
            $json['status'] ='failed';
        }
        echo json_encode($json);

    }

    public function add(){
        $rdata = $this->input->post();
        if($this->M_devices->insert($rdata)){
            $json['status']="success";
        }else{

            $json['status'] = "error";
        }

        echo json_encode($json);
    }


}