<?php
Class Mailer extends CI_Controller{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('admin/M_user');
        $this->mail = $this->phpmailer_load->load(); 
        $this->mail->isSMTP(); 
        $this->mail->Host = $this->config->item('Host'); 
        $this->mail->SMTPAuth = true;
        $this->mail->Username = $this->config->item('Username');
        $this->mail->Password = $this->config->item('Password');
        $this->mail->SMTPSecure = 'tls';
        $this->mail->Port = 587;
        $this->mail->setFrom('noreply@skat.sitb.id', 'SKAT SITB'); 

        
    }

    public function newuser(){

       // print_r($this->config->item('smtp'));


        //die();
        $username = $this->input->post();
        $mdata = $this->M_user->detail($username);
        $this->mail->addAddress($mdata->user_email,$mdata->user_fname); 
        $this->mail->Subject = "Registrasi Pengguna Aplikasi SKAT ";
        $this->mail->msgHtml("
            <h3>REGISTRASI PENGGUNA SKAT </h3><hr>
                Kepada Yth.<br>
                ".ucwords($mdata->user_fname)."<br>
                Bersama email ini kami menginformasikan bahwa anda saat ini sudah di daftar sebagai pengguna aplikasi SKAT di subdit TB. <br><br>
                Dengan Akun Pengguna sebagai berikut : <br>
                User name : $mdata->user_name <br>
                Password  : $mdata->user_oldpassword<br><br>
                Untuk membuka aplikasi SKAT silahkan klik link berikut ini atau membukanya dengan browser :<br>
                <a href=\"".base_url()."\">".base_url()."</a> <br>

                <br>
                note: password tersebut hanya berlaku sementara, setelah anda berhasil login silahkan untuk mengganti passwordnya
                <br>
                <p>
                Terima Kasih
            "); 
 
 
        if (!$this->mail->send()) {
                   $json['status'] = "error";
                } else {
                $json['status'] = "success";
                } 

                echo json_encode($json);





    }


    public function resetpassword(){

        $email = $this->input->post();
        $mdata = $this->M_user->detail($email);
        $this->mail->addAddress($mdata->user_email,$mdata->user_fname); 
        $this->mail->Subject = "Reset Password Pengguna SKAT ";
        $this->mail->msgHtml("
            <h3>RESET PASSWORD PENGGUNA </h3><hr>
                Kepada Yth.<br>
                ".ucwords($mdata->user_fname)."<br>
                Bersama email ini kami menginformasikan bahwa password anda saat ini sudah diganti, <br><br>
                dengan detail sebagai berikut : <br>
                User name : $mdata->user_name <br>
                Password  : $mdata->user_oldpassword<br><br>
                Untuk membuka aplikasi SKAT silahkan klik link berikut ini atau membukanya dengan browser :<br>
                <a href=\"".base_url()."\">".base_url()."</a> <br>
                



                <br>
                note: password tersebut hanya berlaku sementara, setelah anda berhasil login silahkan untuk mengganti passwordnya
                <br>
                <p>
                Terima Kasih
            "); 
 
 
        if (!$this->mail->send()) {
                   $json['status'] = "error";
                } else {
                $json['status'] = "success";
                } 

                echo json_encode($json);


    }
}