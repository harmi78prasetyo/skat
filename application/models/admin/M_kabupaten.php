<?php
Class M_kabupaten extends CI_Model{
    public function __construct()
    {
        parent::__construct();
    }

    public function getList($page=1){
        $start=($page-1)*100;
        $this->db->limit(100,$start);
        $this->db->from(DB_MASTER_KABUPATEN);
        $this->db->join(DB_MASTER_PROPINSI,"province_code=district_province");
        return $this->db->get()->result();
    }

    public function countAll(){
        return $this->db->count_all_results(DB_MASTER_KABUPATEN);
    }

    public function getKabupaten($prop){
        $this->db->where("district_province",$prop);
        return $this->db->get(DB_MASTER_KABUPATEN)->result();
    }

    public function detail($idkab){
        $this->db->where("district_code",$idkab);
        $this->db->from(DB_MASTER_KABUPATEN);
        $this->db->join(DB_MASTER_PROPINSI,"province_code=district_province");
        return $this->db->get()->row();
    }


}