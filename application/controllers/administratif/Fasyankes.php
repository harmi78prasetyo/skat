<?php
Class Fasyankes extends CI_Controller{
    public function __construct()
    {
        parent::__construct();
        $this->load->model("admin/M_fasyankes");
    }

    public function index($page){
        $data['datalist'] = $this->M_fasyankes->getList($page);
        $data['total'] = $this->M_fasyankes->countAll();
        $this->template->renderpage("admin/fasyankes",$data);
    }

    public function listfaskes(){
        $rdata = $this->input->post();
        if($json['response'] = $this->M_fasyankes->getFaskesList($rdata)){
            $json['status']='success';
        }else{
            $json['status']='error';
        }

        echo json_encode($json);
    }
}