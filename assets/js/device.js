function activated(id){
    $('#host_device_id').val(id);
    $('#modalForm').modal('show');
}


function getListKab(id,target,site){
    $.ajax({
        url:site,
        type:"POST",
        dataType:"json",
        data:{
            "province_code":id
        },
        success:function(jsdata){
            var str ='<option value="">Kabupaten/Kota</option>';
            $.each(jsdata.response,function(i,item){
                str +='<option value="'+item.district_code+'">'+item.district_name+'</option>';
            })
            $(target).html(str).removeAttr('disabled');
        }
    });
}

function getListFaskes(id,target,site){
    $.ajax({
        url:site,
        type:"POST",
        dataType:"json",
        data:{
            "unit_district":id
        },
        success:function(jsdata){
            var str = '<option value="">Lab/Faskes</option>';
            $.each(jsdata.response,function(i,item){
                str +='<option value="'+item.unit_code+'">'+item.unit_name+'</option>';
            })
            $(target).html(str).removeAttr('disabled');

        }
    })
}

function submitForm(site,success){
    $.ajax({
        url:site,
        type:"POST",
        dataType:"json",
        data:{
            "host_province":$('#host_province').val(),
            "host_district":$('#host_district').val(),
            "host_unit_code":$('#host_unit_code').val(),
            "host_device_id":$('#host_device_id').val()
        },
        success:function(jdata){
            if(jdata.status=='success'){
            alert('aktivasi berhasil!');
        document.location = success;
            }else{
                alert('aktivasi gagal');
            }
        }

    })
}

