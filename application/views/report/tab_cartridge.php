<div class="panel-header panel-header-sm">
    
      </div>
<div class="row">
          <div class="col-md-12">
            <div class="card">

            <div class="card-header">
                <h5 class="title">Tabulasi Data</h5>
                <p class="category">Tabel data per penggunaan Cartridge</p>
              </div>


              <div class="card-body all-icons">

              <div class="row">
                <form>
  <select name="province_code" id="province_code">
    <option value="">-- Propinsi --</option>
    <?php foreach($propinsi as $lp){?>
      <?php if($this->session->userdata("user_group")=='2' || $this->session->userdata("user_group")=='1' ){?>
        <option value="<?php echo $lp->province_code;?>"><?php echo $lp->province_name;?></option>
     
      <?php }else{ ?> 
        <option value="<?php echo $lp->province_code;?>" <?php if($this->session->userdata("user_province")==$lp->province_code){?> selected="selected" <?php }else{?> disabled="disabled"<?php }?>><?php echo $lp->province_name;?></option>
       
        <?php }?>
      <?php }?>
  </select>
  <select name="district_code" id="district_code">
    <option value="">-- Kab/Kota --</option>
  </select>
  <select name="unit_code" id="unit_code">
    <option value="">-- Fasyankes --</option>
  </select>
  <input type="text" name="start_date" id="start_date" placeholder="Periode" class="tanggal"> s/d <input type="text" name="end_date" id="end_date" placeholder="Periode" class="tanggal" disabled="disabled">
</form>  
              </div>

            <div class="row" id="datadiv">




<table class="table table-stripe col-md-6">
<thead>
<tr>
    <td scope="col" class="text-center"><h6 class="title">No.</h6></td>
    <td scope="col" class="text-center"><h6 class="title">Propinsi</h6></td>
    <td scope="col" class="text-center"><h6 class="title">Kabupaten</h6></td>
    <td scope="col" class="text-center"><h6 Class="title">Nama Lab</h6></td>
    <td scope="col"  class="text-center"><h6 Class="title">Total</h6></td>
   
                      </tr>
</thead>
<tbody id="dataBody">
    <?php 
    $i=1;
    foreach($datalist as $list){ ?>
    <tr>
    <th scope="row" class="text-center"><?php echo $i;?></th>
    <td scope="col" class="text-center"><?php echo $list->province_name;?></td>
    <td scope="col" class="text-center"><?php echo $list->district_name;?></td>
    <td scope="col" class="text-center"><a href="<?php echo base_url()."report/tabulasi/logistikbulanan/".$list->unit_code;?>"><?php echo $list->unit_name;?></a></td>
    <td scope="col" class="text-center"><?php echo $list->total?></td>
    
</tr>

    <?php $i++;
} ?>
</tbody>
</table>

            </div>
              </div>

              <div class="card-footer">

<a href="<?php echo base_url()."report/tabulasi/logistikdownload/nasional";?>" id="linkDownload"><div class="btn btn-block"><i class="fas fa-download fa-2x"></i>&nbsp;Download</div></a>
</div>
            </div>
          </div>
</div>


<script>

  $('document').ready(function(){
    $('#province_code').change(function(){
                  $.ajax({
                    url :'<?php echo base_url()."administratif/kabupaten/listkabupaten";?>',
                    type:'POST',
                    dataType:'json',
                    data:{
                      'province_code':$(this).val()
                      },
                      success:function(jdata){
                        var str ='<option value=""><?php echo $this->lang->line('district');?></option>';
                        $.each(jdata.response,function(i,item){
                          str +='<option value="'+item.district_code+'">'+item.district_name+'</option>';
                          })
                          $('#district_code').html(str);
                          }
                          })
                         // FusionCharts('lc2').dispose();
                          if($(this).val()!=''){
                            $('#dataBody').load("<?php echo base_url()."report/tabulasi/logistikfilter/1";?>",{"unit_province":$(this).val()});
                            $('#linkDownload').attr("href","<?php echo base_url()."report/tabulasi/logistikdownload/daerah/";?>"+$(this).val());
                          }else{
                            $('#dataBody').load("<?php echo base_url()."report/tabulasi/logistikfilter/1";?>");
                            $('#linkDownload').attr("href","<?php echo base_url()."report/tabulasi/logistikdownload/nasional";?>");
                          }
                  
                  
                  });




                  $('#district_code').change(function(){
                    $.ajax({
                      url:'<?php echo base_url()."administratif/fasyankes/listfaskes";?>',
                      type:'POST',
                      dataType:'json',
                      data:{
                        'unit_district':$(this).val(),
                        'unit_unitgroup_id':'4'
                      },
                      success:function(jsdata){
                        var str = '<option value="">Fasyankes</option>';
                        $.each(jsdata.response,function(i,item){
                        str +='<option value="'+item.unit_code+'">'+item.unit_name+'</option>';  
                        })
                        $('#unit_code').html(str);;
                      }
                    })
                   // FusionCharts('lc2').dispose();

                    if($(this).val()!=''){
                      $('#dataBody').load("<?php echo base_url()."report/tabulasi/logistikfilter/1";?>",{"unit_district":$(this).val()});
                      $('#linkDownload').attr("href","<?php echo base_url()."report/tabulasi/logistikdownload/kabupaten/";?>"+$(this).val());
                     

                     
                    }else{
                      $('#dataBody').load("<?php echo base_url()."report/tabulasi/logistikfilter/1";?>",{"unit_province":$("#province_code").val()});                            
                      $('#linkDownload').attr("href","<?php echo base_url()."report/tabulasi/logistikdownload/daerah/";?>"+$("#province_code").val());
                    }
                  });



                  
                  $('#unit_code').change(function(){
                  //  FusionCharts('lc2').dispose();


                    if($(this).val()!=''){
                      $('#dataBody').load("<?php echo base_url()."report/tabulasi/logistikfilter/1";?>",{"unit_code":$(this).val()});
                      $('#linkDownload').attr("href","<?php echo base_url()."report/tabulasi/logistikdownload/unit/";?>"+$(this).val());
                    }else{
                      $('#dataBody').load("<?php echo base_url()."report/tabulasi/logistikfilter/1";?>",{"unit_district":$("#district_code").val()});
                      $('#linkDownload').attr("href","<?php echo base_url()."report/tabulasi/logistikdownload/kabupaten/";?>"+$("#district_code").val());

                    }



                  })




                  $('#start_date').datepicker({
  format:"yyyy-mm-dd",
  startView:"year",
  minView:"year",
  maxDate:"0d"
}).on('changeDate',function(ev){
  $(this).blur();
  $(this).datepicker('hide');
$('#end_date').removeAttr('disabled');
if($('#end_date').val()!=''){
  var periode = $('#start_date').val()+"_"+$('#end_date').val();
if($('#province_code').val()==''){
  $('#dataBody').load("<?php echo base_url()."report/tabulasi/logistikfilter/1";?>",{"start_date":$('#start_date').val(),"end_date":$('#end_date').val()});
  $('#linkDownload').attr("href","<?php echo base_url()."report/tabulasi/logistikdownload/daerah/";?>"+periode);
 
}else if($('#district_code').val()=='' && $('#province_code').val()!=''){
  $('#dataBody').load("<?php echo base_url()."report/tabulasi/logistikfilter/1";?>",{"unit_province":$("#province_code").val(),"start_date":$('#start_date').val(),"end_date":$('#end_date').val()});
  $('#linkDownload').attr("href","<?php echo base_url()."report/tabulasi/logistikdownload/daerah/";?>"+$("#province_code").val()+"/"+periode);
 
}else if($('#unit_code').val()=='' && $('#district_code').val()!=''){
  $('#dataBody').load("<?php echo base_url()."report/tabulasi/logistikfilter/1";?>",{"unit_district":$("#district_code").val(),"start_date":$('#start_date').val(),"end_date":$('#end_date').val()});
  $('#linkDownload').attr("href","<?php echo base_url()."report/tabulasi/logistikdownload/kabupaten/";?>"+$("#district_code").val()+"/"+periode);
 
}else if($('#unit_code').val()!=''){
  $('#dataBody').load("<?php echo base_url()."report/tabulasi/logistikfilter/1";?>",{"host_unit_code":$("#unit_code").val(),"start_date":$('#start_date').val(),"end_date":$('#end_date').val()});

  $('#linkDownload').attr("href","<?php echo base_url()."report/tabulasi/logistikdownload/unit/";?>"+$("#unit_code").val()+"/"+periode);
}else{
  $('#dataBody').load("<?php echo base_url()."report/tabulasi/logistikfilter/1";?>",{"start_date":$('#start_date').val(),"end_date":$('#end_date').val()});
  $('#linkDownload').attr("href","<?php echo base_url()."report/tabulasi/logistikdownload/daerah/";?>"+periode);
}

}
});


$('#end_date').datepicker({
  format:"yyyy-mm-dd",
  startView:"year",
  minView:"year",
  maxDate:"0d"
}).on('changeDate',function(ev){
  $(this).blur();
  $(this).datepicker('hide');
 // alert($(this).val());
//$('#end_date').removeAttr('disabled');
var periode = $('#start_date').val()+"_"+$('#end_date').val();
if($('#province_code').val()==''){
  $('#dataBody').load("<?php echo base_url()."report/tabulasi/logistikfilter/1";?>",{"start_date":$('#start_date').val(),"end_date":$('#end_date').val()});
  $('#linkDownload').attr("href","<?php echo base_url()."report/tabulasi/logistikdownload/daerah/";?>"+periode);
 
}else if($('#district_code').val()=='' && $('#province_code').val()!=''){
  $('#dataBody').load("<?php echo base_url()."report/tabulasi/logistikfilter/1";?>",{"unit_province":$("#province_code").val(),"start_date":$('#start_date').val(),"end_date":$('#end_date').val()});
  $('#linkDownload').attr("href","<?php echo base_url()."report/tabulasi/logistikdownload/daerah/";?>"+$("#province_code").val()+"/"+periode);
 
}else if($('#unit_code').val()=='' && $('#district_code').val()!=''){
  $('#dataBody').load("<?php echo base_url()."report/tabulasi/logistikfilter/1";?>",{"unit_district":$("#district_code").val(),"start_date":$('#start_date').val(),"end_date":$('#end_date').val()});
  $('#linkDownload').attr("href","<?php echo base_url()."report/tabulasi/logistikdownload/kabupaten/";?>"+$("#district_code").val()+"/"+periode);
 
}else if($('#unit_code').val()!=''){
  $('#dataBody').load("<?php echo base_url()."report/tabulasi/logistikfilter/1";?>",{"host_unit_code":$("#unit_code").val(),"start_date":$('#start_date').val(),"end_date":$('#end_date').val()});

  $('#linkDownload').attr("href","<?php echo base_url()."report/tabulasi/logistikdownload/unit/";?>"+$("#unit_code").val()+"/"+periode);
}else{
  $('#dataBody').load("<?php echo base_url()."report/tabulasi/logistikfilter/1";?>",{"start_date":$('#start_date').val(),"end_date":$('#end_date').val()});
  $('#linkDownload').attr("href","<?php echo base_url()."report/tabulasi/logistikdownload/daerah/";?>"+periode);
}
 
});


  })
</script>