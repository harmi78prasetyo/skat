<?php
Class M_user extends CI_Model{
    public function __construct()
    {
        parent::__construct();
    }


    public function getPassword(){
        $this->db->select("left(UUID(),8) as user_oldpassword");
        $q = $this->db->get()->row();
        return $q->user_oldpassword;
    }

    public function checkUsername($username){
        $this->db->select("user_name");
        $this->db->where("user_name",$username);
        $q=$this->db->get(DB_SYS_USER);
        return $q->num_rows();
    }

    public function getList($page){
        $start=($page-1)*100;
        $this->db->limit(100,$start);
        $this->db->from(DB_SYS_USER);
        $this->db->join(DB_SYS_USERGROUP,"user_group=group_id");
        $this->db->join(DB_MASTER_UNIT_TB,"user_unit=unit_code");
        return $this->db->get()->result();
    }

    public function countAll(){
        return $this->db->count_all_results();
    }
    public function insert($rdata){
        return $this->db->insert(DB_SYS_USER,$rdata);
    }
    public function delete($id){
        return $this->db->delete(DB_SYS_USER,array("user_name"=>$id));
    }

    public function update($rdata,$username){
        return $this->db->update(DB_SYS_USER,$rdata,array("user_name"=>$username));
    }

    public function search($search){
        $this->db->like('user_name',$search);
        $this->db->or_like("user_fname",$search);
        $this->db->or_like("unit_name",$search);
        $this->db->limit(20,0);
        $this->db->from(DB_SYS_USER);
        $this->db->join(DB_SYS_USERGROUP,"user_group=group_id");
        $this->db->join(DB_MASTER_UNIT_TB,"user_unit=unit_code");
       // return $this->db->get_compiled_select();
        return $this->db->get()->result();

    }

    public function detail($id){
        $this->db->where($id);
        return $this->db->get(DB_SYS_USER)->row();
    }

    public function userGroup($uid){
        $this->db->where("group_level",$uid);
        return $this->db->get(DB_SYS_USERGROUP)->result();
    }

    public function unitLevel(){
        return $this->db->get(DB_MASTER_UNIT_GROUP)->result();

    }

    public function checkmail($rdata){
        $this->db->where($rdata);
        $q = $this->db->get(DB_SYS_USER);
        return $q->num_rows();
    }

    public function mailValid($mdata){
        $this->db->where($mdata);
        $this->db->select("user_name");
        $q = $this->db->get(DB_SYS_USER)->row();
        return $q->user_name;
    }
}