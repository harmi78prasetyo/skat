<?php 
Class Login extends CI_Controller{
    public function __construct()
    {
        parent::__construct();
        $this->load->model("M_login");
    }

    public function index(){
        $this->template->renderlogin('login/index');
    }

    public function auth(){

        $username = $this->input->post("username");
        $password = md5($this->input->post("password"));
        $rdata = array("user_name"=>$username,"user_password"=>$password);

        $jdata = $this->M_login->auth($rdata);
        if($jdata){
            $json['status']='success';
            $this->session->set_userdata((array)$jdata);
        }else{
            $json['status']="error";
        }
        echo json_encode($json);

    }

    public function pwdchange(){
           
     if(!$this->session->userdata('user_name')){
        redirect(base_url()."login");
    }
        $data['user'] = $this->session->userdata('user_name');
        $this->template->renderpage('login/chpassword',$data);
    }


    public function chpasswd(){
        $this->session->sess_destroy();

        $username['user_name'] = $this->input->post("user_name");
        $rdata = array("user_password"=>md5($this->input->post('user_password')),"user_activated"=>"1");
        if($this->M_login->resetPassword($rdata,$username)){
            $json['status'] = 'success';
        }else{
            $json['status'] = "error";
        }
        echo json_encode($json);
    }

    public function checkmail(){
        $email =$this->input->post();
        $mcheck = $this->M_login->checkmail($email);
        if($mcheck>0){
            echo "true";
        }else{
            echo "false";
        }

    }

    public function reset(){
       $pwd = $this->M_global->getPassword();
        $email = $this->input->post();
        $rdata = array(
            "user_password"=>md5($pwd),
            "user_oldpassword"=>$pwd,
            "user_activated"=>"2"
        );

       if($this->M_login->resetPassword($rdata,$email)){
           $json['status'] = 'success';
       }else{
           $json['status'] = "error";
       }
       echo json_encode($json);
    }



    public function logout(){
        $this->session->sess_destroy();
        redirect("login");

    }
}