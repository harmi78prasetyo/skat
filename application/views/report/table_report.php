<div class="row" style="margin-top: 100px;">
          <div class="col-md-12">
            <div class="card">
              <div class="card-header">
                <h4 class="card-title"> Laporan Pemeriksaan TCM</h4>
              </div>
              <div class="card-body">
                <div class="table-responsive">
                  <table class="table" style="border: thin;">
                    <thead class=" text-primary">
                    <TR  style="border: thin;"> 
                    <th rowspan="2">
                        No.
                      </th>
                    <th rowspan="2">
                        Lab Pemeriksa
                      </th>
                      <th rowspan="2">
                        Terakhir Kirim
                      </th>
                      <th rowspan="2">
                        Total Pemeriksaan
                      </th>
                      <th colspan="6" class="text-center">
                        Hasil Pemeriksaan
                      </th>
                    </TR>
                      <tr>
                      <th  class="text-center">
                        MTB Neg
                      </th>
                      <th  class="text-center">MTB Pos Rif Sen</th>
                      <th  class="text-center">MTB Pos Rif Res</th>
                      <th  class="text-center">Invalid</th>
                      <th  class="text-center">Error</th>
                      <th  class="text-center">No Result</th>
                      </tr>
                    </thead>
                    <tbody>
                     
                       <?php
                       $i=1;
                       foreach($report as $list){?>
<td><?php echo $i;?></td>
<td><?php echo $list->unit_name;?></td>
<td><?php echo $list->last_date;?></td>
<td class="text-center"><?php echo $list->total;?></td>
<td class="text-center"><?php echo $list->mtb_neg;?></td>
<td class="text-center"><?php echo $list->mtb_pos_rs;?></td>
<td class="text-center"><?php echo $list->mtb_pos_rr;?></td>
<td class="text-center"><?php echo $list->mtb_invalid;?></td>
<td class="text-center"><?php echo $list->mtb_error;?></td>
<td class="text-center"><?php echo $list->mtb_noresult;?></td>


                       <?php

                       $i++;
                    }
                    ?>
                      
                    </tbody>
                  </table>
                </div>
              </div>
            </div>
          </div>
         

          <script>
              $('document').ready(function(){
                  $('#dashboard').removeClass("active");
                  $('#laporan').addClass("active");
              })
              </script>