<?php
Class M_devices extends CI_Model{
    public function __construct()
    {
        parent::__construct();
    }

    public function getList($page){
        $start = ($page-1)*100;
        $this->db->limit(100,$start);
        $this->db->from(DB_MASTER_TCMHOST);
        $this->db->join(DB_MASTER_UNIT_TB,"unit_code=host_unit_code");
        return $this->db->get()->result();
    }

    public function getNewList(){
        $this->db->select("*");
        $this->db->where("Approved",0);
        $this->db->from(DB_DEVICE_DEPLOYMENT);
        $this->db->join(DB_DEVICE_UNIT,"DeploymentId=CurrentDeploymentId");
        return $this->db->get()->result();
    }

    public function getInfoDevices($id){

        $this->db->select("*");
        $this->db->where("DeviceId",$id);
        $this->db->from(DB_DEVICE_DEPLOYMENT);
        $this->db->join(DB_DEVICE_UNIT,"DeploymentId=CurrentDeploymentId");
        return $this->db->get()->row();
    
    }

    public function insert($rdata){
        return $this->db->insert(DB_MASTER_TCMHOST,$rdata);
    }

    public function update($id){
        $this->db->where("DeploymentId",$id);
        return $this->db->update(DB_DEVICE_DEPLOYMENT,array("Approved"=>1));
    }

    public function getListbyFaskes($id){
        $this->db->limit(100,$start);
        $this->db->where("host_unit_code",$id);
        $this->db->from(DB_MASTER_TCMHOST);
        $this->db->join(DB_MASTER_UNIT_TB,"unit_code=host_unit_code");
        return $this->db->get()->result();
    }
}