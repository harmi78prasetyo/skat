<?php
Class M_dashboard extends CI_Model{
    public function __construct()
    {
        parent::__construct();
    }

    public function pieResult($lev,$kode=null,$periode=null){
        if($periode!=null){
            $exp = explode("_",$periode);
            $this->db->where("TestEndedOn between '".$exp[0]."' AND '".$exp[1]."'");
        }else{
            $this->db->where("TestEndedOn between CURDATE()-INTERVAL 24 MONTH AND CURDATE()");
        }

        if($lev=='daerah'){
            $this->db->where("unit_province",$kode);
            $this->db->group_by("unit_province");
        }elseif($lev=='kabupaten'){
            $this->db->where("unit_district",$kode);
            $this->db->group_by("unit_district");
        }elseif($lev=='unit'){
            $this->db->where("test_unit_code",$kode);
            $this->db->group_by("test_unit_code");
        }

        $this->db->select_sum("mtb_neg");
        $this->db->select_sum("mtb_pos_rs");
        $this->db->select_sum("mtb_pos_rr");
        $this->db->select_sum("mtb_pos_ri");
        $this->db->select_sum("mtb_invalid");
        $this->db->select_sum("mtb_error");
        $this->db->select_sum("mtb_noresult");
        $this->db->from(DB_VIEW_CHART_RESULT_PERDAY);
        return $this->db->get()->row();



    }

    public function getResult($id,$periode='null'){
         if($id!=null){
            $this->db->where("hp_master_unit_tb.unit_district",$id);
        }
        if($periode!=null){
            $exp = explode("_",$periode);
            $this->db->where("TestEndedOn between '".$exp[0]."' AND '".$exp[1]."'");
        }else{
            $this->db->where("TestEndedOn between CURDATE()-INTERVAL 24 MONTH AND CURDATE()");
        }

        $this->db->select("unit_name as label");
        $this->db->select_sum("mtb_neg");
        $this->db->select_sum("mtb_pos_rs");
        $this->db->select_sum("mtb_pos_rr");
        $this->db->select_sum("mtb_pos_ri");
        $this->db->select_sum("mtb_invalid");
        $this->db->select_sum("mtb_error");
        $this->db->select_sum("mtb_noresult");
        $this->db->from(DB_VIEW_CHART_RESULT_PERDAY);
        $this->db->group_by("unit_code");
       $this->db->join("hp_master_unit_tb","unit_code=test_unit_code");
//return $this->db->get_compiled_select();
        return $this->db->get()->result();
    }


    public function getResultHost($id,$periode='null'){
        if($id!=null){
           $this->db->where("test_unit_code",$id);
       }
       if($periode!=null){
           $exp = explode("_",$periode);
           $this->db->where("TestEndedOn between '".$exp[0]."' AND '".$exp[1]."'");
       }else{
        $this->db->where("TestEndedOn between CURDATE()-INTERVAL 24 MONTH AND CURDATE()");
    }

       $this->db->select("host_name as label");
       $this->db->select_sum("mtb_neg");
       $this->db->select_sum("mtb_pos_rs");
       $this->db->select_sum("mtb_pos_rr");
       $this->db->select_sum("mtb_pos_ri");
       $this->db->select_sum("mtb_invalid");
       $this->db->select_sum("mtb_error");
       $this->db->select_sum("mtb_noresult");
       $this->db->from(DB_VIEW_CHART_RESULT_PERHOST);
       $this->db->group_by("host_name");
       return $this->db->get()->result();
   }


    public function getResultProvince($periode=null){
        if($periode!=null){
            $exp = explode("_",$periode);
            $this->db->where("TestEndedOn between '".$exp[0]."' AND '".$exp[1]."'");
        }else{
            $this->db->where("TestEndedOn between CURDATE()-INTERVAL 24 MONTH AND CURDATE()");
        }
        $this->db->select("province_name as label");
        $this->db->select_sum("mtb_neg");
        $this->db->select_sum("mtb_pos_rs");
        $this->db->select_sum("mtb_pos_rr");
        $this->db->select_sum("mtb_pos_ri");
        $this->db->select_sum("mtb_invalid");
        $this->db->select_sum("mtb_error");
        $this->db->select_sum("mtb_noresult");
        $this->db->group_by("unit_province");
        $this->db->from(DB_VIEW_CHART_RESULT_PERDAY);
        $this->db->join("hp_master_province","unit_province=province_code");
        return $this->db->get()->result();

    }

    public function getResultDistrict($id,$periode=null){
        
        if($id!=null){
            $this->db->where("unit_province",$id);
        }
        if($periode!=null){
            $exp = explode("_",$periode);
            $this->db->where("TestEndedOn between '".$exp[0]."' AND '".$exp[1]."'");
        }else{
            $this->db->where("TestEndedOn between CURDATE()-INTERVAL 24 MONTH AND CURDATE()");
        }
        $this->db->select("district_name as label");
        $this->db->select_sum("mtb_neg");
        $this->db->select_sum("mtb_pos_rs");
        $this->db->select_sum("mtb_pos_rr");
        $this->db->select_sum("mtb_invalid");
        $this->db->select_sum("mtb_pos_ri");
        $this->db->select_sum("mtb_error");
        $this->db->select_sum("mtb_noresult");
        $this->db->group_by("district_code");
        $this->db->from(DB_VIEW_CHART_RESULT_PERDAY);
        $this->db->join("hp_master_district","unit_district=district_code");
        return $this->db->get()->result();

    }

    

    public function getTrendline($periode=null){
        if($periode!=null){
            $exp = explode("_",$periode);
            $this->db->where("thbl between DATE_FORMAT('".$exp[0]."','%Y%m') AND DATE_FORMAT('".$exp[1]."','%Y%m')");
        }else{
            $this->db->where("thbl between DATE_FORMAT(CURDATE()-INTERVAL 24 MONTH,'%Y%m') AND DATE_FORMAT(CURDATE(),'%Y%m')");
        }
        $this->db->select("monthyear as label");
        $this->db->select("total as value");
       $this->db->select("CONCAT('newchart-jsonurl-','".base_url()."chart/chart_examtrend_daily/nasional/',thbl) as link");
        return $this->db->get(DB_VIEW_CHART_TREND)->result();
    }

    public function getTrendlineDaily($lev,$kode=null,$periode){
        $this->db->where("thbl",$periode);
     if($lev=='nasional'){
         $this->db->group_by('id');
         $this->db->select("CONCAT('j-getDetail-','".$lev."','_',id) as link");
     }elseif($lev=='daerah'){
         $this->db->where("unit_province",$kode);
         $this->db->group_by("id,unit_province");
         $this->db->select("CONCAT('j-getDetail-','".$lev."','_','".$kode."_',id) as link");
     }elseif($lev=='kabupaten'){
         $this->db->where("unit_district",$kode);
         $this->db->group_by("id,unit_district");
         $this->db->select("CONCAT('j-getDetail-','".$lev."','_','".$kode."_',id) as link");
     }elseif($lev=='unit'){
         $this->db->where("host_unit_code",$kode);
         $this->db->group_by("id,host_unit_code");
         $this->db->select("CONCAT('j-getDetail-','".$lev."','_','".$kode."_',id) as link");
     }
        $this->db->select("days as label");

       
        //$this->db->where("STR_TO_DATE(id,'%Y%m%d')=STR_TO_DATE('".$periode.$day."','%Y%m%d')");
        $this->db->select_sum("total","value");
        //$this->db->group_by('id');
        return  $this->db->get(DB_VIEW_CHART_TREND_DAILY)->result();
    }

    


    public function getTrendlineProp($id,$periode=null)
    {
        if($periode!=null){
        $exp = explode("_",$periode);
        $this->db->where("thbl between DATE_FORMAT('".$exp[0]."','%Y%m') AND DATE_FORMAT('".$exp[1]."','%Y%m')");
        }else{
            $this->db->where("thbl between DATE_FORMAT(CURDATE()-INTERVAL 24 MONTH,'%Y%m') AND DATE_FORMAT(CURDATE(),'%Y%m')");
        }
        
        
        $this->db->where("unit_province",$id);

        $this->db->select("monthyear as label");
        $this->db->select("total as value");
      // $this->db->select("CONCAT('j-getDetail-',thbl) as link");
      //$this->db->select("CONCAT('newchart-jsonurl-','".base_url()."chart/chart_examtrend_daily/',thbl,'/',maxdate) as link");
      $this->db->select("CONCAT('newchart-jsonurl-','".base_url()."chart/chart_examtrend_daily/daerah/',unit_province,'/',thbl) as link"); 
      return $this->db->get(DB_VIEW_CHART_TREND_PROP)->result();
    }

    public function getTrendlineKab($id,$periode=null){
        
        if($periode!=null){
            $exp = explode("_",$periode);
            $this->db->where("thbl between DATE_FORMAT('".$exp[0]."','%Y%m') AND DATE_FORMAT('".$exp[1]."','%Y%m')");
            }else{
                $this->db->where("thbl between DATE_FORMAT(CURDATE()-INTERVAL 24 MONTH,'%Y%m') AND DATE_FORMAT(CURDATE(),'%Y%m')");
            }

        $this->db->where("unit_district",$id);
        $this->db->select("monthyear as label");
        $this->db->select("total as value");
        $this->db->select("CONCAT('newchart-jsonurl-','".base_url()."chart/chart_examtrend_daily/kabupaten/',unit_district,'/',thbl) as link"); 
      // $this->db->where("thbl between DATE_FORMAT(CURDATE()-INTERVAL 24 MONTH,'%Y%m') AND DATE_FORMAT(CURDATE(),'%Y%m')");
        return $this->db->get(DB_VIEW_CHART_TREND_KAB)->result();
    }


    public function getTrendlineUnit($id,$periode=null){
        
        if($periode!=null){
            $exp = explode("_",$periode);
            $this->db->where("thbl between DATE_FORMAT('".$exp[0]."','%Y%m') AND DATE_FORMAT('".$exp[1]."','%Y%m')");
            }else{
                $this->db->where("thbl between DATE_FORMAT(CURDATE()-INTERVAL 24 MONTH,'%Y%m') AND DATE_FORMAT(CURDATE(),'%Y%m')");
            }

        $this->db->where("host_unit_code",$id);
        $this->db->select("monthyear as label");
        $this->db->select("total as value");
        $this->db->select("CONCAT('newchart-jsonurl-','".base_url()."chart/chart_examtrend_daily/unit/',host_unit_code,'/',thbl) as link"); 
      // $this->db->where("thbl between DATE_FORMAT(CURDATE()-INTERVAL 24 MONTH,'%Y%m') AND DATE_FORMAT(CURDATE(),'%Y%m')");
        return $this->db->get(DB_VIEW_CHART_TREND_UNIT)->result();
    }




    public function getOffline(){
        $this->db->select("host_name as label");
        $this->db->select("last_date as value");
        $this->db->select("
        CASE
        WHEN last_date>1 && last_date <=2 THEN 'ff9933'
        when last_date>=3 THEN 'FF0000'
        ELSE
        '00ff00'
        END
        as color");

        $this->db->order_by("last_date","DESC");
        $this->db->limit("10","0");
        return $this->db->get("hp_view_dashboard_offrate")->result();
    }

    public function getOfflinePropinsi($id){
        $this->db->where("unit_province",$id);
        $this->db->select("host_name as label");
        $this->db->select("last_date as value");
        $this->db->order_by("last_date","DESC");
        $this->db->limit("10","0");
        return $this->db->get("hp_view_dashboard_offrate")->result();
    }

    public function getOfflineKabupaten($id){
        $this->db->where("unit_district",$id);
        $this->db->select("host_name as label");
        $this->db->select("last_date as value");
        $this->db->order_by("last_date","DESC");
        $this->db->limit("10","0");
        return $this->db->get("hp_view_dashboard_offrate")->result();
    }

    public function getOfflineUnit($id){
        $this->db->where("unit_code",$id);
        $this->db->select("host_name as label");
        $this->db->select("last_date as value");
        $this->db->order_by("last_date","DESC");
        $this->db->limit("10","0");
        return $this->db->get("hp_view_dashboard_offrate")->result();
    }




    public function getError($id=null){
        if($id!=null){
            $this->db->where($id);
        }
        $this->db->select("unit_name as label");
        $this->db->select("er_rate as value");
        $this->db->select("
        CASE
        WHEN er_rate>2 && er_rate <=5 THEN 'ff9933'
        when er_rate>5 THEN 'FF0000'
        ELSE
        '00ff00'
        END
        as color");
        $this->db->order_by("er_rate","DESC");
        //$this->db->select("concat('j-alert-',er_rate) as link");
        return $this->db->get("hp_view_dashboard_chart_error")->result();
    }


    public function getErrorUnit($id=null){
        if($id!=null){
            $this->db->where($id);
        }
        $this->db->select("host_name as label");
        $this->db->select("er_rate as value");
        $this->db->select("
        CASE
        WHEN er_rate>2 && er_rate <=5 THEN 'ff9933'
        when er_rate>5 THEN 'FF0000'
        ELSE
        '00ff00'
        END
        as color");
        $this->db->order_by("er_rate","DESC");
        //$this->db->select("concat('j-alert-',er_rate) as link");
        return $this->db->get("hp_view_dashboard_chart_error_perhost")->result();
    }


}