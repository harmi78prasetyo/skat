<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

if(!function_exists('DateFormat')){
    function DateFormat($val){
        $xp = explode("-",$val);
        return $xp['1']."-".$xp['2']."-".$xp['0'];
    }
}



if(!function_exists('getDate')){
    function getDate($val){
        $d = substr($val,1,10);
        
        return $d;
    }
}

if(!function_exists('bulanIndo')){
    function bulanIndo($val){
        $bal = explode("-",$val);
        switch($bal[0]){
            case "01":$val="Januari";break;
            case "02":$val="Februari";break;
            case "03":$val="Maret";break;
            case "04":$val="April";break;
            case "05":$val="Mei";break;
            case "06":$val="Juni";break;
            case "07":$val="Juli";break;
            case "08":$val="Agustus";break;
            case "09":$val="September";break;
            case "10":$val="Oktober";break;
            case "11":$val="November";break;
            case "12":$val="Desember";break;
            case "1":$val="Januari";break;
            case "2":$val="Februari";break;
            case "3":$val="Maret";break;
            case "4":$val="April";break;
            case "5":$val="Mei";break;
            case "6":$val="Juni";break;
            case "7":$val="Juli";break;
            case "8":$val="Agustus";break;
            case "9":$val="September";break;

        }

        $res = $val." ".$bal[1];

        return $res;
    }
}
