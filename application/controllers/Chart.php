<?php
Class Chart extends CI_Controller{

    public function __construct()
    {
        parent::__construct();
        $this->load->model("M_dashboard");
    }

   public function chartPieResult(){
    $json = array();
    $json['chart'] = array("palette"=>"2",
       "caption"=>"Hasil Pemeriksaan TCM",
       "showpercentageinlabel"=>"1",
       "showpercentvalues"=> "1",
       "showvalues"=>"1",
       "showlabels"=>"1",
       "showlegend"=>"1"
    );


    if($this->uri->segment('3')=='nasional'){
       
           if($this->uri->segment('4')==''){
               $json['chart']['subcaption'] = "24 Bulan Terakhir";
           }else{
            $xp=explode("_",$this->uri->segment('4'));
            $json['chart']['subcaption'] = "Periode :".DateFormat($xp[0])." s/d ".DateFormat($xp[1]);
           }
        $mdata = $this->M_dashboard->pieResult($this->uri->segment('3'),null,$this->uri->segment('4'));
        }elseif($this->uri->segment('3')=='daerah'){
          
            if($this->uri->segment('5')==''){
                $json['chart']['subcaption'] = "24 Bulan Terakhir";
            }else{
             $xp=explode("_",$this->uri->segment('5'));
             $json['chart']['subcaption'] = "Periode :".DateFormat($xp[0])." s/d ".DateFormat($xp[1]);
            }
    
            $mdata = $this->M_dashboard->pieResult($this->uri->segment('3'),$this->uri->segment('4'),$this->uri->segment('5'));
        }elseif($this->uri->segment('3')=='kabupaten'){
           
            if($this->uri->segment('5')==''){
                $json['chart']['subcaption'] = "24 Bulan Terakhir";
            }else{
             $xp=explode("_",$this->uri->segment('5'));
             $json['chart']['subcaption'] = "Periode :".DateFormat($xp[0])." s/d ".DateFormat($xp[1]);
            }
            $mdata = $this->M_dashboard->pieResult($this->uri->segment('3'),$this->uri->segment('4'),$this->uri->segment('5'));
    
        }elseif($this->uri->segment('3')=='unit'){
            
            if($this->uri->segment('5')==''){
                $json['chart']['subcaption'] = "24 Bulan Terakhir";
            }else{
             $xp=explode("_",$this->uri->segment('5'));
             $json['chart']['subcaption'] = "Periode :".DateFormat($xp[0])." s/d ".DateFormat($xp[1]);
            }
            $mdata = $this->M_dashboard->pieResult($this->uri->segment('3'),$this->uri->segment('4'),$this->uri->segment('5'));
        }

        if($mdata){
        $json['data']=array();
        $json['data'] = array(
            array("label"=>"MTB NEG","color"=>"00FF00","value"=>$mdata->mtb_neg),
            array("label"=>"MTB+ RS","color"=>"FFFF00","value"=>$mdata->mtb_pos_rs),
            array("label"=>"MTB+ RR","color"=>"FF3300","value"=>$mdata->mtb_pos_rr),
            array("label"=>"MTB+ RI","color"=>"FF9900","value"=>$mdata->mtb_pos_ri),
            array("label"=>"INVALID","color"=>"00CCFF","value"=>$mdata->mtb_invalid),
            array("label"=>"ERROR","color"=>"0000FF","value"=>$mdata->mtb_error),
            array("label"=>"No Result","color"=>"e6e6e6","value"=>$mdata->mtb_noresult)
        );
    }
echo json_encode($json);

   }
    public function chartresult(){
        $json = array();
    $json['chart'] = array("palette"=>"2",
       // "caption"=>"Hasil Pemeriksaan TCM",
       "canvasbgcolor"=> "FFFFDD",
    "bgcolor"=> "B8D288,FFFFFF",
    "bgangle"=> "90",
    "numdivlines"=> "0",
    "plotbordercolor"=> "83B242",
    "canvasbordercolor"=> "83B242",
    "canvasborderthickness"=> "1",
    "showsum"=>"1",
    "showvalues"=>"0",

        "exportenabled"=>"1"
    );

    $json['categories'][0]['category'] = array();
    $json['dataset'][0]['seriesName']="MTB NEG";
    $json['dataset'][0]['color'] = "00FF00";
    $json['dataset'][0]['data'] = array();

    
    $json['dataset'][1]['seriesName']="MTB POS RS";
    $json['dataset'][1]['color'] = "FFFF00";
    $json['dataset'][1]['data'] = array();

    $json['dataset'][2]['seriesName']="MTB POS RR";
    $json['dataset'][2]['color'] = "FF3300";
    $json['dataset'][2]['data'] = array();
    
    $json['dataset'][3]['seriesName']="MTB POS RI";
    $json['dataset'][3]['color'] = "FF9900";
    $json['dataset'][3]['data'] = array();


    $json['dataset'][4]['seriesName']="INVALID";
    $json['dataset'][4]['color'] = "00CCFF";
    $json['dataset'][4]['data'] = array();

    $json['dataset'][5]['seriesName']="ERROR";
    $json['dataset'][5]['color'] = "0000FF";
    $json['dataset'][5]['data'] = array();

    $json['dataset'][6]['seriesName']="NO RESULT";
    $json['dataset'][6]['color'] = "e6e6e6";
    $json['dataset'][6]['data'] = array();
   // $mdata = $this->M_dashboard->getResult();


   if($this->uri->segment('3')=='nasional'){
    $json['chart']['caption'] = "Hasil Pemeriksaan TCM Per Propinsi";
       if($this->uri->segment('4')==''){
           $json['chart']['subcaption'] = "24 Bulan Terakhir";
       }else{
        $xp=explode("_",$this->uri->segment('4'));
        $json['chart']['subcaption'] = "Periode :".DateFormat($xp[0])." s/d ".DateFormat($xp[1]);
       }
    $mdata = $this->M_dashboard->getResultProvince($this->uri->segment('4'));
    }elseif($this->uri->segment('3')=='daerah'){
        $json['chart']['caption'] = "Hasil Pemeriksaan TCM Per Kabupaten";
        if($this->uri->segment('5')==''){
            $json['chart']['subcaption'] = "24 Bulan Terakhir";
        }else{
         $xp=explode("_",$this->uri->segment('5'));
         $json['chart']['subcaption'] = "Periode :".DateFormat($xp[0])." s/d ".DateFormat($xp[1]);
        }

        $mdata = $this->M_dashboard->getResultDistrict($this->uri->segment('4'),$this->uri->segment('5'));
    }elseif($this->uri->segment('3')=='kabupaten'){
        $json['chart']['caption'] = "Hasil Pemeriksaan TCM Per Faskes";
        if($this->uri->segment('5')==''){
            $json['chart']['subcaption'] = "24 Bulan Terakhir";
        }else{
         $xp=explode("_",$this->uri->segment('5'));
         $json['chart']['subcaption'] = "Periode :".DateFormat($xp[0])." s/d ".DateFormat($xp[1]);
        }
        $mdata = $this->M_dashboard->getResult($this->uri->segment('4'),$this->uri->segment('5'));

    }elseif($this->uri->segment('3')=='unit'){
        $json['chart']['caption'] = "Hasil Pemeriksaan TCM Per Perangkat";
        if($this->uri->segment('5')==''){
            $json['chart']['subcaption'] = "24 Bulan Terakhir";
        }else{
         $xp=explode("_",$this->uri->segment('5'));
         $json['chart']['subcaption'] = "Periode :".DateFormat($xp[0])." s/d ".DateFormat($xp[1]);
        }
        $mdata = $this->M_dashboard->getResultHost($this->uri->segment('4'),$this->uri->segment('5'));
    }


    $i=0;
    foreach($mdata as $list){
        $label['label'] = $list->label;
        array_push($json['categories'][0]['category'],$label);
        $mtbneg['value'] = $list->mtb_neg;
        $mtbpos_rs['value'] = $list->mtb_pos_rs;
        $mtbpos_rr['value'] = $list->mtb_pos_rr;
        $mtbpos_ri['value'] = $list->mtb_pos_ri;
        $error['value'] = $list->mtb_error;
        $invalid['value'] = $list->mtb_invalid;
        $noresult['value'] = $list->mtb_noresult;

        array_push($json['dataset'][0]['data'],$mtbneg);
        array_push($json['dataset'][1]['data'],$mtbpos_rs);
        array_push($json['dataset'][2]['data'],$mtbpos_rr);
        array_push($json['dataset'][3]['data'],$mtbpos_ri);
        array_push($json['dataset'][4]['data'],$invalid);
        array_push($json['dataset'][5]['data'],$error);
        array_push($json['dataset'][6]['data'],$noresult);



    $i++;
}
echo json_encode($json);





    }




    public function chart_examtrend(){
        $json=array();
        $json['chart']= array("caption"=>"Tren Pemeriksaan TCM ",
        "captionFontSize"=>"20",
        //"subcaption"=>"per bulan (24 Bulan Terakhir)",
        "subcaptionFontSize"=>"15",
        "yaxismaxvalue"=>"100",
        "bgcolor"=>"406181, 6DA5DB",
        "bgalpha"=>"100",
        "basefontcolor"=>"FFFFFF",
        "canvasbgalpha"=>"0",
        "canvasbordercolor"=>"FFFFFF",
        "divlinecolor"=>"FFFFFF",
        "divlinealpha"=>"100",
        "numvdivlines"=>"10",
        "vdivlineisdashed"=>"1",
        "showalternatevgridcolor"=>"1",
        "linecolor"=>"ff0000",
        "anchorradius"=>"4",
        "anchorbgcolor"=>"BBDA00",
        "anchorbordercolor"=>"FFFFFF",
        "anchorborderthickness"=>"2",
        "showvalues"=>"0",
        //"numbersuffix"=>"%",
        "tooltipbgcolor"=>"406181",
        "tooltipbordercolor"=>"406181",
        "alternatehgridalpha"=>"5",
        "numvisibleplot"=> "12",
        "displayStartIndex"=>'16',  
        "displayEndIndex"=>'25',
        "exportenabled"=>"1"
    
    );
    //$json['data'] = $this->M_dashboard->getTrendline();
    if($this->uri->segment('3')=='nasional'){
        if($this->uri->segment('4')==''){
            $json['chart']['subcaption']="24 Bulan Terakhir";
        }else{
            $exp = explode("_",$this->uri->segment('4'));
            $json['chart']['subcaption']="Periode : ".DateFormat($exp['0'])." s/d ".DateFormat($exp[1]);
        }
    $mdata = $this->M_dashboard->getTrendline($this->uri->segment('4'));
    }elseif($this->uri->segment('3')=='daerah'){
        if($this->uri->segment('5')==''){
            $json['chart']['subcaption']="Propinsi :".$this->M_global->namaPropinsi($this->uri->segment('4'))." - Periode : 24 Bulan Terakhir";
        }else{
            $exp = explode("_",$this->uri->segment('5'));
            $json['chart']['subcaption']="Propinsi :".$this->M_global->namaPropinsi($this->uri->segment('4'))." - Periode : ".DateFormat($exp['0'])." s/d ".DateFormat($exp[1]);
        }

        $mdata = $this->M_dashboard->getTrendlineProp($this->uri->segment('4'),$this->uri->segment('5'));
    }elseif($this->uri->segment('3')=='kabupaten'){
        if($this->uri->segment('5')==''){
            $json['chart']['subcaption']="Kabupaten :".$this->M_global->namaKabupaten($this->uri->segment('4'))." - Periode : 24 Bulan Terakhir";
        }else{
            $exp = explode("_",$this->uri->segment('5'));
            $json['chart']['subcaption']="Kabupaten :".$this->M_global->namaKabupaten($this->uri->segment('4'))." - Periode : ".DateFormat($exp['0'])." s/d ".DateFormat($exp[1]);
        }
        $mdata = $this->M_dashboard->getTrendlineKab($this->uri->segment('4'),$this->uri->segment('5'));
    }elseif($this->uri->segment('3')=='unit'){
        if($this->uri->segment('5')==''){
            $json['chart']['subcaption']="24 Bulan Terakhir";
        }else{
            $exp = explode("_",$this->uri->segment('5'));
            $json['chart']['subcaption']="Periode : ".DateFormat($exp['0'])." s/d ".DateFormat($exp[1]);
        }
        $mdata = $this->M_dashboard->getTrendlineUnit($this->uri->segment('4'),$this->uri->segment('5'));
    }
    $json['categories'][0]['category'] = array();
    $json['dataset'][0]['data'] = array();
    $i=0;
    foreach($mdata as $list){
        $label['label'] = $list->label;
        $link = $list->link;
        array_push($json['categories'][0]['category'],$label);
        $total = array("value"=>$list->value,"link"=>$link);
        array_push($json['dataset'][0]['data'],$total);
      //  array_push($json['dataset'][0]['data']['link'],$link);


    $i++;
}
    echo json_encode($json);
    }



    public function chart_examtrend_daily(){
        $json=array();
        $json['chart']= array("caption"=>"Tren Pemeriksaan TCM ",
        "captionFontSize"=>"20",
        "subcaptionFontSize"=>"15",
        "yaxismaxvalue"=>"100",
        "bgcolor"=>"406181, 6DA5DB",
        "bgalpha"=>"100",
        "basefontcolor"=>"FFFFFF",
        "canvasbgalpha"=>"0",
        "canvasbordercolor"=>"FFFFFF",
        "divlinecolor"=>"FFFFFF",
        "divlinealpha"=>"100",
        "numvdivlines"=>"10",
        "vdivlineisdashed"=>"1",
        "showalternatevgridcolor"=>"1",
        "linecolor"=>"ff0000",
        "anchorradius"=>"4",
        "anchorbgcolor"=>"BBDA00",
        "anchorbordercolor"=>"FFFFFF",
        "anchorborderthickness"=>"2",
        "showvalues"=>"0",
        //"numbersuffix"=>"%",
        "tooltipbgcolor"=>"406181",
        "tooltipbordercolor"=>"406181",
        "alternatehgridalpha"=>"5",
        "numvisibleplot"=> "12",
        "displayStartIndex"=>'16',  
        "displayEndIndex"=>'25',
        "exportenabled"=>"1"
    
    );

    if($this->uri->segment('3')=='nasional'){
        $lev = $this->uri->segment('3');
        $kode = null;
        $bln=$this->uri->segment('4');
    }else{
        $lev = $this->uri->segment('3');
        $kode = $this->uri->segment('4');
        $bln = $this->uri->segment('5');
    }
  
    //$sublabel = $this->M_global->getMonth($bln.$maxdate);
    

    $thn =substr($bln,0,4);
    $bl = substr($bln,4,5);
    
       
    
   //if($this->uri->segment('3')=='nasional'){
        $gdata = $this->M_dashboard->getTrendlineDaily($lev,$kode,$bln);
   



    $json['chart']['subcaption'] = bulanIndo($bl."-".$thn);

    $json['categories'][0]['category'] = array();
    $json['dataset'][0]['data'] = array();
    $i=0;
    foreach($gdata as $list){
        $link = $list->link;
        $label['label'] = $list->label;
        array_push($json['categories'][0]['category'],$label);
        $total = array("value"=>$list->value,"link"=>$link);
        array_push($json['dataset'][0]['data'],$total);

    $i++;
}

    echo json_encode($json);

    }


    public function chartoffline(){
        $json=array();
        $json['chart'] = array(
            "caption"=>"Berapa hari Fasyankes Tidak upload data",
            "subcaption"=>"(hari)",
    "yaxisname"=>"Hari",
    "alternatevgridcolor"=>"AFD8F8",
    "basefontcolor"=>"114B78",
    "tooltipbordercolor"=>"114B78",
    "tooltipbgcolor"=>"E7EFF6",
    "plotborderdashed"=>"1",
    "plotborderdashlen"=>"2",
    "plotborderdashgap"=>"2",
    "useroundedges"=>"1",
    "showborder"=>"0",
    "bgcolor"=>"FFFFFF,FFFFFF",
    "numbersuffix"=>" hari",
    "formatnumber"=>"0"
        );

        if($this->uri->segment('3')=='nasional'){
        $json['data'] = $this->M_dashboard->getOffline();
        }elseif($this->uri->segment('3')=='daerah'){
            $json['data'] = $this->M_dashboard->getOfflinePropinsi($this->uri->segment('4'));
        }elseif($this->uri->segment('3')=='kabupaten'){
            $json['data'] = $this->M_dashboard->getOfflineKabupaten($this->uri->segment('4'));
        }elseif($this->uri->segment('3')=='unit'){
            $json['data'] = $this->M_dashboard->getOfflineUnit($this->uri->segment('4'));
        }
        echo json_encode($json);
    }




    public function charterror(){
        $json=array();
        $json['chart'] = array(
            "caption"=>"Tingkat Kesalahan (Error Rate)",
    "alternatevgridcolor"=>"AFD8F8",
    "basefontcolor"=>"114B78",
    "tooltipbordercolor"=>"114B78",
    "tooltipbgcolor"=>"E7EFF6",
    //"plotborderdashed"=>"1",
    //"plotborderdashlen"=>"2",
    //"plotborderdashgap"=>"2",
    "useroundedges"=>"0",
    "showborder"=>"0",
    "bgcolor"=>"FFFFFF,FFFFFF",
    "numbersuffix"=>"%",
    //"plotGradientColor"=>"1" 
    
        );

       // $json['data'] = $this->M_dashboard->getError($id);
       
       
       if($this->uri->segment('3')=='nasional'){
        $json['data'] = $this->M_dashboard->getError();
        }elseif($this->uri->segment('3')=='daerah'){
            $json['data'] = $this->M_dashboard->getError(array("unit_province"=>$this->uri->segment('4')));
        }elseif($this->uri->segment('3')=='kabupaten'){
            $json['data'] = $this->M_dashboard->getError(array("unit_district"=>$this->uri->segment('4')));
        }elseif($this->uri->segment('3')=='unit'){
            $json['data'] = $this->M_dashboard->getErrorUnit(array("unit_code"=>$this->uri->segment('4')));
        }

       
       echo json_encode($json);
    }






    


}
?>