<?php
/* Language : ID */
//A
$lang['action'] = "Tindakan";
$lang['add_network'] = "Jejaring Lab";
$lang['add_specimen'] = "Menambah Spesimen";
$lang['address'] = "Alamat";



//B

//C
$lang['condition_bad'] = "Rusak";
$lang['condition_good'] = "Baik";
//D
$lang['dashboard'] = "Dashboard";
$lang['data_form'] = "Form Data";
$lang['dateformat'] ="Format Tanggal";
$lang['district'] = "Kab/Kota";
$lang['district_code'] = "Kode Kab/Kota";
$lang['district_name'] = "Nama Kab/Kota";
$lang['detination'] = "Tujuan";
$lang['detail'] = "Detil";
$lang['doctor_name'] = "Dokter PIC";
$lang['delivered'] = "Sampai tujuan";

//E
$lang['examinationType'] = "Jenis Tes";


//F
$lang['forgetpassword'] = "Lupa Password";
$lang['finish'] = "Selesai";
$lang['feedback'] = "Feedback";

//G
$lang['geo_structure'] = "Data Dasar Wilayah";
$lang['group'] = "Group";
$lang['group_id'] = "ID Group";

//H
$lang['health_facility'] = "Fasilitas Kesehatan";
$lang['home'] = "Beranda";
$lang['healthfacility_name'] = "Nama Fasyankes";
$lang['healthfacility_code'] = "Kode Fasyankes";
$lang['healthfacility_referral'] = "Faskes Rujukan";

$lang['healthfacility_type'] = "Jenis Faskes";
$lang['healthfacility_type_hospital'] = "Rumah Sakit";
$lang["healthfacility_type_phc"] = "Puskesmas";
$lang['healthfacility_type_clinic'] = "Klinik";

//I

$lang['internal'] = "Internal Examination Request";
$lang['internal_pdp'] ="Examination Request";
$lang["internal_result"] = "Result";
//J

//K

//L
$lang['logout'] = "Keluar";

//M
$lang['monitoring']="Follow Up";
$lang['microscopic'] = "Mikroskopis";


//N
$lang['number'] = "No.";
$lang['network'] = "Jejaring Layanan Kesehatan";
$lang["network_lab"] = "Jejaring lab";
$lang["network_treatment"] = "Jejaring Pengobatan";
$lang["network_origin"] = "Faskes Pengirim";

//O
$lang['order'] = "Order";
$lang['order_number'] = "No. Order";
$lang['order_date'] = "Tanggal Order";
$lang['order_destination'] = "Tujuan";
$lang['order_courier'] = "Kurir";
$lang['order_status'] = "Status Order";
$lang['origin'] = "Pengirim";
$lang['order_origin'] = "Faskes Pengirim";
$lang['order_rejected'] = "Permintaan ditolak";
$lang["order_approved"] = "Permintaan Diterima";
$lang["order_num_specimen"] = "No. Spesimen";
$lang['order_reason'] = "Alasan Penolakan";
$lang['order_confirmation'] = "Konfirmasi Order";
//P
$lang['password'] = "Password";
$lang['patient_reg_nation'] = "No. Register Nasional";
$lang['periode'] = "Periode";
$lang['profile'] = "Profil";
$lang['province'] = "Propinsi";
$lang['province_code'] = "Kode Propinsi";
$lang['province_name'] = "Nama Propinsi";
$lang['pickup'] = "Pengambilan";
$lang['pickup_confirmation'] = "Konfirmasi";
//new
$lang['phone_number'] = "No. Telp";

//Q

//R
$lang["recipient_facility"] = "Faskes Penerima";
$lang['ReferenceData'] = "Referensi Data";
$lang['rememberpassword'] = "Pengingat Password";
$lang['report'] = "Laporan";
$lang['receive'] = "Terima Paket";
$lang['confirmation'] = "Konfirmasi";
$lang['result'] ="Hasil";
$lang['result_absolut'] = "Angka absolut";
$lang['rejected'] = "Tolak";
$lang['approved'] = "Terima";

$lang['report_sender_facility'] = "Faskes Pengirim";
$lang['report_exam_facility'] = "Faskes Penerima";
$lang['report_month_to'] = "bulan";
$lang['report_periode'] = "periode tes";
$lang["report_year"] = "Tahun";
$lang["report_month"] = "Bulan";
$lang["report_date"] = "Terakhir perika";
$lang["result_view"] = "Lihat Hasil";
$lang["result_conclusion"] = "Ringkasan hasil";
//S
$lang['specimen_id'] = "ID Spesimen";
$lang['specimen_art_date'] = "Tgl mulari ART";
$lang['specimen_date_collected'] = "Tanggal Pengambilan sampel";
$lang['specimen_test_type'] = "Kategori Pemeriksaan";
$lang['specimen_exam_date'] = "Tanggal Pemeriksaan";
$lang['specimen_date_release'] = "Tanggal hasil";

$lang['sender_facilities'] = "Faskes Pengirim";
$lang['shipper_company'] = "Nama Perusahaan Pengiriman";
$lang['shipper_code'] = "Kode Kurir";
$lang['shipper_name'] = "Nama Kurir";
$lang['specimen']="Spesimen";
$lang['SwitchLanguage'] = "Pilih bahasa";
$lang['subdistrict'] = "Kecamatan";
$lang['subdistrict_code'] = "Kode Kecamatan";
$lang['subdistrict_name'] = "Nama Kecamatan";

//T

//U
$lang['username'] = "Nama Pengguna";
$lang['UserData'] = " Data Pengguna";
$lang['unit'] = "Unit";
$lang['user_management'] = "Setting pengguna";
$lang['user'] = "Pengguna";
$lang['user_group'] = "Group";
$lang['user_role'] = "Setting Akses";
$lang['user_fname'] = "Nama Depan";
$lang['user_lname'] = "Nama Belakang";
$lang['user_unit'] = "Institusi";

$lang['username_group'] = "Group Pengguna";
$lang['user_hf'] = "Tenaga kesehatan";
$lang['user_kurir'] = "Petugas Kurir";
$lang['user_fullname'] = "Nama Lengkap";



$lang['user_group_superuser'] = "Super User";
$lang['user_group_moh'] = "Nasional/pusat";
$lang['user_group_dho'] = "Dinas Kesehatan Kab/Kota";
$lang['user_group_pho'] = "Dinas Kesehatan Propinsi";
$lang['user_group_moh'] = "Kementerian Kesehatan";
$lang['user_group_facility'] = "Fasilitas Kesehatan";
$lang['user_group_courier'] = "Kurir";
$lang['user_group_authority'] = "Akses Pengguna";
$lang['user_group_fac_2'] = "Sebagai Penerima";
$lang['user_group_fac_1'] = "Sebagai Pengirim"; 
$lang['user_group_fac_3'] = "Pengirim dan Penerima";
$lang['user_hf_group'] = "Tugas Pokok dan Fungsi";
$lang["user_hf_group_1"] = "LAB Faskes Pengampu";
$lang['user_hf_group_2'] = "PDP Faskes Pengampu";
$lang['user_hf_group_3'] = "Lab Faskes Satelit";
$lang['user_hf_group_4'] = "PDP Faskes Satelit";
$lang['user_hf_group_5'] = "Lab Faskes Rujukan";

//V
$lang['vl_report'] = "Laporan Pemeriksaan VL";
$lang['vl_monitoring'] = "Monitoring Pengiriman";
$lang['eid_report'] = "Laporan Pemeriksaan EID";

//W
$lang['welcome'] = "Welcome";

//X
$lang['xpert'] = "TCM";

//Y

//Z



$lang['dashboard_num_specimen_ref'] = "Jumlah <br>spesimen dikirim";
$lang['dashboard_num_shipment_distpatched'] = "Jumlah <br>spesimen diterima";
$lang['dashboard_num_arrived'] = "Jumlah <br>pengiriman sampai tujuan (48 Jam)";
$lang['dashboard_num_result'] = "Jumlah <br>Sudah ada hasil";
$lang["dashboard_num_participants"] = "Jumlah <br>Faskes Mengirim spesimen";





$lang['specimen_report'] = "Laporan <br>Spesimen dikirim";
$lang['shipment_report'] = "laporan pengiriman ";
$lang['report_tat'] = "Turn around Time";
$lang["start_date"] = "Tanggal Mulai";
$lang["end_date"] = "Tanggal Selesai";

$lang["report_pickup_site"] = "Lokasi Pengambilan";
$lang["report_recipient_site"] = "Tujuan Pengiriman";
$lang["report_number_of_specimen"] = "Jumlah spesimen";
$lang["report_pickup_date"] = "Tanggal Penjemputan";
$lang["report_pickup_time"] = "Jam Penjemputan";
$lang["report_pickup_duration"] = "Lama Penjemputan";
$lang["report_date_delivered"] = "tanggal sampai";
$lang["report_time_delivered"] = "Jam Sampai";
$lang["report_date_received"] = "Tanggal diterima";
$lang["report_shipping_duration"] = "Lama Pengiriman";
$lang["order_monitoring"] = "Monitoring pengiriman";
$lang["filter"] = "Filter";
$lang["data_table"] = "Data Tabel";
$lang["yes"] = "Ya";
$lang["no"] = "Tidak";


































