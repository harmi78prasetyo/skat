<?php
class Template{
    protected $_ci;
    
    function __construct(){
        $this->_ci = &get_instance();
    }
    
  function renderpage($content, $data = NULL){
    /*
     * $data['headernya'] , $data['contentnya'] , $data['footernya']
     * variabel diatas ^ nantinya akan dikirim ke file views/template/index.php
     * */
        $data['header'] = $this->_ci->load->view('template/__header', $data, TRUE);
        $data['menu'] = $this->_ci->load->view('template/__menu', $data, TRUE);
        $data['navbar'] = $this->_ci->load->view('template/__navbar', $data, TRUE);
        //$data['sidebar'] = $this->_ci->load->view('template/__side', $data, TRUE);
        $data['content'] = $this->_ci->load->view($content, $data, TRUE);
        $data['footer'] = $this->_ci->load->view('template/__footer', $data, TRUE);
        
        $this->_ci->load->view('template/index', $data);
    }

    function renderlogin($content,$data=Null){
        $data['header'] = $this->_ci->load->view('template/login/__header', $data, TRUE);
        $data['content'] = $this->_ci->load->view($content, $data, TRUE);
        $this->_ci->load->view('template/login/__login', $data);
    }

    function renderbody($content,$data=null){
        $data['header'] = $this->_ci->load->view('templates/__header', $data, TRUE);
        $data['content'] = $this->_ci->load->view($content, $data, TRUE);
        $this->_ci->load->view('template/__body', $data);
    }
}