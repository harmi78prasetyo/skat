<footer class="footer">
        <div class=" container-fluid ">
          <nav>
            <ul>
              <li>
                <a href="https://sitb.id">
                  SITB
                </a>
              </li>
              <li>
                <a href="http://gxalert.sitb.id">
                  GX Alert
                </a>
              </li>
              <li>
                <a href="http://jogjamedia.net">
                  Jogja Media
                </a>
              </li>
            </ul>
          </nav>
          <div class="copyright" id="copyright">
           <!-- &copy; <script>
              document.getElementById('copyright').appendChild(document.createTextNode(new Date().getFullYear()))
            </script>, Designed by <a href="https://www.invisionapp.com" target="_blank">Invision</a>. Coded by <a href="https://www.creative-tim.com" target="_blank">Creative Tim</a>. -->
            &copy;2020, Jogja Info Media
          </div>
        </div>
      </footer>