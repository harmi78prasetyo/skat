<style>
  label{
  color: red;
  margin-left: 10px;
}

    </style>
<div class="panel-header panel-header-sm">
    
      </div>
<div class="row">
          <div class="col-md-12">
            <div class="card">

            <div class="card-header">
                <h5 class="title">Administrator Panel</h5>
                <p class="category">Data Pengguna</p>
                
              </div>

              <div class="card-body">
<div class="input-group col-lg-4">
    <input type="text" id="search" name="search" class="form-control" placeholder="<?php echo $this->lang->line('search');?>">
    &nbsp;
    <a href="#cardform" class="btn btn-warning btn-xs" id="add" data-toggle="modal" data-target="#modalForm" data-backdrop="static" data-keyboard='false'><i class="fas fa-plus-circle"></i>&nbsp;<?php echo $this->lang->line('add');?></a>
</div>



<?php
$max = ceil($total/100);
  if($this->uri->segment(4)>5 && ($this->uri->segment(4)+5)>6){

  
   if($this->uri->segment(4)==$max || ($this->uri->segment(4)+2)>$max){ 
     $maxnum = 1+$this->uri->segment(4);
  }else{ 
    $maxnum = 3+$this->uri->segment(4);
  }
  
  $start = $maxnum-5;
  $prev = $start-1; 

}else{
      $maxnum = 6;
      $start = 1;
      $prev = 1;

  }
  ?>
<ul class="pagination pagination-sm justify-content-center">
  <li class="page-item prev"><a class="page-link" href="<?php echo $prev;?>"><i class="fas fa-arrow-circle-left"></i></a></li>
  

<?php
//echo $max;
  for($i=$start;$i<=($maxnum-1);$i++){

    if($i==$this->uri->segment(4)){
        ?>
        <li class="page-item active" id="page_<?php echo $i;?>"><a class="page-link" href="<?php echo $i;?>"><?php echo $i;?></a></li>
        <?php

    }else{

      ?>
<li class="page-item" id="page_<?php echo $i;?>"><a class="page-link" href="<?php echo $i;?>"><?php echo $i;?></a></li>

      <?php
    }

  }
  ?>
  <li class="page-item next"><a class="page-link " href="<?php echo $maxnum; ?>"><i class="fas fa-arrow-circle-right"></i></a></li>
</ul>



</div>

              <div class="card-body all-icons">
            <div class="row">




<table class="table table-stripe">
<thead>
<tr>
    <th scope="col"><h6><?php echo $this->lang->line('number');?></h6></th>
    <th scope="col"><h6>User Group</h6></th>
    <th scope="col"><h6>User Name </h6></th>
    <th scope="col"><h6>E-mail </h6></th>
    <th scope="col"><h6>Unit TB </h6></th>
    <th scope="col"><h6><?php echo $this->lang->line('action');?></h6></th>
</tr>
</thead>
<tbody id="dataBody">
    <?php 
    $i=1;
    foreach($datalist as $list){ ?>
    <tr>
    <th scope="row"><?php echo $i;?></th>
    <td scope="col"><?php echo $list->group_name;?></td>
    <td scope="col"><?php echo $list->user_name?></td>
    <td scope="col"><?php echo $list->user_email?></td>
    <td scope="col"><?php echo $list->unit_name?></td>
    <td scope="col">
    <a href="#" class="btn btn-danger btn-sm" onclick="conf('<?php echo $list->user_name?>')"><i class="fas fa-trash-alt"></i>&nbsp;<?php echo $this->lang->line('delete');?></a>
    &nbsp;
    <a href="#cardedit" id="btnedit" class="btn btn-success btn-sm"   onclick="getDetail('<?php echo $list->user_name;?>')"><i class="fas fa-edit"></i>&nbsp;<?php echo $this->lang->line('edit');?></a>
    </td>
    
</tr>

    <?php $i++;
} ?>
</tbody>
</table>

            </div>
              </div>
            </div>
          </div>
</div>




<!---- Modal Form -->

<!---- Form Add New User -->
<div class="modal fade" id="modalForm">
    <div class="modal-dialog modal-dialog-centered">
      <div class="modal-content">
      
        <!-- Modal Header -->
        <div class="modal-header bg-merah">
          <h4 class="modal-title"><i class="fas fa-database"></i>&nbsp;Register Pengguna</h4>
          <button type="button" class="close" data-dismiss="modal">&times;</button>
        </div>
        
        <!-- Modal body -->
        <div class="modal-body">
          
        <form method="POST"  id="new_account">
        <div class="form-row">
<div class="col-md-12">
               
              <input type="hidden" id="user_id" value="">
              <input type="hidden" id="user_adminlevel" value="2">
                <input type="text" id="user_name" name="user_name" class="form-control" placeholder="User Name" required="required" autofocus="autofocus">
                
              </div>
</div>


<div class="form-row">
<div class="col-md-12" style="margin-top: 10px;">
               
              <input type="email" id="user_email" class="form-control" name="user_email" placeholder="Alamat Email" required="required">
                <!-- <input type="password" id="user_password" name="user_password" class="form-control user_pass" placeholder="Password" required="required" autofocus="autofocus"> -->
    
            
                
              </div>
</div>

<div class="form-row">
<div class="col-md-12" style="margin-top: 10px;">
               
              
                <input type="text" id="user_fullname" name="user_fullname" class="form-control" placeholder="<?php echo $this->lang->line('user_fullname');?>" required="required" autofocus="autofocus">
                 
            
                
              </div>
</div>




<div class="form-row">
<div class="col-md-12" style="margin-top: 10px;">
               
              
                <select name="user_unit_level" id="user_unit_level" class="form-control custom-select"  required="required" placeholder="Level Pengguna">
                      <option value="">Level Pengguna</option>
                  <?php

foreach($unitLevel as $level){
?>
<option value="<?php echo $level->unitgroup_id; ?>"><?php echo $level->unitgroup_name;?></option>
<?php
}
?>

</select>

            
                
              </div>
</div>



<div class="form-row">
<div class="col-md-12" style="margin-top: 10px;">
               
              
                <select name="user_group" id="user_group" class="form-control custom-select"  required="required" placeholder="<?php echo $this->lang->line("username_group");?>" disabled="disabled">
                      <option value=""><?php echo $this->lang->line("username_group");?></option>

</select>

            
                
              </div>
</div>



<div class="form-row">
<div class="col-md-12" style="margin-top: 10px;">
               
              
                <select name="user_province" id="user_province" class="form-control custom-select"  disabled="disabled">
                      <option value=""><?php echo $this->lang->line("province");?></option>
                  <?php

foreach($propinsi as $provlist){
?>
<option value="<?php echo $provlist->province_code; ?>"><?php echo $provlist->province_name;?></option>
<?php
}
?>

</select>        
               
              </div>
</div>



<div class="form-row">
<div class="col-md-12" style="margin-top: 10px;">
               
              
                <select name="user_district" id="user_district" class="form-control custom-select"  disabled="disabled">
                      <option value=""><?php echo $this->lang->line("district");?></option>
                  

</select>        
               
              </div>
</div>



<div class="form-row">
<div class="col-md-12" style="margin-top: 10px;">
               
              
                <select name="user_unit" id="user_unit" class="form-control custom-select"  disabled="disabled">
                      <option value=""><?php echo $this->lang->line("user_unit");?></option>
                  

</select>        
                
              </div>
</div>








<div class="form-row">
<div class="col-md-12" style="padding-top:10px">
               
                
                <button class="btn btn-success btn-block" type="submit"   id="SubmitButton"><?php echo $this->lang->line('submit');?></button>
               
               
              
              </div>
</div>

        </form>
        </div>
      </div>
    </div>
</div>

<!-- End Form -->







<!--- Edit Form -->


<!---- Form Add New User -->
<div class="modal fade" id="modalEdit">
    <div class="modal-dialog modal-dialog-centered modal-lg">
      <div class="modal-content">
      
        <!-- Modal Header -->
        <div class="modal-header bg-merah">
          <h4 class="modal-title"><i class="fas fa-database"></i>&nbsp;Register Pengguna</h4>
          <button type="button" class="close" data-dismiss="modal">&times;</button>
        </div>
        
        <!-- Modal body -->
        <div class="modal-body">
          
        <form method="POST"  id="edit_account">
        <div class="form-row">
<div class="input-group mb-3">
<div class="input-group-prepend w-50">
      <div class="input-group-text w-100" style="background-color: grey;color:white" style="background-color: grey;color:white">User name</div>
    </div> 
                <input type="text" id="edit_user_name" name="edit_user_name" class="form-control" placeholder="User Name" required="required" autofocus="autofocus">
                
              </div>
</div>


<div class="form-row">
<div class="input-group  mb-3">
<div class="input-group-prepend w-50">
      <div class="input-group-text w-100" style="background-color: grey;color:white">alamat Email</div>
    </div> 
               
              <input type="email" id="edit_user_email" class="form-control" name="edit_user_email" placeholder="Alamat Email" required="required">
                <!-- <input type="password" id="edit_user_password" name="edit_user_password" class="form-control edit_user_pass" placeholder="Password" required="required" autofocus="autofocus"> -->
    
            
                
              </div>
</div>

<div class="form-row">
<div class="input-group  mb-3">
<div class="input-group-prepend w-50">
      <div class="input-group-text w-100" style="background-color: grey;color:white">Nama Lengkap</div>
    </div> 
               
              
                <input type="text" id="edit_user_fullname" name="edit_user_fullname" class="form-control" placeholder="<?php echo $this->lang->line('edit_user_fullname');?>" required="required" autofocus="autofocus">
                 
            
                
              </div>
</div>




<div class="form-row">
<div class="input-group  mb-3">
<div class="input-group-prepend w-50">
      <div class="input-group-text w-100" style="background-color: grey;color:white">Level Pengguna</div>
    </div> 
               
              
                <select name="edit_user_unit_level" id="edit_user_unit_level" class="form-control custom-select"  required="required" placeholder="Level Pengguna">
                      
                  <?php

foreach($unitLevel as $level){
?>
<option value="<?php echo $level->unitgroup_id; ?>"><?php echo $level->unitgroup_name;?></option>
<?php
}
?>

</select>

            
                
              </div>
</div>



<div class="form-row">
<div class="input-group  mb-3">
<div class="input-group-prepend w-50">
      <div class="input-group-text w-100" style="background-color: grey;color:white">Group Pengguna</div>
    </div> 
               
              
                <select name="edit_user_group" id="edit_user_group" class="form-control custom-select"  required="required" placeholder="<?php echo $this->lang->line("username_group");?>" disabled="disabled">
                     

</select>

            
                
              </div>
</div>



<div class="form-row">
<div class="input-group  mb-3">
<div class="input-group-prepend w-50">
      <div class="input-group-text w-100" style="background-color: grey;color:white"><?php echo $this->lang->line("province");?></div>
    </div> 
               
              
                <select name="edit_user_province" id="edit_user_province" class="form-control custom-select"  disabled="disabled">
                    <option value="">&nbsp;</option>
                     
                  <?php

foreach($propinsi as $provlist){
?>
<option value="<?php echo $provlist->province_code; ?>"><?php echo $provlist->province_name;?></option>
<?php
}
?>

</select>        
               
              </div>
</div>



<div class="form-row">
<div class="input-group  mb-3">
<div class="input-group-prepend w-50">
      <div class="input-group-text w-100" style="background-color: grey;color:white"><?php echo $this->lang->line("district");?></div>
    </div> 
               
              
                <select name="edit_user_district" id="edit_user_district" class="form-control custom-select"  disabled="disabled">
                    
                  

</select>        
               
              </div>
</div>



<div class="form-row">
<div class="input-group  mb-3">
<div class="input-group-prepend w-50">
      <div class="input-group-text w-100" style="background-color: grey;color:white"><?php echo $this->lang->line("user_unit");?></div>
    </div> 
               
              
                <select name="edit_user_unit" id="edit_user_unit" class="form-control custom-select"  disabled="disabled">
                 
                  

</select>        
                
              </div>
</div>








<div class="form-row">
<div class="col-md-12" style="padding-top:10px">
               
                
                <button class="btn btn-success btn-block" type="submit"   id="SubmitButton"><?php echo $this->lang->line('submit');?></button>
               
               
              
              </div>
</div>

        </form>
        </div>
      </div>
    </div>
</div>

<!-- End Form -->

<script>

    function conf(id){
        if(confirm("apakah yakin akan menghapus user ini?")){
            document.location="<?php echo base_url()."administrator/users/delete/";?>"+id;
        }
    }

    function getDetail(uname){
        $('#new_account').attr('id','edit_account');
        $.ajax({
            url:"<?php echo base_url()."administrator/users/detail";?>",
            type:"POST",
            dataType:"json",
            data:{
                "user_name":uname
            },
            success:function(jdata){
                if(jdata.status=='success'){
                    
                    $('#edit_user_name').val(jdata.response.user_name).attr('readonly','readonly').css('background-color','white');
                    $('#edit_user_email').val(jdata.response.user_email);
                    $('#edit_user_fullname').val(jdata.response.user_fname);
                    $('#edit_user_unit_level').val(jdata.response.user_unit_level);



                    $.ajax({
                url:"<?php echo base_url()."administrator/users/usergroup";?>",
                type:"POST",
                dataType:"json",
                data:{
                    "user_unit_level":jdata.response.user_unit_level
                },
                success:function(jsdata){
                    var str = '<option value=""><?php echo $this->lang->line("username_group");?></option>';
                    $.each(jsdata.response,function(i,item){
                        if(jdata.response.user_group==item.group_id){
                            str +='<option value="'+item.group_id+'" selected=\"selected\">'+item.group_name+'</option>';

                        }else{
                        str +='<option value="'+item.group_id+'">'+item.group_name+'</option>';
                        }
                        $('#edit_user_group').html(str).removeAttr('disabled');

                    })
                }
            });
            if(jdata.response.user_unit_level=='1'){
                $.ajax({
                    url:"<?php echo base_url()."administratif/fasyankes/listfaskes";?>",
                    type:"POST",
                    dataType:"json",
                    data:{
                        //"unit_district":jdata.response.user_district,
                        "unit_unitgroup_id":jdata.response.user_unit_level
                    },
                    success:function(jfdata){
                        var str = '<option value=""><?php echo $this->lang->line("user_unit");?></option>';
                    $.each(jfdata.response,function(i,item){
                        str +='<option value="'+item.unit_code+'">'+item.unit_name+'</option>';
                        $('#edit_user_unit').html(str).removeAttr('disabled').val(jdata.response.user_unit);
                        $('#edit_user_province,#user_district').attr("disabled","disabled");

                    })
                    }
                })
            }else if(jdata.response.user_unit_level=='2'){
                $('#edit_user_province').removeAttr('disabled').val(jdata.response.user_province);
                $.ajax({
                    url:"<?php echo base_url()."administratif/fasyankes/listfaskes";?>",
                    type:"POST",
                    dataType:"json",
                    data:{
                        "unit_unitgroup_id":jdata.response.user_unit_level
                    },
                    success:function(jfdata){
                        var str = '<option value=""><?php echo $this->lang->line("user_unit");?></option>';
                    $.each(jfdata.response,function(i,item){
                        str +='<option value="'+item.unit_code+'">'+item.unit_name+'</option>';
                        $('#edit_user_unit').html(str).removeAttr('disabled').val(jdata.response.user_unit);
                        $('#user_district').attr("disabled","disabled");

                    })
                    }
                })

            }else if(jdata.response.user_unit_level=='3'){
                $('#edit_user_province').removeAttr('disabled').val(jdata.response.user_province);

                $.ajax({
        url :'<?php echo base_url()."administratif/kabupaten/listkabupaten";?>',
        type:'POST',
        dataType:'json',
        data:{
            'province_code':jdata.response.user_province
        },
        success:function(jkdata){

            var str ='<option value=""><?php echo $this->lang->line('district');?></option>';
            $.each(jkdata.response,function(i,item){
                if(jdata.response.user_district==item.district_code){
                    str +='<option value="'+item.district_code+'" selected=\"selected\">'+item.district_name+'</option>';

                }else{
                str +='<option value="'+item.district_code+'">'+item.district_name+'</option>';
                }
            })
            $('#edit_user_district').html(str).removeAttr('disabled');
            
          //  $(".preloader").fadeOut();
        }
    })



    $.ajax({
                    url:"<?php echo base_url()."administratif/fasyankes/listfaskes";?>",
                    type:"POST",
                    dataType:"json",
                    data:{
                        "unit_province":jdata.response.user_province,
                        "unit_unitgroup_id":jdata.response.user_unit_level
                    },
                    success:function(jfdata){
                        var str = '<option value=""><?php echo $this->lang->line("user_unit");?></option>';
                    $.each(jfdata.response,function(i,item){
                        if(jdata.response.user_unit==item.unit_code){
                            str +='<option value="'+item.unit_code+'" selected=\"selected\">'+item.unit_name+'</option>';

                        }else{
                        str +='<option value="'+item.unit_code+'">'+item.unit_name+'</option>';
                        }
                        $('#edit_user_unit').html(str).removeAttr('disabled');
                        

                    })
                    }
                })


            }else if(jdata.response.user_unit_level=='4'){
                $('#edit_user_province').removeAttr('disabled').val(jdata.response.user_province);

$.ajax({
url :'<?php echo base_url()."administratif/kabupaten/listkabupaten";?>',
type:'POST',
dataType:'json',
data:{
'province_code':jdata.response.user_province
},
success:function(jkdata){

var str ='<option value=""><?php echo $this->lang->line('district');?></option>';
$.each(jkdata.response,function(i,item){
if(jdata.response.user_district==item.district_code){
    str +='<option value="'+item.district_code+'" selected=\"selected\">'+item.district_name+'</option>';

}else{
str +='<option value="'+item.district_code+'">'+item.district_name+'</option>';
}
})
$('#edit_user_district').html(str).removeAttr('disabled');

//  $(".preloader").fadeOut();
}
})



$.ajax({
                    url:"<?php echo base_url()."administratif/fasyankes/listfaskes";?>",
                    type:"POST",
                    dataType:"json",
                    data:{
                        "unit_district":jdata.response.user_district,
                        "unit_unitgroup_id":jdata.response.user_unit_level
                    },
                    success:function(jfdata){
                        var str = '<option value=""><?php echo $this->lang->line("user_unit");?></option>';
                    $.each(jfdata.response,function(i,item){
                        str +='<option value="'+item.unit_code+'">'+item.unit_name+'</option>';
                        $('#edit_user_unit').html(str).removeAttr('disabled').val(jdata.response.user_unit);
                      

                    })
                    }
                })


}








                    $('#modalEdit').modal({
                        show:true,
                        backdrop:'static',
                        keyboard:false
                    })
                }
            }
        })
    }



    $('document').ready(function(){
        $('#search').keyup(function(){
            $('#dataBody').load('<?php echo base_url()."administrator/users/search";?>',{"search":$('#search').val()});
        })

        $("#user_name").alphanum({
  allowSpace: false, // Allow the space character
  allowUpper: false,  // Allow Upper Case characters
  disallow:''
});



        $('#user_province').change(function(){

            if($('#user_unit_level').val()=='3' || $('#user_unit_level').val()=='4'){
            $.ajax({
        url :'<?php echo base_url()."administratif/kabupaten/listkabupaten";?>',
        type:'POST',
        dataType:'json',
        data:{
            'province_code':$('#user_province').val()
        },
        success:function(jdata){

            var str ='<option value=""><?php echo $this->lang->line('district');?></option>';
            $.each(jdata.response,function(i,item){
                str +='<option value="'+item.district_code+'">'+item.district_name+'</option>';
            })
            $('#user_district').html(str).removeAttr('disabled');
            if($('#user_unit_level').val()=='3'){
                $('#user_unit').attr('disabled','disabled');
            }
          //  $(".preloader").fadeOut();
        }
    })
            }else{


                $.ajax({
                    url:"<?php echo base_url()."administratif/fasyankes/listfaskes";?>",
                    type:"POST",
                    dataType:"json",
                    data:{
                        "unit_unitgroup_id":$('#user_unit_level').val(),
                        "unit_province":$(this).val()
                    },
                    success:function(jdata){
                      //  if(jdata.response.status=='success'){

                        var str = '<option value=""><?php echo $this->lang->line("user_unit");?></option>';
                    $.each(jdata.response,function(i,item){
                        str +='<option value="'+item.unit_code+'">'+item.unit_name+'</option>';
                        $('#user_unit').html(str).removeAttr('disabled');
                    })
                        /*}else{
                            $('#user_unit').attr('disabled','disabled');
                        }*/
                    }
                })


            }
        });

$('#user_district').change(function(){
    

       

        $.ajax({
                    url:"<?php echo base_url()."administratif/fasyankes/listfaskes";?>",
                    type:"POST",
                    dataType:"json",
                    data:{
                        "unit_unitgroup_id":$('#user_unit_level').val(),
                        "unit_district":$(this).val()
                    },
                    success:function(jdata){
                     if(jdata.status=='success'){

                        var str = '<option value=""><?php echo $this->lang->line("user_unit");?></option>';
                    $.each(jdata.response,function(i,item){
                        str +='<option value="'+item.unit_code+'">'+item.unit_name+'</option>';
                        $('#user_unit').html(str).removeAttr('disabled');
                    })
                     }else{
                         $('#user_unit').attr('disabled','disabled');
                     }
                       
                    }
                })
        
    
})

        $('#user_unit_level').change(function(){
            $.ajax({
                url:"<?php echo base_url()."administrator/users/usergroup";?>",
                type:"POST",
                dataType:"json",
                data:{
                    "user_unit_level":$('#user_unit_level').val()
                },
                success:function(jsdata){
                    var str = '<option value=""><?php echo $this->lang->line("username_group");?></option>';
                    $.each(jsdata.response,function(i,item){
                        str +='<option value="'+item.group_id+'">'+item.group_name+'</option>';
                        $('#user_group').html(str).removeAttr('disabled');

                    })
                }
            });
            if($(this).val()=='1'){
                $.ajax({
                    url:"<?php echo base_url()."administratif/fasyankes/listfaskes";?>",
                    type:"POST",
                    dataType:"json",
                    data:{
                        "unit_unitgroup_id":$(this).val()
                    },
                    success:function(jdata){
                        var str = '<option value=""><?php echo $this->lang->line("user_unit");?></option>';
                    $.each(jdata.response,function(i,item){
                        str +='<option value="'+item.unit_code+'">'+item.unit_name+'</option>';
                        $('#user_unit').html(str).removeAttr('disabled');
                        $('#user_province,#user_district').attr("disabled","disabled");

                    })
                    }
                })
            }else{
                $('#user_province').removeAttr('disabled');
            }
        })


 
        

      




$('#edit_account').validate({
    rules:{
        edit_user_email:{
            required:true,
            email:true,
            remote:{
                url:"<?php echo base_url()."administrator/users/editemail";?>",
                type:"POST",
                data: {
          edit_user_name: function() {
            return $( "#edit_user_name" ).val();
          }


            }
        }
    }
    },
    messages:{
        edit_user_email:{
            required:"Email wajib diisi",
            remote:"Email  sudah digunakan",
            email:"Email Tidak valid"
        }
    },
    submitHandler:function(form){
        $.ajax({
            url:"<?php echo base_url()."administrator/users/update";?>",
            type:"POST",
            dataType:"json",
            data:{
                "user_name":$('#edit_user_name').val(),
                    "user_email":$('#edit_user_email').val(),   
                   // "user_password":$('#user_password').val(),
                    "user_fname":$('#edit_user_fullname').val(),
                    "user_group":$('#edit_user_group').val(),
                    "user_unit_level":$('#edit_user_unit_level').val(),
                    "user_province":$('#edit_user_province').val(),
                    "user_district":$('#edit_user_district').val(),
                    "user_unit":$('#edit_user_unit').val(),
                    "user_level":"1"

            },
            success:function(jsdata){
                if(jsdata.status=='success'){
                    alert("User berhasil di update");
                    document.location="<?php echo base_url()."account/user/list/1";?>";
                }else{
                    alert("User gagal diupdate");
                    document.location="<?php echo base_url()."account/user/list/1";?>";
                }

            }
        })
    }
});

        $('#new_account').validate({
            rules:{
                user_name:{
                    required:true,
                    remote:{
                        url:"<?php echo base_url()."administrator/users/uservalidate";?>",
                        type:"POST"
                    }
                },
                user_email:{
                    required:true,
                    email:true,
                    remote:{
                        url:"<?php echo base_url()."administrator/users/emailvalidation";?>",
                        type:"POST"
                    }
                },
                user_province:{
                    required:function(element){
                        return $('#user_unit_level').val()>1;
                    }
                },
                user_district:{
                    required:function(element){
                        return $('#user_unit_level').val()>2;
                    }
                },
                user_unit:{
                    required:function(element){
                    return $('#user_unit_level').val()=='4';
                    }
                }


            },
            messages:{
                user_name:{
                    required:"User name Wajib diisi",
                    remote:"User name {0} Sudah digunakan"
                },
                user_email:{
                    required : "Alamat Email Wajib diisi",
                    email:"Alamat Email tidak valid",
                    remote:"Alamat Email {0} ini sudah terdaftar"
                },
                user_fullname:"Nama Lengkap Wajib diisi",
                user_unit_level:"Level pengguna wajib dipilih",
                user_group:"Group Pengguna wajib dipilih",
                user_province:"Propinsi wajib dipilih",
                user_district:"Kabupaten wajib diisi",
                user_unit:"Institusi/Unit TB harus dipilih"
            },
            submitHandler:function(form){
                $.ajax({
                url:"<?php echo base_url()."administrator/users/useradd";?>",
                type:"POST",
                dataType:"json",
                data:{
                    "user_name":$('#user_name').val(),
                    "user_email":$('#user_email').val(),
                   
                   // "user_password":$('#user_password').val(),
                    "user_fname":$('#user_fullname').val(),
                    "user_group":$('#user_group').val(),
                    "user_unit_level":$('#user_unit_level').val(),
                    "user_province":$('#user_province').val(),
                    "user_district":$('#user_district').val(),
                    "user_unit":$('#user_unit').val(),
                    "user_level":"1"
                },
                success:function(jdata){
                    if(jdata.status=='success'){
                        $.ajax({
                            url:"<?php echo base_url()."email/mailer/newuser";?>",
                            type:"POST",
                            dataType:"json",
                            data:{
                                "user_name":$('#user_name').val()
                            },
                            success:function(jmail){
                                if(jmail.status=='success'){
                                    alert('User berhasil ditambahkan, username dan password dikirimkan melalui email');
                                    document.location="<?php echo base_url()."account/user/list/1";?>";
                                }else{
                                    alert("Pengiriman email gagal silahkan check kembali alamat emailnya");
                                    document.location="<?php echo base_url()."account/user/list/1";?>";
                                }
                            }

                        })



                    }else{
                        alert("Penambahan User Gagal");
                    }
                }
            })

            }
        });
        $('.close').click(function(){
            document.location="<?php echo base_url()."account/user/list/1";?>"
        })
    })




    //blok edit

    $('#edit_user_unit_level').change(function(){
            $.ajax({
                url:"<?php echo base_url()."administrator/users/usergroup";?>",
                type:"POST",
                dataType:"json",
                data:{
                    "user_unit_level":$('#edit_user_unit_level').val()
                },
                success:function(jsdata){
                    var str = '<option value=""><?php echo $this->lang->line("username_group");?></option>';
                    $.each(jsdata.response,function(i,item){
                        str +='<option value="'+item.group_id+'">'+item.group_name+'</option>';
                        $('#edit_user_group').html(str).removeAttr('disabled');

                    })
                }
            });
            if($(this).val()=='1'){
                $.ajax({
                    url:"<?php echo base_url()."administratif/fasyankes/listfaskes";?>",
                    type:"POST",
                    dataType:"json",
                    data:{
                        "unit_unitgroup_id":$(this).val()
                    },
                    success:function(jdata){
                        var str = '<option value=""><?php echo $this->lang->line("user_unit");?></option>';
                    $.each(jdata.response,function(i,item){
                        str +='<option value="'+item.unit_code+'">'+item.unit_name+'</option>';
                        $('#edit_user_unit').html(str).removeAttr('disabled');
                        $('#edit_user_province,#edit_user_district').attr("disabled","disabled");

                    })
                    }
                })
            }else{
                $('#edit_user_province').removeAttr('disabled');
            }
        })



    $('#edit_user_district').change(function(){
    

       

    $.ajax({
                url:"<?php echo base_url()."administratif/fasyankes/listfaskes";?>",
                type:"POST",
                dataType:"json",
                data:{
                    "unit_unitgroup_id":$('#edit_user_unit_level').val(),
                    "unit_district":$(this).val()
                },
                success:function(jdata){
                 if(jdata.status=='success'){

                    var str = '<option value=""><?php echo $this->lang->line("user_unit");?></option>';
                $.each(jdata.response,function(i,item){
                    str +='<option value="'+item.unit_code+'">'+item.unit_name+'</option>';
                    $('#edit_user_unit').html(str).removeAttr('disabled');
                })
                 }else{
                     $('#edit_user_unit').attr('disabled','disabled');
                 }
                   
                }
            })
    

})


    $('#edit_user_province').change(function(){

if($('#edit_user_unit_level').val()=='3' || $('#edit_user_unit_level').val()=='4'){
$.ajax({
url :'<?php echo base_url()."administratif/kabupaten/listkabupaten";?>',
type:'POST',
dataType:'json',
data:{
'province_code':$('#edit_user_province').val()
},
success:function(jdata){

var str ='<option value=""><?php echo $this->lang->line('district');?></option>';
$.each(jdata.response,function(i,item){
    str +='<option value="'+item.district_code+'">'+item.district_name+'</option>';
})
$('#edit_user_district').html(str).removeAttr('disabled');
if($('#edit_user_unit_level').val()=='3'){
    $('#edit_user_unit').attr('disabled','disabled');
}
//  $(".preloader").fadeOut();
}
})
}else{


    $.ajax({
        url:"<?php echo base_url()."administratif/fasyankes/listfaskes";?>",
        type:"POST",
        dataType:"json",
        data:{
            "unit_unitgroup_id":$('#edit_user_unit_level').val(),
            "unit_province":$(this).val()
        },
        success:function(jdata){
          //  if(jdata.response.status=='success'){

            var str = '<option value=""><?php echo $this->lang->line("user_unit");?></option>';
        $.each(jdata.response,function(i,item){
            str +='<option value="'+item.unit_code+'">'+item.unit_name+'</option>';
            $('#edit_user_unit').html(str).removeAttr('disabled');
        })
            /*}else{
                $('#user_unit').attr('disabled','disabled');
            }*/
        }
    })


}
});

    </script>